# Algo-web (Archived)

The algo-web repository is part of the Algo project.  
A frontend application of the Algo service is being developed in this repository.

## Scripts

First, you should install all the dependencies with the command `npm install`.

| Script              | What does                                                                               |
|---------------------|-----------------------------------------------------------------------------------------|
| `npm start`         | Runs the app in the development mode on [http://localhost:3000](http://localhost:3000). |
| `npm test`          | Launches the test runner in the interactive watch mode.                                 |
| `npm run build`     | Builds the app for production to the `build` folder.                                    |
| `npm run style`     | Check's a code style.                                                                   |
| `npm run style:fix` | Check's a code style then trying to correct all misspellings.                           |

## Contributing

Contributes to this project must follow approved gitflow.

### Branches & Tags

#### The master branch

* The **master** branch contains the latest version.
* Source branch for the **master** branch is only the **develop** branch.

#### The tags

* Previous versions are available through tags.
* Versions have a format: MAJOR.MINOR.PATCH e.g.: 1.3.2.
* Only the last PATCH version is stored as tag, e.g., 1.3.2 is stored, but 1.3.1 is deprecated and removed from the git.

#### The develop branch

* The **develop** branch is used for developing.

#### The other branches

* The dev branches should start from the **develop** branch.
* The dev branches should have prefix *feature/* or *fix/*.
* Any branch name should follow kebab-case.
* The dev branches should end in the **develop** branch with merge request.
* Merge request of the dev branch to develop branch should delete this dev branch after accept.

### Other rules

* Do not use rebase.
* Do not use squashing.

## Developers

Michael Linker - [work.michaellinker@gmail.com](mailto:work.michaellinker@gmail.com)