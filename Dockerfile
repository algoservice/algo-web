# Global ARG
ARG PROJECT_DIRECTORY=/tmp/algo-web

# 1: Build React app
FROM node:20-alpine AS develop

ARG PROJECT_DIRECTORY
WORKDIR $PROJECT_DIRECTORY

# Prepare production dependencies
COPY package.json .
RUN npm install

# Prepare project files
COPY . .

ENV CI=true
ENV PORT=3000

CMD ["npm", "start"]

FROM develop AS build

# Production build
RUN npm run build

# 2: Nginx setup
FROM nginx:alpine

ARG PROJECT_DIRECTORY
# Prepare Nginx config
COPY --from=build $PROJECT_DIRECTORY/nginx/algo.conf /etc/nginx/conf.d/algo.conf

WORKDIR /usr/share/nginx/html

# Remove default nginx static assets
RUN rm -rf ./*

# Copy built application to nginx web folder
COPY --from=build $PROJECT_DIRECTORY/build .

# Containers run Nginx with global directives and daemon off
ENTRYPOINT ["nginx", "-g", "daemon off;"]