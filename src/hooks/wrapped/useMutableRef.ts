import { MutableRefObject, useRef } from 'react';

export type MutableRef<T> = {
  set: (value: T) => void;
  get: () => T;
  reference: () => MutableRefObject<T>;
};
const useMutableRef = <T>(initialValue: T): MutableRef<T> => {
  const ref = useRef<T>(initialValue);

  const set = (value: T) => {
    ref.current = value;
  };

  const get = (): T => ref.current;

  const reference = (): MutableRefObject<T> => ref;

  return { set, get, reference };
};

export default useMutableRef;
