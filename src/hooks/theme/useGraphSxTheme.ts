import React from 'react';
import { useSelector } from 'react-redux';

import { useAppTheme } from './useAppTheme';
import { GraphSxTheme } from './types/GraphSxTheme';
import { RootState } from '../../redux/reducers/rootReducer';
import { createGraphPalette } from '../../constants/palettes/graphPalette';

export const useGraphSxTheme = (): GraphSxTheme => {
  const { mode } = useSelector((state: RootState) => state.themeReducer);
  const appTheme = useAppTheme();

  return React.useMemo(() => {
    const graphPalette = createGraphPalette(appTheme, mode);
    return {
      background: graphPalette.background,
      '& .react-flow__node': {
        background: graphPalette.node.background,
        color: graphPalette.node.primary,
        border: `1px solid  ${graphPalette.node.border.normal}`,
        '&:focus': {
          border: `1px solid ${graphPalette.node.border.selected}`,
        },
      },
      '& .react-flow__edge-text': {
        fill: graphPalette.edge.text.fill,
      },
      '& .react-flow__edge-textbg': {
        fill: graphPalette.edge.text.background.fill,
      },
      '& .react-flow__handle': {
        background: graphPalette.handle.background,
      },
      '& .react-flow__minimap': {
        background: graphPalette.minimap.background,
        '& .react-flow__minimap-mask': {
          fill: graphPalette.minimap.mask.background,
        },
        '& .react-flow__minimap-node': {
          fill: graphPalette.minimap.node.fill,
          stroke: 'none',
        },
      },
      '& .react-flow__controls': {
        button: {
          color: graphPalette.controls.button.primary,
          background: graphPalette.controls.button.background.normal,
          borderBottom: `1px solid ${graphPalette.controls.button.border}`,
          '&:hover': {
            background: graphPalette.controls.button.background.hover,
          },
          path: {
            fill: 'currentColor',
          },
        },
      },
      '& .react-flow__attribution': {
        background: graphPalette.attribution.background,
        textColor: graphPalette.attribution.textColor,
      },
    };
  }, [appTheme, mode]);
};
