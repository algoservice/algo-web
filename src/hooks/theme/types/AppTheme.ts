import { Theme } from '@mui/material';
import { AppPalette } from '../../../constants/palettes/types/AppPalette';

export type AppTheme = Theme & {
  palette: AppPalette;
};
