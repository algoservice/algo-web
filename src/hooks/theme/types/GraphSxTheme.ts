export type GraphSxTheme = {
  background: string;
  '& .react-flow__node': {
    border: string;
    color: string;
    background: string;
    '&:focus': { border: string };
  };
  '& .react-flow__handle': { background: string };
  '& .react-flow__minimap': {
    background: string;
    '& .react-flow__minimap-node': { fill: string; stroke: string };
    '& .react-flow__minimap-mask': { fill: string };
  };
  '& .react-flow__controls': {
    button: {
      color: string;
      background: string;
      path: { fill: string };
      '&:hover': { background: string };
      borderBottom: string;
    };
  };
  '& .react-flow__attribution': { background: string; textColor: string };
};
