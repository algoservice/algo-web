import React from 'react';
import { useSelector } from 'react-redux';

import { createTheme } from '@mui/material';

import { AppTheme } from './types/AppTheme';
import { RootState } from '../../redux/reducers/rootReducer';
import { createAppPalette } from '../../constants/palettes/appPalette';

export const useAppTheme = (): AppTheme => {
  const { mode } = useSelector((state: RootState) => state.themeReducer);

  return React.useMemo(() => createTheme(createAppPalette(mode)), [mode]);
};
