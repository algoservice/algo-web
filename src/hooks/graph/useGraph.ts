import { useCallback, useEffect } from 'react';
import { useSelector } from 'react-redux';

import { RootState } from '../../redux/reducers/rootReducer';
import { useRootDispatch } from '../../redux/dispatch/rootDispatch';
import { getGraph as getGraphAction } from '../../redux/actions/graph/getGraph';
import { getGraphs as getGraphsAction } from '../../redux/actions/graph/getGraphs';
import { updateGraph } from '../../redux/actions/graph/updateGraph';
import { createGraph } from '../../redux/actions/graph/createGraph';

import { MetaDataSchemeGraph } from '../../graph/types/MetaDataSchemeGraph';
import { GraphMetaId } from '../../graph/types/meta/GraphMetaId';
import { GraphSearch } from '../../search/types/GraphSearch';
import { Graph } from '../../graph/types/Graph';
import { SearchStatus } from '../../search/types/common/SearchStatus';
import { updateGraphSearch } from '../../redux/actions/search/updateGraphSearch';
import { updateGraphSearchStatus } from '../../redux/actions/search/updateGraphSearchStatus';

interface UseGraph {
  getGraph: (graphId: GraphMetaId) => void;
  getGraphs: (graphSearch: GraphSearch) => void;
  saveGraph: (graphValue: MetaDataSchemeGraph) => void;
  graph: Graph | null;
  graphs: Graph[];
  loading: boolean;
  success: boolean;
}

const useGraph = (): UseGraph => {
  const dispatch = useRootDispatch();

  const { graph, graphs, loading, success } = useSelector(
    (state: RootState) => state.graphReducer,
  );
  const { graphSearch, graphSearchStatus } = useSelector(
    (state: RootState) => state.searchReducer,
  );

  const getGraph = useCallback(
    (graphId: GraphMetaId) => {
      dispatch(getGraphAction(graphId));
    },
    [dispatch],
  );

  const getGraphs = useCallback(
    (search: GraphSearch) => {
      if (graphSearchStatus !== SearchStatus.EXHAUSTED) {
        dispatch(updateGraphSearchStatus(SearchStatus.IN_PROGRESS));
        dispatch(updateGraphSearch(search));
        dispatch(getGraphsAction(search));
      }
    },
    [dispatch, graphSearchStatus],
  );

  useEffect(() => {
    if (graphSearchStatus === SearchStatus.IN_PROGRESS && !loading) {
      if (graphs.length < graphSearch.searchPagination.size) {
        dispatch(updateGraphSearchStatus(SearchStatus.EXHAUSTED));
      } else {
        dispatch(updateGraphSearchStatus(SearchStatus.AVAILABLE));
      }
    }
  }, [
    dispatch,
    graphs,
    loading,
    graphSearchStatus,
    graphSearch.searchPagination.size,
  ]);

  const saveGraph = useCallback(
    (newGraph: MetaDataSchemeGraph) => {
      if (newGraph.meta.id) {
        dispatch(updateGraph(newGraph.meta.id, newGraph));
      } else {
        dispatch(createGraph(newGraph));
      }
    },
    [dispatch],
  );

  return {
    getGraph,
    getGraphs,
    saveGraph,
    graph,
    graphs,
    loading,
    success,
  };
};

export default useGraph;
