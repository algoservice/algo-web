import { useCallback } from 'react';
import { useParams } from 'react-router-dom';

import { GraphMetaId } from '../../graph/types/meta/GraphMetaId';

interface NavigationParams {
  getGraphId(): GraphMetaId | undefined;
}

const useNavigationParams = (): NavigationParams => {
  const params = useParams();

  const getGraphId = useCallback(() => params.graphId, [params]);

  return {
    getGraphId,
  };
};

export default useNavigationParams;
