import { useCallback } from 'react';
import { useNavigate } from 'react-router-dom';

import { navigationScheme } from '../../constants/navigation/navigationScheme';
import { Endpoint } from '../../constants/navigation/endpoints/Endpoint';

interface Navigation {
  toHome(): void;

  toEditor(): void;

  toGraphs(): void;
}

const useNavigation = (): Navigation => {
  const navigate = useNavigate();

  const to = useCallback(
    (endpoint: Endpoint) => {
      navigate(endpoint.url);
    },
    [navigate],
  );

  const toHome = useCallback(() => {
    to(navigationScheme.home);
  }, [to]);

  const toEditor = useCallback(() => {
    to(navigationScheme.editor);
  }, [to]);

  const toGraphs = useCallback(() => {
    to(navigationScheme.graphs);
  }, [to]);

  return {
    toHome,
    toEditor,
    toGraphs,
  };
};

export default useNavigation;
