import { useCallback } from 'react';
import { useSelector } from 'react-redux';

import { RootState } from '../../redux/reducers/rootReducer';
import { useRootDispatch } from '../../redux/dispatch/rootDispatch';
import { clearGraph } from '../../redux/actions/internal_graph/clearGraph';
import { setGraph } from '../../redux/actions/internal_graph/setGraph';

import { MetaDataSchemeGraph } from '../../graph/types/MetaDataSchemeGraph';
import isGraphEdge from '../../graph/utils/guards/isGraphEdge';
import isGraphNode from '../../graph/utils/guards/isGraphNode';

interface UseInternalGraph {
  loadGraph: (newGraph: MetaDataSchemeGraph) => void;
  unloadGraph: () => void;
  graph: MetaDataSchemeGraph;
}

const useInternalGraph = (): UseInternalGraph => {
  const dispatch = useRootDispatch();

  const { graph, selectedElement } = useSelector(
    (state: RootState) => state.internalGraphReducer,
  );

  const loadGraph = useCallback(
    (newGraph: MetaDataSchemeGraph) => {
      dispatch(setGraph(newGraph));
    },
    [dispatch],
  );

  const unloadGraph = useCallback(() => {
    dispatch(clearGraph());
  }, [dispatch]);

  const currentGraph = {
    ...graph,
    nodes: isGraphNode(selectedElement)
      ? graph.scheme.nodes.concat(selectedElement)
      : graph.scheme.nodes,
    edges: isGraphEdge(selectedElement)
      ? graph.scheme.edges.concat(selectedElement)
      : graph.scheme.edges,
  };

  return {
    loadGraph,
    unloadGraph,
    graph: currentGraph,
  };
};

export default useInternalGraph;
