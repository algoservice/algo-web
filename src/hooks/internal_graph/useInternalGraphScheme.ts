import { useCallback } from 'react';
import { useSelector } from 'react-redux';

import { useRootDispatch } from '../../redux/dispatch/rootDispatch';
import { RootState } from '../../redux/reducers/rootReducer';
import { addNode } from '../../redux/actions/internal_graph/addNode';
import { updateGraphScheme } from '../../redux/actions/internal_graph/updateGraphScheme';
import { updateSelectedGraphElement } from '../../redux/actions/internal_graph/updateSelectedGraphElement';
import { addEdge } from '../../redux/actions/internal_graph/addEdge';
import { deleteSelectedGraphElement } from '../../redux/actions/internal_graph/deleteSelectedGraphElement';

import { GraphScheme } from '../../graph/types/scheme/GraphScheme';
import {
  GraphNodeType,
  InputNodeType,
  OutputNodeType,
} from '../../graph/types/node/GraphNodeType';
import { GraphNodeId } from '../../graph/types/node/GraphNodeId';
import { GraphNodeData } from '../../graph/types/node/GraphNodeData';
import { GraphNode } from '../../graph/types/node/GraphNode';
import { GraphEdge } from '../../graph/types/edge/GraphEdge';
import { GraphEdgeId } from '../../graph/types/edge/GraphEdgeId';
import isGraphNode from '../../graph/utils/guards/isGraphNode';
import isGraphEdge from '../../graph/utils/guards/isGraphEdge';
import getEdgesFromNode from '../../graph/utils/getEdgesFromNode';
import getEdgesToNode from '../../graph/utils/getEdgesToNode';
import {
  currentElementSelection,
  ElementSelection,
  newElementSelection,
  noElementSelection,
  replacementElementSelection,
} from '../../graph/utils/elementSelections';

import { en } from '../../constants/l10n/en';

interface UseInternalGraphScheme {
  addNodeToGraph: (type: GraphNodeType) => void;
  changeNodeType: (node: GraphNode, type: GraphNodeType) => void;
  addEdgeToGraph: (sourceId: GraphNodeId, targetId: GraphNodeId) => void;
  reconnectEdge: (
    edge: GraphEdge,
    newSourceId: GraphNodeId,
    newTargetId: GraphNodeId,
  ) => void;
  removeEdgeFromGraph: (edgeId: GraphEdgeId) => void;
  updateGraphScheme: (newScheme: GraphScheme) => void;
  selectGraphElement: (id: GraphNodeId | GraphEdgeId | null) => void;
  updateSelectedGraphElement: (element: GraphNode | GraphEdge) => void;
  deleteSelectedGraphElement: () => void;
  graphScheme: GraphScheme;
  selectedGraphElement: GraphNode | GraphEdge | null;
}

const useInternalGraphScheme = (): UseInternalGraphScheme => {
  const dispatch = useRootDispatch();

  const { graph, selectedElement } = useSelector(
    (state: RootState) => state.internalGraphReducer,
  );

  const generateNewNodeId = useCallback((): GraphNodeId => {
    const selectedElementId: GraphNodeId = isGraphNode(selectedElement)
      ? selectedElement.id
      : '0';
    const nodeMaxId = Math.max(
      ...graph.scheme.nodes.map((node) => (node ? Number(node.id) : 0)),
      Number(selectedElementId),
    );
    return (nodeMaxId + 1).toString();
  }, [graph, selectedElement]);

  const generateNewNodePlaceholderData = useCallback(
    (nodeId: GraphNodeId): GraphNodeData => ({
      label: `${en.graph.scheme.placeholder.node} ${nodeId}`,
    }),
    [],
  );

  const addNodeToGraph = useCallback(
    (type: GraphNodeType) => {
      const nodeId = generateNewNodeId();
      const nodeData = generateNewNodePlaceholderData(nodeId);
      const newNode: GraphNode = {
        id: nodeId,
        position: {
          x: 0,
          y: 0,
        },
        data: nodeData,
        type,
      };
      dispatch(addNode(newNode));
    },
    [dispatch, generateNewNodeId, generateNewNodePlaceholderData],
  );

  const changeNodeType = useCallback(
    (targetNode: GraphNode, newType: GraphNodeType) => {
      let redundantInputEdges: GraphEdge[] = [];
      let redundantOutputEdges: GraphEdge[] = [];
      if (newType === OutputNodeType) {
        redundantOutputEdges = getEdgesFromNode(
          targetNode.id,
          graph.scheme.edges,
        );
      }
      if (newType === InputNodeType) {
        redundantInputEdges = getEdgesToNode(targetNode.id, graph.scheme.edges);
      }

      const edgesWithoutRedundantEdges = graph.scheme.edges.filter(
        (edge) =>
          !(
            redundantInputEdges.find((inputEdge) => edge.id === inputEdge.id) ||
            redundantOutputEdges.find((outputEdge) => edge.id === outputEdge.id)
          ),
      );

      dispatch(
        updateGraphScheme({
          ...graph.scheme,
          edges: [...edgesWithoutRedundantEdges],
        }),
      );
      if (
        isGraphEdge(selectedElement) &&
        (redundantInputEdges.find(
          (inputEdge) => inputEdge.id === selectedElement.id,
        ) ||
          redundantOutputEdges.find(
            (outputEdge) => outputEdge.id === selectedElement.id,
          ))
      ) {
        deleteSelectedGraphElement();
      }
      if (
        isGraphNode(selectedElement) &&
        targetNode.id === selectedElement.id
      ) {
        dispatch(
          updateSelectedGraphElement({
            ...targetNode,
            type: newType,
          }),
        );
      }
    },
    [dispatch, graph.scheme, selectedElement],
  );

  const addEdgeToGraph = useCallback(
    (sourceId: GraphNodeId, targetId: GraphNodeId) => {
      const edgeId = `${sourceId}-${targetId}`;
      const newEdge: GraphEdge = {
        id: edgeId,
        source: sourceId,
        target: targetId,
      };
      dispatch(
        addEdge({
          ...graph.scheme,
          edges: [...graph.scheme.edges, newEdge],
        } as GraphScheme),
      );
    },
    [dispatch, graph.scheme],
  );

  const reconnectEdge = useCallback(
    (edge: GraphEdge, newSourceId: GraphNodeId, newTargetId: GraphNodeId) => {
      const newEdge: GraphEdge = {
        id: `${newSourceId}-${newTargetId}`,
        source: newSourceId,
        target: newTargetId,
        label: edge.label,
        type: edge.type,
      };
      if (isGraphEdge(selectedElement) && edge.id === selectedElement.id) {
        dispatch(updateSelectedGraphElement(newEdge));
      }
    },
    [dispatch, selectedElement],
  );

  const removeEdgeFromGraph = useCallback(
    (edgeId: GraphEdgeId) => {
      dispatch(
        updateGraphScheme({
          ...graph.scheme,
          edges: graph.scheme.edges.filter((edge) => edge.id !== edgeId),
        }),
      );
    },
    [dispatch, graph.scheme],
  );

  const updateScheme = useCallback(
    (scheme: GraphScheme) => {
      dispatch(updateGraphScheme(scheme));
    },
    [dispatch],
  );

  const selectElement = useCallback(
    (id: GraphNodeId | GraphEdgeId | null) => {
      let selectionAction: ElementSelection;

      if (id) {
        if (selectedElement) {
          selectionAction = replacementElementSelection(
            id,
            selectedElement,
            graph.scheme,
          );
        } else {
          selectionAction = newElementSelection(id, graph.scheme);
        }
      } else if (selectedElement) {
        selectionAction = currentElementSelection(
          selectedElement,
          graph.scheme,
        );
      } else {
        selectionAction = noElementSelection(graph.scheme);
      }

      dispatch(updateGraphScheme(selectionAction.newScheme));
      dispatch(updateSelectedGraphElement(selectionAction.selectedElement));
    },
    [dispatch, graph.scheme, selectedElement],
  );

  const updateSelectedElement = useCallback(
    (element: GraphNode | GraphEdge) => {
      dispatch(updateSelectedGraphElement(element));
    },
    [dispatch],
  );

  const deleteSelectedElement = useCallback(() => {
    if (isGraphNode(selectedElement)) {
      const redundantInputEdges: GraphEdge[] = getEdgesToNode(
        selectedElement.id,
        graph.scheme.edges,
      );
      const redundantOutputEdges: GraphEdge[] = getEdgesFromNode(
        selectedElement.id,
        graph.scheme.edges,
      );

      const edgesWithoutRedundantEdges = graph.scheme.edges.filter(
        (edge) =>
          !(
            redundantInputEdges.find((inputEdge) => edge.id === inputEdge.id) ||
            redundantOutputEdges.find((outputEdge) => edge.id === outputEdge.id)
          ),
      );

      dispatch(
        updateGraphScheme({
          ...graph.scheme,
          edges: [...edgesWithoutRedundantEdges],
        }),
      );
    }

    dispatch(deleteSelectedGraphElement());
  }, [dispatch, graph.scheme, selectedElement]);

  return {
    addNodeToGraph,
    changeNodeType,
    addEdgeToGraph,
    reconnectEdge,
    removeEdgeFromGraph,
    updateGraphScheme: updateScheme,
    selectGraphElement: selectElement,
    updateSelectedGraphElement: updateSelectedElement,
    deleteSelectedGraphElement: deleteSelectedElement,
    graphScheme: graph.scheme,
    selectedGraphElement: selectedElement,
  };
};

export default useInternalGraphScheme;
