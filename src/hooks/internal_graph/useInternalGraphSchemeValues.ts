import { useMemo } from 'react';
import { useSelector } from 'react-redux';

import { RootState } from '../../redux/reducers/rootReducer';

import isGraphNode from '../../graph/utils/guards/isGraphNode';
import isGraphEdge from '../../graph/utils/guards/isGraphEdge';
import { GraphNode } from '../../graph/types/node/GraphNode';
import getEdgeAvailableConnections, {
  EdgeAvailableConnections,
} from '../../graph/utils/getEdgeAvailableConnections';
import getNodeAvailableConnections, {
  NodeAvailableConnections,
} from '../../graph/utils/getNodeAvailableConnections';
import getNodeConnections, {
  NodeConnections,
} from '../../graph/utils/getNodeConnections';

interface UseInternalGraphSchemeValues {
  selectedEdgeAvailableSource: GraphNode[];
  selectedEdgeAvailableTarget: GraphNode[];
  selectedNodeAvailableInput: GraphNode[];
  selectedNodeAvailableOutput: GraphNode[];
  selectedNodeConnectedToInput: GraphNode[];
  selectedNodeConnectedFromOutput: GraphNode[];
}

const useInternalGraphSchemeValues = (): UseInternalGraphSchemeValues => {
  const { graph, selectedElement } = useSelector(
    (state: RootState) => state.internalGraphReducer,
  );

  const selectedEdgeAvailableConnections =
    useMemo((): EdgeAvailableConnections => {
      if (isGraphEdge(selectedElement)) {
        return getEdgeAvailableConnections(
          selectedElement,
          graph.scheme.nodes,
          graph.scheme.edges,
        );
      }
      return {
        source: [],
        target: [],
      };
    }, [graph.scheme, selectedElement]);

  const selectedNodeAvailableConnections =
    useMemo((): NodeAvailableConnections => {
      if (isGraphNode(selectedElement)) {
        return getNodeAvailableConnections(selectedElement, graph.scheme.nodes);
      }
      return {
        output: graph.scheme.nodes,
        input: graph.scheme.nodes,
      };
    }, [graph.scheme.nodes, selectedElement]);

  const selectedNodeCurrentConnections = useMemo((): NodeConnections => {
    if (isGraphNode(selectedElement)) {
      return getNodeConnections(
        selectedElement,
        graph.scheme.nodes,
        graph.scheme.edges,
      );
    }
    return {
      input: [],
      output: [],
    };
  }, [graph.scheme, selectedElement]);

  return {
    selectedEdgeAvailableSource: selectedEdgeAvailableConnections.source,
    selectedEdgeAvailableTarget: selectedEdgeAvailableConnections.target,
    selectedNodeAvailableInput: selectedNodeAvailableConnections.input,
    selectedNodeAvailableOutput: selectedNodeAvailableConnections.output,
    selectedNodeConnectedToInput: selectedNodeCurrentConnections.input,
    selectedNodeConnectedFromOutput: selectedNodeCurrentConnections.output,
  };
};

export default useInternalGraphSchemeValues;
