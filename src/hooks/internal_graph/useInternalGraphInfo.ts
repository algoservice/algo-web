import { useCallback } from 'react';
import { useSelector } from 'react-redux';

import { useRootDispatch } from '../../redux/dispatch/rootDispatch';
import { RootState } from '../../redux/reducers/rootReducer';

import { GraphDataName } from '../../graph/types/data/GraphDataName';
import { GraphData } from '../../graph/types/data/GraphData';
import { updateGraphInfo } from '../../redux/actions/internal_graph/updateGraphInfo';

interface UseInternalGraphInfo {
  updateGraphName: (name: GraphDataName) => void;
  graphInfo: GraphData;
}

const useInternalGraphInfo = (): UseInternalGraphInfo => {
  const dispatch = useRootDispatch();

  const { graph } = useSelector(
    (state: RootState) => state.internalGraphReducer,
  );

  const updateName = useCallback(
    (name: GraphDataName) => {
      const newInfo = {
        ...graph.data,
        name,
      };
      dispatch(updateGraphInfo(newInfo));
    },
    [dispatch, graph.data],
  );

  return {
    updateGraphName: updateName,
    graphInfo: graph.data,
  };
};

export default useInternalGraphInfo;
