import { useCallback } from 'react';

import { useRootDispatch } from '../../redux/dispatch/rootDispatch';
import { centerGraph as centerGraphAction } from '../../redux/actions/internal_graph/centerGraph';
import { fitGraph as fitGraphAction } from '../../redux/actions/internal_graph/fitGraph';

interface InternalGraphControls {
  centerGraph: () => void;
  fitGraph: () => void;
}

const useInternalGraphControls = (): InternalGraphControls => {
  const dispatch = useRootDispatch();

  const centerGraph = useCallback(() => {
    dispatch(centerGraphAction());
  }, [dispatch]);

  const fitGraph = useCallback(() => {
    dispatch(fitGraphAction());
  }, [dispatch]);

  return {
    centerGraph,
    fitGraph,
  };
};

export default useInternalGraphControls;
