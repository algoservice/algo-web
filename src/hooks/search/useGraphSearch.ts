import { useCallback } from 'react';
import { useSelector } from 'react-redux';

import { RootState } from '../../redux/reducers/rootReducer';
import { useRootDispatch } from '../../redux/dispatch/rootDispatch';
import { updateGraphSearch as updateGraphSearchAction } from '../../redux/actions/search/updateGraphSearch';
import { updateGraphSearchStatus as updateGraphSearchStatusAction } from '../../redux/actions/search/updateGraphSearchStatus';

import { GraphSearch } from '../../search/types/GraphSearch';
import { SearchStatus } from '../../search/types/common/SearchStatus';

interface UseGraphSearch {
  updateGraphSearch: (graphSearch: GraphSearch) => void;
  updateGraphSearchStatus: (graphSearchStatus: SearchStatus) => void;
  graphSearch: GraphSearch;
  graphSearchStatus: SearchStatus;
}

const useGraphSearch = (): UseGraphSearch => {
  const dispatch = useRootDispatch();

  const { graphSearch, graphSearchStatus } = useSelector(
    (state: RootState) => state.searchReducer,
  );

  const updateGraphSearch = useCallback(
    (search: GraphSearch) => {
      dispatch(updateGraphSearchAction(search));
    },
    [dispatch],
  );

  const updateGraphSearchStatus = useCallback(
    (searchStatus: SearchStatus) => {
      dispatch(updateGraphSearchStatusAction(searchStatus));
    },
    [dispatch],
  );

  return {
    updateGraphSearch,
    graphSearch,
    updateGraphSearchStatus,
    graphSearchStatus,
  };
};

export default useGraphSearch;
