import React, { useEffect, useState } from 'react';

import { Box, Button, LinearProgress } from '@mui/material';

import useGraph from '../../../hooks/graph/useGraph';
import useInternalGraphScheme from '../../../hooks/internal_graph/useInternalGraphScheme';
import useInternalGraphControls from '../../../hooks/internal_graph/useInternalGraphControls';
import useInternalGraph from '../../../hooks/internal_graph/useInternalGraph';
import useNavigationParams from '../../../hooks/navigation/useNavigationParams';
import useMountEffect from '../../../hooks/wrapped/useMountEffect';
import useNavigation from '../../../hooks/navigation/useNavigation';

import LoadingModalAtom, {
  LoadingModalAtomState,
} from '../../components/atoms/modals/loading/LoadingModalAtom';
import TypographyAtom from '../../components/atoms/typography/default/TypographyAtom';
import StatelessGraphAtom from '../../components/atoms/graphs/stateless/StatelessGraphAtom';

import { en } from '../../../constants/l10n/en';
import { emptyGraph } from '../../../graph/instances/emptyGraph';
import isMetaDataSchemeGraph from '../../../graph/utils/guards/isMetaDataSchemeGraph';

const ViewPage: React.FC<unknown> = () => {
  const { toHome } = useNavigation();
  const { getGraphId } = useNavigationParams();
  const { getGraph, graph, loading, success } = useGraph();
  const { graphScheme } = useInternalGraphScheme();
  const { loadGraph } = useInternalGraph();
  const { fitGraph } = useInternalGraphControls();

  const [loadingModalState, setLoadingModalState] =
    useState<LoadingModalAtomState>(LoadingModalAtomState.LOADING);

  useMountEffect(() => {
    getGraph(getGraphId() ?? '');
  });

  useEffect(() => {
    if (loading) {
      setLoadingModalState(LoadingModalAtomState.LOADING);
    } else if (success) {
      setLoadingModalState(LoadingModalAtomState.SUCCESS);
      loadGraph(isMetaDataSchemeGraph(graph) ? graph : emptyGraph);
      fitGraph();
    } else {
      setLoadingModalState(LoadingModalAtomState.FAILURE);
    }
  }, [loading, success, graph, loadGraph, fitGraph]);

  return (
    <Box className='view' component='main' width='100%' height='100%'>
      <LoadingModalAtom
        className='view__loading'
        open={
          loadingModalState === LoadingModalAtomState.LOADING ||
          loadingModalState === LoadingModalAtomState.FAILURE
        }
        state={loadingModalState}
        renderOnLoading={() => (
          <>
            <TypographyAtom
              className='view__loading-text-success'
              component='h5'
              variant='h5'
              color='text.primary'>
              {en.graph.loading}
            </TypographyAtom>
            <LinearProgress
              className='view__loading-progress'
              sx={{ width: '60%', maxWidth: '900px' }}
            />
          </>
        )}
        renderOnFailure={() => (
          <>
            <TypographyAtom
              className='view__loading-text-failure'
              component='h5'
              variant='h5'
              color='text.primary'>
              {en.graph.notFound}
            </TypographyAtom>
            <Button
              className='view__loading-back-button'
              variant='outlined'
              sx={{ width: 'fit-content' }}
              onClick={() => toHome()}>
              {en.graph.back}
            </Button>
          </>
        )}
      />
      <StatelessGraphAtom
        className='view__graph'
        sx={{ width: '100%', height: '100%' }}
        graphScheme={graphScheme}
      />
    </Box>
  );
};

export default ViewPage;
