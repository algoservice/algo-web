import React from 'react';

import { Box, Button, Stack } from '@mui/material';

import useNavigation from '../../../hooks/navigation/useNavigation';

import TypographyAtom from '../../components/atoms/typography/default/TypographyAtom';

import { en } from '../../../constants/l10n/en';

const NotFoundPage: React.FC<unknown> = () => {
  const navigation = useNavigation();

  return (
    <Box
      className='not-found'
      width='100%'
      height='100%'
      display='flex'
      alignItems='center'
      justifyContent='center'>
      <Stack
        className='not-found__content'
        component='main'
        direction='column'
        alignItems='center'
        spacing={2}
        paddingX='10vw'
        paddingY='10vh'>
        <TypographyAtom
          className='not-found__header'
          component='h5'
          align='center'
          variant='h5'>
          {en.pages.notFound.header}
        </TypographyAtom>
        <TypographyAtom
          className='not-found__fzf'
          component='h1'
          align='center'
          fontSize='1000%'>
          {en.pages.notFound.fzf}
        </TypographyAtom>
        <Button
          className='not_found__back-button'
          variant='outlined'
          sx={{ width: 'fit-content' }}
          onClick={() => navigation.toHome()}>
          {en.pages.notFound.back}
        </Button>
      </Stack>
    </Box>
  );
};

export default NotFoundPage;
