import React from 'react';
import GraphGridOrganism from '../../components/organisms/graphs/grid/GraphGridOrganism';

const GraphsPage: React.FC = () => <GraphGridOrganism />;

export default GraphsPage;
