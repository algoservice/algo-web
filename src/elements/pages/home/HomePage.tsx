import React from 'react';

import { Box, Stack } from '@mui/material';

import { useThemeBreakpoints } from '../../../hooks/theme/useThemeBreakpoints';
import useNavigation from '../../../hooks/navigation/useNavigation';
import useGraph from '../../../hooks/graph/useGraph';

import { useRootDispatch } from '../../../redux/dispatch/rootDispatch';
import { switchThemeMode } from '../../../redux/actions/theme/switchThemeMode';
import TypographyAtom from '../../components/atoms/typography/default/TypographyAtom';
import ImageAtom from '../../components/atoms/images/ImageAtom';
import ThemeModeIconButtonAtom from '../../components/atoms/buttons/icon/theme_mode/ThemeModeIconButtonAtom';
import SearchInputAtom from '../../components/atoms/inputs/search/SearchInputAtom';

import { SearchText } from '../../../search/types/common/SearchText';
import makeGraphSearch from '../../../search/makers/makeGraphSearch';

import { en } from '../../../constants/l10n/en';
import { publicFiles } from '../../../constants/public/files';

const HomePage: React.FC = () => {
  const dispatch = useRootDispatch();

  const breakpoint = useThemeBreakpoints();
  const { toGraphs } = useNavigation();
  const { getGraphs } = useGraph();

  const [searchText, setSearchText] = React.useState<SearchText>('');

  const handleSearchChange = (change: SearchText) => {
    setSearchText(change);
  };

  const doSearch = () => {
    toGraphs();
    getGraphs(makeGraphSearch({ searchText }));
  };

  const calculateSearchSize = () => {
    switch (breakpoint) {
      case 'xs':
        return { width: '90%' };
      case 'sm':
        return { width: '80%' };
      case 'md':
        return { width: '65%' };
      default:
        return { width: '55%' };
    }
  };

  return (
    <Box
      className='home'
      width='100%'
      height='100%'
      display='flex'
      alignItems='center'
      justifyContent='center'>
      <ThemeModeIconButtonAtom
        className='home__theme-button'
        onClick={() => dispatch(switchThemeMode())}
        sx={{ position: 'absolute', top: '10px', right: '10px' }}
      />
      <Stack
        className='home__content'
        component='main'
        width='100%'
        height='100%'
        direction='column'
        alignItems='center'
        justifyContent='center'
        spacing={4}>
        <TypographyAtom
          className='home__header'
          component={breakpoint === 'xs' ? 'h3' : 'h2'}
          variant={breakpoint === 'xs' ? 'h3' : 'h2'}
          fontWeight='bolder'>
          {en.pages.home.header}
        </TypographyAtom>
        <SearchInputAtom
          className='home__search'
          placeholder={en.pages.home.search.placeholder}
          sx={calculateSearchSize()}
          onChange={handleSearchChange}
          onSearch={doSearch}
        />
        <ImageAtom
          className='home__image'
          src={publicFiles.static.images.UniversalHome}
          alt={en.serviceName}
          sx={{ height: '20vh', minHeight: '20vw' }}
        />
      </Stack>
    </Box>
  );
};

export default HomePage;
