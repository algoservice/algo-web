import React from 'react';

import { Box } from '@mui/material';

import { useAppTheme } from '../../../hooks/theme/useAppTheme';
import { useThemeBreakpoints } from '../../../hooks/theme/useThemeBreakpoints';

import StatefulGraphAtom from '../../components/atoms/graphs/stateful/StatefulGraphAtom';
import GraphCompactEditorOrganism from '../../components/organisms/graphs/editors/compact/GraphCompactEditorOrganism';
import GraphFullEditorOrganism from '../../components/organisms/graphs/editors/full/GraphFullEditorOrganism';

const EditorPage: React.FC<unknown> = () => {
  const theme = useAppTheme();
  const breakpoint = useThemeBreakpoints();

  const renderGraph = () => (
    <StatefulGraphAtom
      className='editor__graph'
      sx={{ width: '100%', height: '100%' }}
      interactive
    />
  );

  const renderEditor = () =>
    breakpoint === 'xs' ? (
      <GraphCompactEditorOrganism children={renderGraph()} />
    ) : (
      <GraphFullEditorOrganism children={renderGraph()} />
    );

  return (
    <Box
      className='editor'
      component='main'
      width='100%'
      height='100%'
      sx={{ backgroundColor: theme.palette.background.paper }}>
      {renderEditor()}
    </Box>
  );
};

export default EditorPage;
