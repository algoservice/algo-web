import React from 'react';
import { Outlet } from 'react-router-dom';

import { LayoutProps } from '../LayoutProps';
import BackgroundLayout from '../background/BackgroundLayout';

const MainLayout: React.FC<LayoutProps> = ({ children }) => (
  <BackgroundLayout>{children ? children : <Outlet />}</BackgroundLayout>
);

export default MainLayout;
