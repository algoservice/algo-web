import React from 'react';
import { Outlet } from 'react-router-dom';

import { Box } from '@mui/material';

import { LayoutProps } from '../LayoutProps';

const BlankLayout: React.FC<LayoutProps> = ({ children, elevated }) => {
  const height = elevated ? { height: '100%' } : { minHeight: '100%' };

  return (
    <Box className='blank-layout' width='100%' {...height}>
      <Box className='blank-layout__content' width='100%' height='100%'>
        {children ? children : <Outlet />}
      </Box>
    </Box>
  );
};

export default BlankLayout;
