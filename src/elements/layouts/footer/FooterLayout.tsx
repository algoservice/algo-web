import React from 'react';
import { Outlet } from 'react-router-dom';

import { Box } from '@mui/material';

import { LayoutProps } from '../LayoutProps';
import FooterOrganism from '../../components/organisms/footer/FooterOrganism';

const FooterLayout: React.FC<LayoutProps> = ({ children }) => (
  <Box
    className='footer-layout'
    width='100%'
    minHeight='100%'
    display='flex'
    flexDirection='column'>
    <Box className='footer-layout__content' display='flex' flexGrow={1}>
      <Box className='footer-layout__content-wrapper' flexGrow={1}>
        {children ? children : <Outlet />}
      </Box>
    </Box>
    <FooterOrganism />
  </Box>
);

export default FooterLayout;
