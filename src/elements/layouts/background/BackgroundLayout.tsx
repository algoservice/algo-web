import React from 'react';
import { Outlet } from 'react-router-dom';

import { Paper } from '@mui/material';

import { LayoutProps } from '../LayoutProps';

const BackgroundLayout: React.FC<LayoutProps> = ({ children }) => (
  <Paper
    square
    sx={{
      width: '100%',
      height: '100%',
    }}>
    {children ? children : <Outlet />}
  </Paper>
);

export default BackgroundLayout;
