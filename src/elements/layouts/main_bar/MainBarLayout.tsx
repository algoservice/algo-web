import React from 'react';
import { Outlet } from 'react-router-dom';

import { Box } from '@mui/material';

import { LayoutProps } from '../LayoutProps';
import AppBarOrganism from '../../components/organisms/app_bar/AppBarOrganism';

const MainBarLayout: React.FC<LayoutProps> = ({ children, elevated }) => (
  <Box
    className='main-bar-layout'
    width='100%'
    height='100%'
    display='flex'
    flexDirection='column'>
    <AppBarOrganism elevated={elevated} />
    <Box className='main-bar-layout__content' width='100%' flexGrow={1}>
      {children ? children : <Outlet />}
    </Box>
  </Box>
);

export default MainBarLayout;
