import React from 'react';

import { Box } from '@mui/material';

import { ThemeProps } from '../ThemeProps';
import { useGraphSxTheme } from '../../../hooks/theme/useGraphSxTheme';

const GraphTheme: React.FC<ThemeProps> = ({ children }) => (
  <Box
    className='graph-theme'
    width='100%'
    height='100%'
    sx={useGraphSxTheme()}>
    {children}
  </Box>
);

export default GraphTheme;
