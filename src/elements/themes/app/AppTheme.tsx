import React from 'react';
import { Outlet } from 'react-router-dom';

import { ThemeProvider } from '@mui/material';

import { ThemeProps } from '../ThemeProps';
import { useAppTheme } from '../../../hooks/theme/useAppTheme';

const AppTheme: React.FC<ThemeProps> = ({ children }) => (
  <ThemeProvider theme={useAppTheme()}>
    {children ? children : <Outlet />}
  </ThemeProvider>
);

export default AppTheme;
