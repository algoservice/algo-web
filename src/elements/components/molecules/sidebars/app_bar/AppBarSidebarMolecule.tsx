import React from 'react';
import { useSelector } from 'react-redux';

import { Divider, Drawer, Grid } from '@mui/material';

import { RootState } from '../../../../../redux/reducers/rootReducer';
import { useRootDispatch } from '../../../../../redux/dispatch/rootDispatch';
import { closeAppBarSidebar } from '../../../../../redux/actions/app/closeAppBarSidebar';
import { switchThemeMode } from '../../../../../redux/actions/theme/switchThemeMode';

import { ComponentProps } from '../../../ComponentProps';
import ThemeModeIconButtonAtom from '../../../atoms/buttons/icon/theme_mode/ThemeModeIconButtonAtom';
import CloseIconButtonAtom from '../../../atoms/buttons/icon/close/CloseIconButtonAtom';
import CreateGraphMenuMolecule from '../../menus/create_graph/CreateGraphMenuMolecule';

const AppBarSidebarMolecule: React.FC<ComponentProps> = ({ className }) => {
  const dispatch = useRootDispatch();

  const { isAppBarOpened } = useSelector(
    (state: RootState) => state.appReducer,
  );

  return (
    <Drawer
      anchor='right'
      className={className}
      open={isAppBarOpened}
      onClose={() => dispatch(closeAppBarSidebar())}>
      <Grid
        className={`${className}__content`}
        container
        rowSpacing={1}
        width={{ xs: '80vw', sm: '300px' }}
        margin='0'>
        <Grid item className={`${className}__header`} container direction='row'>
          <Grid item xs />
          <Grid item xs={2.4} marginTop='2px'>
            <CreateGraphMenuMolecule
              className={`${className}__create-graph`}
              onNavigateSideEffect={() => dispatch(closeAppBarSidebar())}
            />
          </Grid>
          <Grid item xs={1.8}>
            <ThemeModeIconButtonAtom
              className={`${className}__theme-button`}
              onClick={() => dispatch(switchThemeMode())}
            />
          </Grid>
          <Grid item xs={1.8}>
            <CloseIconButtonAtom
              className={`${className}__close-button`}
              onClick={() => dispatch(closeAppBarSidebar())}
            />
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Divider />
        </Grid>
      </Grid>
    </Drawer>
  );
};

export default AppBarSidebarMolecule;
