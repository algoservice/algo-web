import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Divider,
  Stack,
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

import { useRootDispatch } from '../../../../../redux/dispatch/rootDispatch';
import { RootState } from '../../../../../redux/reducers/rootReducer';
import { closeControlsSidebar } from '../../../../../redux/actions/app/closeControlsSidebar';

import useGraph from '../../../../../hooks/graph/useGraph';
import useInternalGraphScheme from '../../../../../hooks/internal_graph/useInternalGraphScheme';
import useInternalGraph from '../../../../../hooks/internal_graph/useInternalGraph';
import useInternalGraphInfo from '../../../../../hooks/internal_graph/useInternalGraphInfo';

import { SidebarMoleculeProps } from '../SidebarMoleculeProps';
import SideBarAtom from '../../../atoms/sidebars/sidebar/SideBarAtom';
import SidebarHeaderAtom from '../../../atoms/sidebars/header/SidebarHeaderAtom';
import SaveButtonAtom from '../../../atoms/buttons/regular/save/SaveButtonAtom';
import SinglelineTextInputAtom from '../../../atoms/inputs/text/singleline/SinglelineTextInputAtom';
import { TextInputAtomOnChange } from '../../../atoms/inputs/text/TextInputAtomProps';
import TypographyAtom from '../../../atoms/typography/default/TypographyAtom';
import GraphNodeCardMolecule, {
  GraphNodeCardImages,
} from '../../cards/graph/GraphNodeCardMolecule';

import { en } from '../../../../../constants/l10n/en';
import { NODES } from '../../../../../graph/constants/Nodes';

const ControlsSidebarMolecule: React.FC<SidebarMoleculeProps> = ({
  className,
  side,
  size,
  parentRef,
}) => {
  const dispatch = useRootDispatch();
  const { saveGraph } = useGraph();
  const { graph } = useInternalGraph();
  const { addNodeToGraph } = useInternalGraphScheme();
  const { graphInfo, updateGraphName } = useInternalGraphInfo();

  const { isControlsOpened } = useSelector(
    (state: RootState) => state.appReducer,
  );

  const [isHighlightGraphNameInput, setIsHighlightGraphNameInput] =
    useState<boolean>(false);
  const [graphNameInputValue, setGraphNameInputValue] =
    useState<TextInputAtomOnChange>({ value: graphInfo.name, isError: true });

  const onSave = () => {
    if (graphNameInputValue.isError) {
      setIsHighlightGraphNameInput(true);
    } else {
      saveGraph(graph);
    }
  };

  return (
    <SideBarAtom
      className={`${className}__controls-sidebar`}
      open={isControlsOpened}
      side={side}
      size={size}
      parentRef={parentRef}
      header={
        <>
          <Box
            className='controls-sidebar__header-wrapper'
            paddingX={2}
            paddingY={1}>
            <SidebarHeaderAtom
              className='controls-sidebar__header'
              heading={en.graph.editor.sidebar.controls.heading}
              onClick={() => dispatch(closeControlsSidebar())}
            />
          </Box>
          <Divider />
        </>
      }>
      <Stack direction='column' alignItems='stretch' padding={2} spacing={2}>
        <Box className='controls-sidebar__accordions-wrapper' component='div'>
          <Accordion
            className='controls-sidebar__controls-accordion'
            defaultExpanded>
            <AccordionSummary
              className='controls-accordion__summary'
              expandIcon={<ExpandMoreIcon />}>
              <TypographyAtom
                className='controls-accordion__heading'
                error={isHighlightGraphNameInput}
                variant='h6'
                component='h6'>
                {en.graph.editor.sidebar.controls.sections.general.heading}
              </TypographyAtom>
            </AccordionSummary>
            <AccordionDetails className='controls-accordion__details'>
              <SinglelineTextInputAtom
                className='controls-accordion__graph-name'
                required
                value={graphNameInputValue.value}
                error={isHighlightGraphNameInput}
                label={
                  en.graph.editor.sidebar.controls.sections.general.nameInput
                    .label
                }
                hintOnEmpty={
                  en.graph.editor.sidebar.controls.sections.general.nameInput
                    .hintOnEmpty
                }
                maxLength={100}
                onChange={(newValue: TextInputAtomOnChange) => {
                  setGraphNameInputValue(newValue);
                  if (!newValue.isError) {
                    updateGraphName(newValue.value);
                    setIsHighlightGraphNameInput(false);
                  }
                }}
                sx={{ width: '100%' }}
              />
            </AccordionDetails>
          </Accordion>
          <Accordion className='controls-sidebar__components-accordion'>
            <AccordionSummary
              className='components-accordion__summary'
              expandIcon={<ExpandMoreIcon />}>
              <TypographyAtom
                className='components-accordion__heading'
                variant='h6'
                component='h6'>
                {en.graph.editor.sidebar.controls.sections.elements.heading}
              </TypographyAtom>
            </AccordionSummary>
            <AccordionDetails className='components-accordion__details'>
              <Stack direction='column' alignItems='stretch' spacing={2}>
                <GraphNodeCardMolecule
                  className='components-accordion__output-node-card'
                  image={GraphNodeCardImages.GRAPH_OUTPUT_NODE}
                  heading={NODES.output.name}
                  onClick={() => addNodeToGraph(NODES.output.type)}
                />
                <GraphNodeCardMolecule
                  className='components-accordion__transput-node-card'
                  image={GraphNodeCardImages.GRAPH_TRANSPUT_NODE}
                  heading={NODES.default.name}
                  onClick={() => addNodeToGraph(NODES.default.type)}
                />
                <GraphNodeCardMolecule
                  className='components-accordion__input-node-card'
                  image={GraphNodeCardImages.GRAPH_INPUT_NODE}
                  heading={NODES.input.name}
                  onClick={() => addNodeToGraph(NODES.input.type)}
                />
              </Stack>
            </AccordionDetails>
          </Accordion>
        </Box>
        <Divider />
        <SaveButtonAtom
          className='controls-sidebar__save-button'
          error={isHighlightGraphNameInput}
          text={en.graph.editor.sidebar.controls.buttons.save.text}
          onClick={onSave}
        />
      </Stack>
    </SideBarAtom>
  );
};

export default ControlsSidebarMolecule;
