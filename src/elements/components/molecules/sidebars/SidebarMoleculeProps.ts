import React from 'react';

import { ComponentProps } from '../../ComponentProps';
import { SideBarPosition } from '../../atoms/sidebars/sidebar/SideBarAtom';

export type SidebarMoleculeProps = ComponentProps & {
  side: SideBarPosition;
  size: string;
  parentRef: React.RefObject<HTMLDivElement>;
};
