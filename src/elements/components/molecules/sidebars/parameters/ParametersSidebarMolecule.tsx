import React from 'react';
import { useSelector } from 'react-redux';

import { Box, Divider, Stack } from '@mui/material';

import { useRootDispatch } from '../../../../../redux/dispatch/rootDispatch';
import { RootState } from '../../../../../redux/reducers/rootReducer';
import { closeParametersSidebar } from '../../../../../redux/actions/app/closeParametersSidebar';

import { SidebarMoleculeProps } from '../SidebarMoleculeProps';
import SideBarAtom from '../../../atoms/sidebars/sidebar/SideBarAtom';
import SidebarHeaderAtom from '../../../atoms/sidebars/header/SidebarHeaderAtom';

import { en } from '../../../../../constants/l10n/en';
import useInternalGraphScheme from '../../../../../hooks/internal_graph/useInternalGraphScheme';
import isGraphNode from '../../../../../graph/utils/guards/isGraphNode';
import NodeGraphLayoutMolecule from '../../layouts/graph/node/NodeGraphLayoutMolecule';
import EmptyGraphLayoutMolecule from '../../layouts/graph/empty/EmptyGraphLayoutMolecule';
import isGraphEdge from '../../../../../graph/utils/guards/isGraphEdge';
import EdgeGraphLayoutMolecule from '../../layouts/graph/edge/EdgeGraphLayoutMolecule';

const ParametersSidebarMolecule: React.FC<SidebarMoleculeProps> = ({
  className,
  side,
  size,
  parentRef,
}) => {
  const dispatch = useRootDispatch();

  const { isParametersOpened } = useSelector(
    (state: RootState) => state.appReducer,
  );

  const { selectedGraphElement } = useInternalGraphScheme();

  const renderParametersForSelectedElement = () => {
    if (isGraphNode(selectedGraphElement)) {
      return <NodeGraphLayoutMolecule className='parameters-sidebar__node' />;
    }
    if (isGraphEdge(selectedGraphElement)) {
      return <EdgeGraphLayoutMolecule className='parameters-sidebar__edge' />;
    }
  };

  const renderParametersLayout = () => {
    if (!selectedGraphElement) {
      return (
        <EmptyGraphLayoutMolecule className='parameters-sidebar__no-element' />
      );
    }
    return (
      <Stack height='100%' direction='column' alignItems='stretch' spacing={2}>
        {renderParametersForSelectedElement()}
      </Stack>
    );
  };

  return (
    <SideBarAtom
      className={`${className}__parameters-sidebar`}
      open={isParametersOpened}
      side={side}
      size={size}
      parentRef={parentRef}
      header={
        <>
          <Box
            className='parameters-sidebar__header-wrapper'
            paddingX={2}
            paddingY={1}>
            <SidebarHeaderAtom
              className='parameters-sidebar__header'
              heading={en.graph.editor.sidebar.parameters.heading}
              onClick={() => dispatch(closeParametersSidebar())}
            />
          </Box>
          <Divider />
        </>
      }>
      {renderParametersLayout()}
    </SideBarAtom>
  );
};

export default ParametersSidebarMolecule;
