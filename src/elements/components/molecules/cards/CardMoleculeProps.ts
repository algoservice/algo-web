import { ReactNode } from 'react';

import { ComponentProps } from '../../ComponentProps';

export type CardMoleculeProps = ComponentProps & {
  children?: ReactNode;
  onClick?: () => void;
};
