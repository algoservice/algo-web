import React from 'react';

import { Card, CardActionArea, CardContent, Skeleton } from '@mui/material';

import { CardMoleculeProps } from '../CardMoleculeProps';
import { GraphData } from '../../../../../graph/types/data/GraphData';
import TypographyAtom from '../../../atoms/typography/default/TypographyAtom';

type GraphCardMoleculeProps = CardMoleculeProps & {
  graphInfo: GraphData;
  loading?: boolean;
};

const GraphCardMolecule: React.FC<GraphCardMoleculeProps> = ({
  className,
  onClick,
  graphInfo,
  loading = false,
}) => (
  <Card className={className}>
    <CardActionArea className={`${className}__action-area`} onClick={onClick}>
      <CardContent className={`${className}__content`}>
        <TypographyAtom className={`${className}__graph-name`}>
          {loading ? <Skeleton /> : graphInfo.name}
        </TypographyAtom>
      </CardContent>
    </CardActionArea>
  </Card>
);

export default GraphCardMolecule;
