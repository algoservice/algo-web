import React from 'react';
import { useSelector } from 'react-redux';

import { Card, CardActionArea, CardContent, CardMedia } from '@mui/material';

import { RootState } from '../../../../../redux/reducers/rootReducer';
import { CardMoleculeProps } from '../CardMoleculeProps';
import TypographyAtom from '../../../atoms/typography/default/TypographyAtom';

import {
  AppPaletteMode,
  DarkMode,
  LightMode,
} from '../../../../../constants/palettes/types/AppPaletteMode';
import { publicFiles } from '../../../../../constants/public/files';

export enum GraphNodeCardImages {
  GRAPH_INPUT_NODE,
  GRAPH_OUTPUT_NODE,
  GRAPH_TRANSPUT_NODE,
}

const GraphNodeCardThemedImages = new Map<
  AppPaletteMode,
  Map<GraphNodeCardImages, string>
>([
  [
    LightMode,
    new Map<GraphNodeCardImages, string>([
      [
        GraphNodeCardImages.GRAPH_INPUT_NODE,
        publicFiles.static.images.LightGraphInputNode,
      ],
      [
        GraphNodeCardImages.GRAPH_OUTPUT_NODE,
        publicFiles.static.images.LightGraphOutputNode,
      ],
      [
        GraphNodeCardImages.GRAPH_TRANSPUT_NODE,
        publicFiles.static.images.LightGraphTransputNode,
      ],
    ]),
  ],
  [
    DarkMode,
    new Map<GraphNodeCardImages, string>([
      [
        GraphNodeCardImages.GRAPH_INPUT_NODE,
        publicFiles.static.images.DarkGraphInputNode,
      ],
      [
        GraphNodeCardImages.GRAPH_OUTPUT_NODE,
        publicFiles.static.images.DarkGraphOutputNode,
      ],
      [
        GraphNodeCardImages.GRAPH_TRANSPUT_NODE,
        publicFiles.static.images.DarkGraphTransputNode,
      ],
    ]),
  ],
]);

type GraphNodeCardMoleculeProps = CardMoleculeProps & {
  image: GraphNodeCardImages;
  heading: string;
};

const GraphNodeCardMolecule: React.FC<GraphNodeCardMoleculeProps> = ({
  className,
  onClick,
  image,
  heading,
}) => {
  const { mode } = useSelector((state: RootState) => state.themeReducer);

  return (
    <Card className={className}>
      <CardActionArea className={`${className}__action-area`} onClick={onClick}>
        <CardMedia
          className={`${className}__media`}
          component='img'
          height='100px'
          image={GraphNodeCardThemedImages.get(mode)?.get(image)}
        />
        <CardContent className={`${className}__content`}>
          <TypographyAtom className={`${className}__heading`}>
            {heading}
          </TypographyAtom>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default GraphNodeCardMolecule;
