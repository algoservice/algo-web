import React from 'react';

import { Box } from '@mui/material';

import useNavigation from '../../../../../hooks/navigation/useNavigation';
import useInternalGraphControls from '../../../../../hooks/internal_graph/useInternalGraphControls';
import useInternalGraph from '../../../../../hooks/internal_graph/useInternalGraph';

import { ComponentProps } from '../../../ComponentProps';
import CreateMenuButtonAtom from '../../../atoms/buttons/regular/create_menu/CreateMenuButtonAtom';
import CreateGraphMenuAtom from '../../../atoms/menus/create_graph/CreateGraphMenuAtom';

type CreateGraphMenuMoleculeProps = ComponentProps & {
  onNavigateSideEffect?: () => void;
};
const CreateGraphMenuMolecule: React.FC<CreateGraphMenuMoleculeProps> = ({
  className,
  onNavigateSideEffect,
}) => {
  const navigation = useNavigation();
  const { unloadGraph } = useInternalGraph();
  const { centerGraph } = useInternalGraphControls();

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const navigateToEditor = () => {
    unloadGraph();
    centerGraph();
    handleClose();
    if (onNavigateSideEffect) {
      onNavigateSideEffect();
    }
    navigation.toEditor();
  };

  return (
    <Box className={`${className}__create-graph`} padding={0} margin={0}>
      <CreateMenuButtonAtom
        className='create-graph__button'
        onClick={handleClick}
      />
      <CreateGraphMenuAtom
        className='create-graph__menu'
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        onClickNew={() => navigateToEditor()}
        onClickImport={() => navigateToEditor()}
      />
    </Box>
  );
};

export default CreateGraphMenuMolecule;
