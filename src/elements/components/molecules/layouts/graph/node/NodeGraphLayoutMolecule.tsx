import React, { useEffect, useState } from 'react';

import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Divider,
  Stack,
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

import useInternalGraphScheme from '../../../../../../hooks/internal_graph/useInternalGraphScheme';
import useInternalGraphSchemeValues from '../../../../../../hooks/internal_graph/useInternalGraphSchemeValues';

import isGraphNode from '../../../../../../graph/utils/guards/isGraphNode';
import { GraphNodeId } from '../../../../../../graph/types/node/GraphNodeId';
import {
  DefaultNodeType,
  GraphNodeType,
  InputNodeType,
  OutputNodeType,
} from '../../../../../../graph/types/node/GraphNodeType';
import { ComponentProps } from '../../../../ComponentProps';
import TypographyAtom from '../../../../atoms/typography/default/TypographyAtom';
import SinglelineTextInputAtom from '../../../../atoms/inputs/text/singleline/SinglelineTextInputAtom';
import { TextInputAtomOnChange } from '../../../../atoms/inputs/text/TextInputAtomProps';
import NumberInputAtom from '../../../../atoms/inputs/number/NumberInputAtom';
import SingleSelectAtom from '../../../../atoms/selects/single/SingleSelectAtom';
import { NumberInputAtomOnChange } from '../../../../atoms/inputs/number/NumberInputAtomProps';
import { SelectAtomValueOption } from '../../../../atoms/selects/SelectAtomProps';
import MultiSelectAtom from '../../../../atoms/selects/multi/MultiSelectAtom';
import DeleteButtonAtom from '../../../../atoms/buttons/regular/delete/DeleteButtonAtom';
import { MultiSelectValueOnChange } from '../../../../atoms/selects/multi/MultiSelectAtomProps';

import { en } from '../../../../../../constants/l10n/en';
import { NODES } from '../../../../../../graph/constants/Nodes';

const graphNodeTypes: SelectAtomValueOption<GraphNodeType>[] = [
  {
    label: NODES.output.name,
    value: NODES.output.type,
  },
  {
    label: NODES.default.name,
    value: NODES.default.type,
  },
  {
    label: NODES.input.name,
    value: NODES.input.type,
  },
];

const NodeGraphLayoutMolecule: React.FC<ComponentProps> = ({ className }) => {
  const {
    selectedGraphElement,
    updateSelectedGraphElement,
    changeNodeType,
    addEdgeToGraph,
    removeEdgeFromGraph,
    deleteSelectedGraphElement,
  } = useInternalGraphScheme();
  const {
    selectedNodeConnectedToInput,
    selectedNodeAvailableInput,
    selectedNodeConnectedFromOutput,
    selectedNodeAvailableOutput,
  } = useInternalGraphSchemeValues();

  const [isHighlightNodeDataInput, setIsHighlightNodeDataInput] =
    useState<boolean>(false);
  const [selectedNodeInputConnections, setSelectedNodeInputConnections] =
    useState<GraphNodeId[]>([]);
  const [selectedNodeOutputConnections, setSelectedNodeOutputConnections] =
    useState<GraphNodeId[]>([]);

  useEffect(() => {
    setSelectedNodeInputConnections(
      selectedNodeConnectedToInput.map((node) => node.id),
    );
  }, [selectedNodeConnectedToInput]);

  useEffect(() => {
    setSelectedNodeOutputConnections(
      selectedNodeConnectedFromOutput.map((node) => node.id),
    );
  }, [selectedNodeConnectedFromOutput]);

  const onUpdateNodeData = (newValue: TextInputAtomOnChange) => {
    if (isGraphNode(selectedGraphElement)) {
      updateSelectedGraphElement({
        ...selectedGraphElement,
        data: {
          ...selectedGraphElement.data,
          label: newValue.value,
        },
      });
    }
    if (!newValue.isError) {
      setIsHighlightNodeDataInput(false);
    }
  };

  const onUpdateNodeX = (newValue: NumberInputAtomOnChange) => {
    if (isGraphNode(selectedGraphElement)) {
      updateSelectedGraphElement({
        ...selectedGraphElement,
        position: {
          ...selectedGraphElement.position,
          x: Number(newValue.value),
        },
      });
    }
  };

  const onUpdateNodeY = (newValue: NumberInputAtomOnChange) => {
    if (isGraphNode(selectedGraphElement)) {
      updateSelectedGraphElement({
        ...selectedGraphElement,
        position: {
          ...selectedGraphElement.position,
          y: Number(newValue.value),
        },
      });
    }
  };

  const onUpdateNodeType = (newValue: string) => {
    if (isGraphNode(selectedGraphElement)) {
      changeNodeType(selectedGraphElement, newValue as GraphNodeType);
    }
  };

  const onDeleteButtonClick = () => {
    deleteSelectedGraphElement();
  };

  const onInputConnectionsChange = (
    newConnections: MultiSelectValueOnChange<GraphNodeId>,
  ) => {
    if (isGraphNode(selectedGraphElement)) {
      if (newConnections.newValue) {
        addEdgeToGraph(newConnections.newValue, selectedGraphElement.id);
      }
      if (newConnections.removedValue) {
        removeEdgeFromGraph(
          `${newConnections.removedValue}-${selectedGraphElement.id}`,
        );
      }
      setSelectedNodeInputConnections(newConnections.values);
    }
  };

  const onOutputConnectionsChange = (
    newConnections: MultiSelectValueOnChange<GraphNodeId>,
  ) => {
    if (isGraphNode(selectedGraphElement)) {
      if (newConnections.newValue) {
        addEdgeToGraph(selectedGraphElement.id, newConnections.newValue);
      }
      if (newConnections.removedValue) {
        removeEdgeFromGraph(
          `${selectedGraphElement.id}-${newConnections.removedValue}`,
        );
      }
      setSelectedNodeOutputConnections(newConnections.values);
    }
  };

  const renderNodeInputConnectionSelector = () => {
    if (
      isGraphNode(selectedGraphElement) &&
      (selectedGraphElement.type === OutputNodeType ||
        selectedGraphElement.type === DefaultNodeType)
    ) {
      const valueOptions = selectedNodeAvailableInput.map((node) => ({
        label: node.data.label,
        value: node.id,
      }));
      return (
        <MultiSelectAtom
          className='node-graph-layout__input-connection-selectors'
          label={
            en.graph.editor.sidebar.parameters.type.node.sections.connection
              .input
          }
          disabled={valueOptions.length === 0}
          value={selectedNodeInputConnections}
          valueOptions={valueOptions}
          onChange={onInputConnectionsChange}
        />
      );
    }
  };

  const renderNodeOutputConnectionSelector = () => {
    if (
      isGraphNode(selectedGraphElement) &&
      (selectedGraphElement.type === InputNodeType ||
        selectedGraphElement.type === DefaultNodeType)
    ) {
      const valueOptions = selectedNodeAvailableOutput.map((node) => ({
        label: node.data.label,
        value: node.id,
      }));
      return (
        <MultiSelectAtom
          className='node-graph-layout__output-connection-selectors'
          label={
            en.graph.editor.sidebar.parameters.type.node.sections.connection
              .output
          }
          disabled={valueOptions.length === 0}
          value={selectedNodeOutputConnections}
          valueOptions={valueOptions}
          onChange={onOutputConnectionsChange}
        />
      );
    }
  };

  return (
    <Stack
      className={`${className} node-graph-layout__wrapper`}
      spacing={2}
      padding={2}>
      <Box className={`${className} node-graph-layout__accordion-wrapper`}>
        <Accordion
          className='node-graph-layout__data-accordeon'
          defaultExpanded>
          <AccordionSummary
            className='data-accordeon__summary'
            expandIcon={<ExpandMoreIcon />}>
            <TypographyAtom
              className='data-accordeon__heading'
              error={isHighlightNodeDataInput}
              variant='h6'
              component='h6'>
              {
                en.graph.editor.sidebar.parameters.type.node.sections.data
                  .heading
              }
            </TypographyAtom>
          </AccordionSummary>
          <AccordionDetails className='data-accordeon__details'>
            <SinglelineTextInputAtom
              className='data-accordion__node-data-label'
              required
              value={
                isGraphNode(selectedGraphElement)
                  ? selectedGraphElement.data.label
                  : ''
              }
              error={isHighlightNodeDataInput}
              label={
                en.graph.editor.sidebar.parameters.type.node.sections.data
                  .labelInput.label
              }
              hintOnEmpty={
                en.graph.editor.sidebar.parameters.type.node.sections.data
                  .labelInput.hintOnEmpty
              }
              maxLength={100}
              onChange={onUpdateNodeData}
              sx={{ width: '100%' }}
            />
          </AccordionDetails>
        </Accordion>
        <Accordion
          className='node-graph-layout__position-accordeon'
          defaultExpanded>
          <AccordionSummary
            className='position-accordeon__summary'
            expandIcon={<ExpandMoreIcon />}>
            <TypographyAtom
              className='position-accordeon__heading'
              variant='h6'
              component='h6'>
              {
                en.graph.editor.sidebar.parameters.type.node.sections.position
                  .heading
              }
            </TypographyAtom>
          </AccordionSummary>
          <AccordionDetails className='position-accordeon__details'>
            <Stack
              className='position-accordeon__details-stack'
              direction='column'
              spacing={2}>
              <NumberInputAtom
                className='position-accordion__node-x'
                value={
                  isGraphNode(selectedGraphElement)
                    ? selectedGraphElement.position.x
                    : 0
                }
                label={
                  en.graph.editor.sidebar.parameters.type.node.sections.position
                    .xInput.label
                }
                integer
                onChange={onUpdateNodeX}
              />
              <NumberInputAtom
                className='position-accordion__node-y'
                value={
                  isGraphNode(selectedGraphElement)
                    ? selectedGraphElement.position.y
                    : 0
                }
                label={
                  en.graph.editor.sidebar.parameters.type.node.sections.position
                    .yInput.label
                }
                integer
                onChange={onUpdateNodeY}
              />
            </Stack>
          </AccordionDetails>
        </Accordion>
        <Accordion
          className='node-graph-layout__connection-accordeon'
          defaultExpanded>
          <AccordionSummary
            className='connection-accordeon__summary'
            expandIcon={<ExpandMoreIcon />}>
            <TypographyAtom
              className='connection-accordeon__heading'
              variant='h6'
              component='h6'>
              {
                en.graph.editor.sidebar.parameters.type.node.sections.connection
                  .heading
              }
            </TypographyAtom>
          </AccordionSummary>
          <AccordionDetails className='connection-accordeon__details'>
            <Stack
              className='connection-accordeon__details-stack'
              direction='column'
              spacing={2}>
              <SingleSelectAtom
                className='connection-accordeon__node-type'
                required
                label={
                  en.graph.editor.sidebar.parameters.type.node.sections
                    .connection.typeSelector.label
                }
                value={
                  isGraphNode(selectedGraphElement)
                    ? selectedGraphElement.type
                    : ''
                }
                valueOptions={graphNodeTypes}
                onChange={onUpdateNodeType}
              />
              {renderNodeInputConnectionSelector()}
              {renderNodeOutputConnectionSelector()}
            </Stack>
          </AccordionDetails>
        </Accordion>
      </Box>
      <Divider />
      <DeleteButtonAtom
        className='node-graph-layout__delete-node-button'
        text={en.graph.editor.sidebar.parameters.type.node.delete}
        onClick={onDeleteButtonClick}
      />
    </Stack>
  );
};

export default NodeGraphLayoutMolecule;
