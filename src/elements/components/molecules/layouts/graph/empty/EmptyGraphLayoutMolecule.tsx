import React from 'react';

import { Box, Stack } from '@mui/material';
import LayersIcon from '@mui/icons-material/Layers';

import { ComponentProps } from '../../../../ComponentProps';
import TypographyAtom from '../../../../atoms/typography/default/TypographyAtom';

import { en } from '../../../../../../constants/l10n/en';

const EmptyGraphLayoutMolecule: React.FC<ComponentProps> = ({ className }) => (
  <Box
    className={`${className} empty-graph-layout__wrapper`}
    width='100%'
    height='100%'
    display='flex'
    alignItems='center'
    justifyContent='center'>
    <Stack
      className='empty-graph-layout__content'
      direction='column'
      alignItems='center'
      paddingX={2}
      spacing={2}>
      <LayersIcon
        sx={{ width: '30%', height: '30%' }}
        className='empty-graph-layout__icon'
      />
      <TypographyAtom
        className='empty-graph-layout__hint'
        variant='h6'
        align='center'>
        {en.graph.editor.sidebar.parameters.type.empty.hint}
      </TypographyAtom>
    </Stack>
  </Box>
);

export default EmptyGraphLayoutMolecule;
