import React from 'react';

import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Divider,
  Stack,
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

import useInternalGraphScheme from '../../../../../../hooks/internal_graph/useInternalGraphScheme';
import useInternalGraphSchemeValues from '../../../../../../hooks/internal_graph/useInternalGraphSchemeValues';

import { ComponentProps } from '../../../../ComponentProps';
import { GraphNodeId } from '../../../../../../graph/types/node/GraphNodeId';
import isGraphEdge from '../../../../../../graph/utils/guards/isGraphEdge';
import TypographyAtom from '../../../../atoms/typography/default/TypographyAtom';
import SinglelineTextInputAtom from '../../../../atoms/inputs/text/singleline/SinglelineTextInputAtom';
import { TextInputAtomOnChange } from '../../../../atoms/inputs/text/TextInputAtomProps';
import SingleSelectAtom from '../../../../atoms/selects/single/SingleSelectAtom';
import DeleteButtonAtom from '../../../../atoms/buttons/regular/delete/DeleteButtonAtom';

import { en } from '../../../../../../constants/l10n/en';

const EdgeGraphLayoutMolecule: React.FC<ComponentProps> = ({ className }) => {
  const {
    selectedGraphElement,
    updateSelectedGraphElement,
    reconnectEdge,
    deleteSelectedGraphElement,
  } = useInternalGraphScheme();
  const { selectedEdgeAvailableSource, selectedEdgeAvailableTarget } =
    useInternalGraphSchemeValues();

  const onUpdateEdgeData = (newValue: TextInputAtomOnChange) => {
    if (isGraphEdge(selectedGraphElement)) {
      updateSelectedGraphElement({
        ...selectedGraphElement,
        label: newValue.value,
      });
    }
  };

  const onUpdateSourceOfEdge = (newSource: GraphNodeId) => {
    if (isGraphEdge(selectedGraphElement)) {
      reconnectEdge(
        selectedGraphElement,
        newSource,
        selectedGraphElement.target,
      );
    }
  };

  const onUpdateTargetOfEdge = (newTarget: GraphNodeId) => {
    if (isGraphEdge(selectedGraphElement)) {
      reconnectEdge(
        selectedGraphElement,
        selectedGraphElement.source,
        newTarget,
      );
    }
  };

  const onDeleteButtonClick = () => {
    deleteSelectedGraphElement();
  };

  return (
    <Stack
      className={`${className} edge-graph-layout__wrapper`}
      spacing={2}
      padding={2}>
      <Box className={`${className} edge-graph-layout__accordion-wrapper`}>
        <Accordion
          className='edge-graph-layout__data-accordeon'
          defaultExpanded>
          <AccordionSummary
            className='data-accordeon__summary'
            expandIcon={<ExpandMoreIcon />}>
            <TypographyAtom
              className='data-accordeon__heading'
              variant='h6'
              component='h6'>
              {
                en.graph.editor.sidebar.parameters.type.edge.sections.data
                  .heading
              }
            </TypographyAtom>
          </AccordionSummary>
          <AccordionDetails className='data-accordeon__details'>
            <SinglelineTextInputAtom
              className='data-accordion__edge-data-label'
              value={
                isGraphEdge(selectedGraphElement)
                  ? selectedGraphElement.label
                  : ''
              }
              label={
                en.graph.editor.sidebar.parameters.type.edge.sections.data
                  .labelInput.label
              }
              maxLength={100}
              onChange={onUpdateEdgeData}
              sx={{ width: '100%' }}
            />
          </AccordionDetails>
        </Accordion>
        <Accordion
          className='edge-graph-layout__connection-accordeon'
          defaultExpanded>
          <AccordionSummary
            className='connection-accordeon__summary'
            expandIcon={<ExpandMoreIcon />}>
            <TypographyAtom
              className='connection-accordeon__heading'
              variant='h6'
              component='h6'>
              {
                en.graph.editor.sidebar.parameters.type.edge.sections.connection
                  .heading
              }
            </TypographyAtom>
          </AccordionSummary>
          <AccordionDetails className='connection-accordeon__details'>
            <Stack
              className='connection-accordeon__details-stack'
              direction='column'
              spacing={2}>
              <SingleSelectAtom
                className='connection-accordeon__source-selector'
                required
                label={
                  en.graph.editor.sidebar.parameters.type.edge.sections
                    .connection.source.label
                }
                value={
                  isGraphEdge(selectedGraphElement)
                    ? selectedGraphElement.source
                    : ''
                }
                valueOptions={selectedEdgeAvailableSource.map((node) => ({
                  label: node.data.label,
                  value: node.id,
                }))}
                onChange={onUpdateSourceOfEdge}
              />
              <SingleSelectAtom
                className='connection-accordeon__target-selector'
                required
                label={
                  en.graph.editor.sidebar.parameters.type.edge.sections
                    .connection.target.label
                }
                value={
                  isGraphEdge(selectedGraphElement)
                    ? selectedGraphElement.target
                    : ''
                }
                valueOptions={selectedEdgeAvailableTarget.map((node) => ({
                  label: node.data.label,
                  value: node.id,
                }))}
                onChange={onUpdateTargetOfEdge}
              />
            </Stack>
          </AccordionDetails>
        </Accordion>
      </Box>
      <Divider />
      <DeleteButtonAtom
        className='edge-graph-layout__delete-edge-button'
        text={en.graph.editor.sidebar.parameters.type.edge.delete}
        onClick={onDeleteButtonClick}
      />
    </Stack>
  );
};

export default EdgeGraphLayoutMolecule;
