import { SxProps } from '@mui/material';

export type ComponentProps = {
  className: string;
  sx?: SxProps;
};
