import React from 'react';

import { AppBar, Link, Stack, Toolbar } from '@mui/material';

import { openAppBarSidebar } from '../../../../redux/actions/app/openAppBarSidebar';
import { useRootDispatch } from '../../../../redux/dispatch/rootDispatch';
import { switchThemeMode } from '../../../../redux/actions/theme/switchThemeMode';

import useNavigation from '../../../../hooks/navigation/useNavigation';

import ThemeModeIconButtonAtom from '../../atoms/buttons/icon/theme_mode/ThemeModeIconButtonAtom';

import LogoIconButtonAtom from '../../atoms/buttons/icon/logo/LogoIconButtonAtom';
import MenuIconButtonAtom from '../../atoms/buttons/icon/menu/MenuIconButtonAtom';
import AppBarSidebarMolecule from '../../molecules/sidebars/app_bar/AppBarSidebarMolecule';
import CreateGraphMenuMolecule from '../../molecules/menus/create_graph/CreateGraphMenuMolecule';

import { en } from '../../../../constants/l10n/en';

type AppBarOrganismProps = {
  elevated?: boolean;
};

const AppBarOrganism: React.FC<AppBarOrganismProps> = ({ elevated }) => {
  const dispatch = useRootDispatch();

  const navigation = useNavigation();

  const position = elevated ? 'relative' : 'fixed';

  const renderToolbarLogoStack = () => (
    <Stack
      className='toolbar__logo'
      direction='row'
      spacing={1}
      alignItems='center'
      color='inherit'
      flexGrow={1}>
      <LogoIconButtonAtom
        className='toolbar__logo-icon'
        onClick={() => navigation.toHome()}
      />
      <Link
        className='toolbar__logo-text'
        component='button'
        underline='none'
        color='text.primary'
        variant='h6'
        textTransform='uppercase'
        display={{ xs: 'none', md: 'block' }}
        fontWeight={700}
        onClick={() => navigation.toHome()}>
        {en.serviceName}
      </Link>
    </Stack>
  );

  const renderButtonsOnWideScreen = () => (
    <Stack
      className='toolbar__buttons-wide'
      direction='row'
      display={{ xs: 'none', md: 'flex' }}>
      <CreateGraphMenuMolecule className='toolbar__create-graph' />
      <ThemeModeIconButtonAtom
        className='toolbar__theme-button'
        onClick={() => dispatch(switchThemeMode())}
      />
    </Stack>
  );

  const renderButtonsOnCompactScreen = () => (
    <MenuIconButtonAtom
      className='toolbar__menu-button'
      sx={{ display: { xs: 'flex', md: 'none' } }}
      onClick={() => dispatch(openAppBarSidebar())}
    />
  );

  const renderToolbarButtonsStack = () => (
    <Stack className='toolbar__buttons' direction='row' alignItems='center'>
      {renderButtonsOnWideScreen()}
      {renderButtonsOnCompactScreen()}
      <AppBarSidebarMolecule className='toolbar__menu' />
    </Stack>
  );

  return (
    <AppBar
      className='app-bar'
      position={position}
      enableColorOnDark
      component='nav'
      sx={{ backgroundColor: 'background.default' }}>
      <Toolbar className='app-bar__toolbar'>
        {renderToolbarLogoStack()}
        {renderToolbarButtonsStack()}
      </Toolbar>
    </AppBar>
  );
};

export default AppBarOrganism;
