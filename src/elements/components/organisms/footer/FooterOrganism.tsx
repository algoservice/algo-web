import React from 'react';

import { Grid, Link, Paper, Stack } from '@mui/material';

import useNavigation from '../../../../hooks/navigation/useNavigation';

import { en } from '../../../../constants/l10n/en';

import LogoIconButtonAtom from '../../atoms/buttons/icon/logo/LogoIconButtonAtom';
import TypographyAtom from '../../atoms/typography/default/TypographyAtom';

const FooterOrganism: React.FC<unknown> = () => {
  const navigation = useNavigation();

  return (
    <Paper
      className='footer'
      component='footer'
      variant='outlined'
      sx={{
        backgroundColor: 'background.default',
        paddingInline: '10%',
        paddingBlock: '5vh',
        borderRadius: 0,
      }}>
      <Grid className='footer__content' container alignItems='flex-start'>
        <Grid item container md={4} display={{ xs: 'none', md: 'block' }}>
          <Grid item md={12}>
            <Stack
              className='footer__copyright'
              direction='row'
              spacing={1}
              alignItems='center'>
              <LogoIconButtonAtom
                className='footer__copyright-logo'
                onClick={() => navigation.toHome()}
              />
              <Link
                className='footer__copyright-text'
                variant='subtitle1'
                component='button'
                underline='none'
                color='text.primary'
                onClick={() => navigation.toHome()}>
                {en.footer.copyright.title}
              </Link>
            </Stack>
          </Grid>
          <Grid item md={12} display={{ xs: 'none', md: 'block' }}>
            <TypographyAtom
              className='footer__dev'
              variant='body2'
              marginLeft='48px'>
              {en.footer.dev.createdBy} {en.footer.dev.creator}
            </TypographyAtom>
          </Grid>
        </Grid>
        <Grid item container spacing={4} xs={12} md={8} paddingTop='8px'>
          <Grid item xs={6}>
            <Stack
              className='footer__links'
              direction='column'
              alignItems='flex-start'
              spacing={2}>
              <Link
                className='footer__feedback-link'
                underline='hover'
                component='button'>
                {en.footer.links.feedback}
              </Link>
              <Link
                className='footer__license-link'
                underline='hover'
                component='button'>
                {en.footer.links.license}
              </Link>
            </Stack>
          </Grid>
          {
            // TODO: Other links
          }
        </Grid>
      </Grid>
    </Paper>
  );
};

export default FooterOrganism;
