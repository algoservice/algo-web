import { ReactNode } from 'react';

export type GraphEditorOrganismProps = {
  children?: ReactNode;
};
