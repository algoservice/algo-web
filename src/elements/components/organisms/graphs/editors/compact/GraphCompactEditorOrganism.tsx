import React, { useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';

import { Box } from '@mui/material';

import { useRootDispatch } from '../../../../../../redux/dispatch/rootDispatch';
import { RootState } from '../../../../../../redux/reducers/rootReducer';
import { openControlsSidebar } from '../../../../../../redux/actions/app/openControlsSidebar';
import { openParametersSidebar } from '../../../../../../redux/actions/app/openParametersSidebar';
import { closeControlsSidebar } from '../../../../../../redux/actions/app/closeControlsSidebar';

import ControlsFabAtom from '../../../../atoms/buttons/fab/controls/ControlsFabAtom';
import { FabAtomSize } from '../../../../atoms/buttons/fab/FabAtomProps';
import ParametersFabAtom from '../../../../atoms/buttons/fab/parameters/ParametersFabAtom';
import { SideBarPosition } from '../../../../atoms/sidebars/sidebar/SideBarAtom';
import MarginBoxAtom from '../../../../atoms/containers/margin/MarginBoxAtom';
import { GraphEditorOrganismProps } from '../GraphEditorOrganismProps';
import ControlsSidebarMolecule from '../../../../molecules/sidebars/controls/ControlsSidebarMolecule';
import ParametersSidebarMolecule from '../../../../molecules/sidebars/parameters/ParametersSidebarMolecule';

const GraphCompactEditorOrganism: React.FC<GraphEditorOrganismProps> = ({
  children,
}) => {
  const dispatch = useRootDispatch();

  const { isControlsOpened, isParametersOpened } = useSelector(
    (state: RootState) => state.appReducer,
  );

  const mainRef = useRef<HTMLDivElement>(null);
  const sidebarsHeight = '40%';

  /**
   * Prevent opening the controls sidebar when the parameters sidebar is opened on the compact layout.
   * Because both sidebars are at the bottom of the screen.
   */
  useEffect(() => {
    if (isControlsOpened && isParametersOpened) {
      dispatch(closeControlsSidebar());
    }
  }, [dispatch, isControlsOpened, isParametersOpened]);

  return (
    <Box
      className='compact-editor'
      position='relative'
      width='100%'
      height='100%'
      ref={mainRef}
      style={{
        overflowX: 'hidden',
        overflowY: 'hidden',
      }}>
      <ControlsFabAtom
        className='compact-editor__control-fab'
        size={FabAtomSize.MEDIUM}
        sx={{ position: 'absolute', left: '30%', bottom: '2%' }}
        onClick={() => dispatch(openControlsSidebar())}
      />
      <ParametersFabAtom
        className='compact-editor__parameters-fab'
        size={FabAtomSize.MEDIUM}
        sx={{ position: 'absolute', right: '30%', bottom: '2%' }}
        onClick={() => dispatch(openParametersSidebar())}
      />
      <MarginBoxAtom
        className='compact-editor__margin-box'
        width='100%'
        height='100%'
        bottom={sidebarsHeight}
        shrinkBottom={isControlsOpened || isParametersOpened}>
        {children}
      </MarginBoxAtom>
      <ControlsSidebarMolecule
        className='compact-editor__controls'
        side={SideBarPosition.BOTTOM}
        size={sidebarsHeight}
        parentRef={mainRef}
      />
      <ParametersSidebarMolecule
        className='compact-editor__parameters'
        side={SideBarPosition.BOTTOM}
        size={sidebarsHeight}
        parentRef={mainRef}
      />
    </Box>
  );
};

export default GraphCompactEditorOrganism;
