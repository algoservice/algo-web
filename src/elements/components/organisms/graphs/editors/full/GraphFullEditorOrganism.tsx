import React, { useRef } from 'react';
import { useSelector } from 'react-redux';

import { Box } from '@mui/material';

import { useRootDispatch } from '../../../../../../redux/dispatch/rootDispatch';
import { RootState } from '../../../../../../redux/reducers/rootReducer';
import { openControlsSidebar } from '../../../../../../redux/actions/app/openControlsSidebar';
import { openParametersSidebar } from '../../../../../../redux/actions/app/openParametersSidebar';

import ControlsFabAtom from '../../../../atoms/buttons/fab/controls/ControlsFabAtom';
import { FabAtomSize } from '../../../../atoms/buttons/fab/FabAtomProps';
import ParametersFabAtom from '../../../../atoms/buttons/fab/parameters/ParametersFabAtom';
import { SideBarPosition } from '../../../../atoms/sidebars/sidebar/SideBarAtom';
import MarginBoxAtom from '../../../../atoms/containers/margin/MarginBoxAtom';
import { GraphEditorOrganismProps } from '../GraphEditorOrganismProps';
import ControlsSidebarMolecule from '../../../../molecules/sidebars/controls/ControlsSidebarMolecule';
import ParametersSidebarMolecule from '../../../../molecules/sidebars/parameters/ParametersSidebarMolecule';

const GraphFullEditorOrganism: React.FC<GraphEditorOrganismProps> = ({
  children,
}) => {
  const dispatch = useRootDispatch();

  const { isControlsOpened, isParametersOpened } = useSelector(
    (state: RootState) => state.appReducer,
  );

  const mainRef = useRef<HTMLDivElement>(null);
  const sidebarsWidth = '30%';

  return (
    <Box
      className='full-editor'
      position='relative'
      width='100%'
      height='100%'
      ref={mainRef}
      style={{
        overflowX: 'hidden',
        overflowY: 'hidden',
      }}>
      <ControlsFabAtom
        className='full-editor__controls-fab'
        size={FabAtomSize.MEDIUM}
        sx={{ position: 'absolute', left: '2%', top: '50%' }}
        onClick={() => dispatch(openControlsSidebar())}
      />
      <ParametersFabAtom
        className='full-editor__parameters-fab'
        size={FabAtomSize.MEDIUM}
        sx={{ position: 'absolute', right: '2%', top: '50%' }}
        onClick={() => dispatch(openParametersSidebar())}
      />
      <MarginBoxAtom
        className='full-editor__margin-box'
        width='100%'
        height='100%'
        right={sidebarsWidth}
        left={sidebarsWidth}
        shrinkLeft={isControlsOpened}
        shrinkRight={isParametersOpened}>
        {children}
      </MarginBoxAtom>
      <ControlsSidebarMolecule
        className='full-editor__controls'
        side={SideBarPosition.LEFT}
        size={sidebarsWidth}
        parentRef={mainRef}
      />
      <ParametersSidebarMolecule
        className='full-editor__parameters'
        side={SideBarPosition.RIGHT}
        size={sidebarsWidth}
        parentRef={mainRef}
      />
    </Box>
  );
};

export default GraphFullEditorOrganism;
