import React, { useEffect } from 'react';

import { Box, CircularProgress, Grid } from '@mui/material';

import useMountEffect from '../../../../../hooks/wrapped/useMountEffect';
import useGraph from '../../../../../hooks/graph/useGraph';
import useGraphSearch from '../../../../../hooks/search/useGraphSearch';

import isMetaDataGraph from '../../../../../graph/utils/guards/isMetaDataGraph';
import { MetaDataGraph } from '../../../../../graph/types/MetaDataGraph';
import { emptyGraphInfo } from '../../../../../graph/instances/emptyGraphInfo';
import { emptyGraphMeta } from '../../../../../graph/instances/emptyGraphMeta';

import makeGraphSearch from '../../../../../search/makers/makeGraphSearch';
import { SearchStatus } from '../../../../../search/types/common/SearchStatus';

import GraphCardMolecule from '../../../molecules/cards/graph/GraphCardMolecule';
import { GraphMetaId } from '../../../../../graph/types/meta/GraphMetaId';
import TypographyAtom from '../../../atoms/typography/default/TypographyAtom';

import { en } from '../../../../../constants/l10n/en';

const GraphGridOrganism: React.FC = () => {
  const { graphs, success, getGraphs } = useGraph();
  const { graphSearchStatus } = useGraphSearch();

  const [displayedGraphs, setDisplayedGraphs] = React.useState<MetaDataGraph[]>(
    [],
  );

  useMountEffect(() => {
    if (graphSearchStatus !== SearchStatus.IN_PROGRESS) {
      getGraphs(makeGraphSearch());
    }
  });

  useEffect(() => {
    if (success && graphSearchStatus !== SearchStatus.IN_PROGRESS) {
      const newGraphs: MetaDataGraph[] = graphs.map((graph) =>
        isMetaDataGraph(graph)
          ? graph
          : ({ emptyGraphMeta, emptyGraphInfo } as unknown as MetaDataGraph),
      );
      setDisplayedGraphs((state) => {
        const displayedGraphsId: Set<GraphMetaId | undefined> = new Set(
          state.map((graph) => graph.meta.id),
        );
        return [
          ...state,
          ...newGraphs.filter(
            (newGraph) => !displayedGraphsId.has(newGraph.meta.id),
          ),
        ];
      });
    }
  }, [graphs, success, graphSearchStatus]);

  const renderGraphCards = () =>
    displayedGraphs.map((graph) => (
      <Grid item xs={12} sm={6} md={4} lg={3} key={graph.meta.id}>
        <GraphCardMolecule
          className={`graph-grid__card-${graph.meta.id}`}
          graphInfo={graph.data}
        />
      </Grid>
    ));

  const renderGridContent = () => (
    <Grid className='graph-grid' container spacing={2}>
      {renderGraphCards()}
    </Grid>
  );

  const renderLoadingContent = () => (
    <Box
      className='graph-grid__loading-content'
      component='main'
      width='100%'
      height='100%'
      display='flex'
      justifyContent='center'
      alignItems='center'>
      <CircularProgress />
    </Box>
  );

  const renderEmptyContent = () => (
    <Box
      className='graph-grid__empty-content'
      component='main'
      width='100%'
      height='100%'
      display='flex'
      justifyContent='center'
      alignItems='center'>
      <TypographyAtom className='graph-grid__empty-content-text' variant='h5'>
        {en.pages.graphs.notFound}
      </TypographyAtom>
    </Box>
  );

  const renderContent = () => {
    if (
      displayedGraphs.length === 0 &&
      graphSearchStatus !== SearchStatus.IN_PROGRESS
    ) {
      return renderEmptyContent();
    } else if (displayedGraphs.length === 0) {
      return renderLoadingContent();
    } else {
      return renderGridContent();
    }
  };

  return renderContent();
};

export default GraphGridOrganism;
