import { ElementType } from 'react';

import { TypographyOwnProps } from '@mui/material/Typography/Typography';

import { ComponentProps } from '../../ComponentProps';

export type TypographyProps = ComponentProps &
  TypographyOwnProps & {
    error?: boolean;
    warning?: boolean;
    component?: ElementType;
  };
