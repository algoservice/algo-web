import React from 'react';
import { TypographyProps } from '../TypographyProps';
import { Typography } from '@mui/material';
import { useAppTheme } from '../../../../../hooks/theme/useAppTheme';

const TypographyAtom: React.FC<TypographyProps> = ({
  className,
  error,
  warning,
  variant,
  component,
  fontSize,
  fontWeight,
  align,
  color,
  flex,
  margin,
  marginTop,
  marginRight,
  marginBottom,
  marginLeft,
  padding,
  paddingTop,
  paddingRight,
  paddingBottom,
  paddingLeft,
  children,
}) => {
  const theme = useAppTheme();

  const selectColor = () => {
    if (error) {
      return theme.palette.error.main;
    }
    if (warning) {
      return theme.palette.warning.main;
    }
    return theme.palette.text.primary;
  };

  return (
    <Typography
      className={className}
      variant={variant}
      component={component ?? 'span'}
      fontSize={fontSize}
      fontWeight={fontWeight}
      align={align}
      color={color ?? selectColor()}
      flex={flex}
      margin={margin}
      marginTop={marginTop}
      marginRight={marginRight}
      marginBottom={marginBottom}
      marginLeft={marginLeft}
      padding={padding}
      paddingTop={paddingTop}
      paddingRight={paddingRight}
      paddingBottom={paddingBottom}
      paddingLeft={paddingLeft}>
      {children}
    </Typography>
  );
};

export default TypographyAtom;
