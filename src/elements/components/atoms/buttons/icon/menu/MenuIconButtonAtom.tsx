import React from 'react';

import { IconButton } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';

import { IconButtonAtomProps } from '../IconButtonAtomProps';

const MenuIconButtonAtom: React.FC<IconButtonAtomProps> = ({
  className,
  onClick,
  sx,
}) => (
  <IconButton className={className} color='default' sx={sx} onClick={onClick}>
    <MenuIcon />
  </IconButton>
);

export default MenuIconButtonAtom;
