import React from 'react';

import { IconButton } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';

import { IconButtonAtomProps } from '../IconButtonAtomProps';

const CloseIconButtonAtom: React.FC<IconButtonAtomProps> = ({
  className,
  onClick,
  sx,
}) => (
  <IconButton className={className} color='inherit' sx={sx} onClick={onClick}>
    <CloseIcon />
  </IconButton>
);

export default CloseIconButtonAtom;
