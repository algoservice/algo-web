import React from 'react';
import { useSelector } from 'react-redux';

import { IconButton } from '@mui/material';

import { RootState } from '../../../../../../redux/reducers/rootReducer';

import { IconButtonAtomProps } from '../IconButtonAtomProps';

import { LightMode } from '../../../../../../constants/palettes/types/AppPaletteMode';
import { en } from '../../../../../../constants/l10n/en';
import { publicFiles } from 'src/constants/public/files';

const LogoIconButtonAtom: React.FC<IconButtonAtomProps> = ({
  className,
  onClick = () => {},
}) => {
  const { mode } = useSelector((state: RootState) => state.themeReducer);

  return (
    <IconButton
      className={className}
      sx={{ ml: 1 }}
      color='default'
      onClick={onClick}>
      <img
        width={24}
        height={24}
        src={
          mode === LightMode
            ? publicFiles.static.images.LightLogo
            : publicFiles.static.images.DarkLogo
        }
        alt={en.serviceName}
      />
    </IconButton>
  );
};

export default LogoIconButtonAtom;
