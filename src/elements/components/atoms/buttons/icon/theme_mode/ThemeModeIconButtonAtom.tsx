import React from 'react';
import { useSelector } from 'react-redux';

import { IconButton, Tooltip } from '@mui/material';
import { Brightness4TwoTone, Brightness7TwoTone } from '@mui/icons-material';

import { RootState } from '../../../../../../redux/reducers/rootReducer';
import { LightMode } from '../../../../../../constants/palettes/types/AppPaletteMode';
import { IconButtonAtomProps } from '../IconButtonAtomProps';

import { en } from '../../../../../../constants/l10n/en';

const ThemeModeIconButtonAtom: React.FC<IconButtonAtomProps> = ({
  className,
  onClick,
  sx,
}) => {
  const { mode } = useSelector((state: RootState) => state.themeReducer);

  return (
    <Tooltip title={en.menu.theme.tooltip} arrow>
      <IconButton
        className={className}
        sx={{ ml: 1, ...sx }}
        color='default'
        onClick={onClick}>
        {mode === LightMode ? <Brightness4TwoTone /> : <Brightness7TwoTone />}
      </IconButton>
    </Tooltip>
  );
};

export default ThemeModeIconButtonAtom;
