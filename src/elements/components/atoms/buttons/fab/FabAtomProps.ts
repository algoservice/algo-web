import { ButtonAtomProps } from '../ButtonAtomProps';

export enum FabAtomSize {
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large',
}

export type FabAtomProps = ButtonAtomProps & {
  size?: FabAtomSize;
};
