import React from 'react';

import { Fab } from '@mui/material';
import EditNoteIcon from '@mui/icons-material/EditNote';

import { FabAtomProps } from '../FabAtomProps';

const ControlsFabAtom: React.FC<FabAtomProps> = ({
  className,
  onClick,
  size,
  sx,
}) => (
  <Fab
    className={className}
    onClick={onClick}
    size={size}
    sx={sx}
    color='default'>
    <EditNoteIcon />
  </Fab>
);

export default ControlsFabAtom;
