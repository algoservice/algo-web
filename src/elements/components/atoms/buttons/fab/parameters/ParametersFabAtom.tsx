import React from 'react';

import { Fab } from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';

import { FabAtomProps } from '../FabAtomProps';

const ParametersFabAtom: React.FC<FabAtomProps> = ({
  className,
  onClick,
  size,
  sx,
  disabled,
}) => (
  <Fab
    className={className}
    onClick={onClick}
    size={size}
    sx={sx}
    color='default'
    disabled={disabled}>
    <EditIcon />
  </Fab>
);

export default ParametersFabAtom;
