import { ButtonAtomProps } from '../ButtonAtomProps';

export type RegularButtonAtomProps = ButtonAtomProps & {
  text?: string;
};
