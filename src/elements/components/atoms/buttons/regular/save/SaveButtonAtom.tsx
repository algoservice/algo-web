import React from 'react';

import { Button, Tooltip } from '@mui/material';
import SaveIcon from '@mui/icons-material/Save';

import { RegularButtonAtomProps } from '../RegularButtonAtomProps';

type SaveButtonAtomProps = RegularButtonAtomProps & {
  error?: boolean;
};

const SaveButtonAtom: React.FC<SaveButtonAtomProps> = ({
  className,
  sx,
  onClick,
  tooltip,
  text,
  error,
}) => (
  <Tooltip title={tooltip} arrow>
    <Button
      className={className}
      startIcon={<SaveIcon />}
      color={error ? 'error' : 'success'}
      variant='outlined'
      sx={sx}
      onClick={onClick}>
      {text}
    </Button>
  </Tooltip>
);

export default SaveButtonAtom;
