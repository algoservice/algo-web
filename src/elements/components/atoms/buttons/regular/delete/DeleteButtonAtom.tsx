import React from 'react';

import { Button, Tooltip } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';

import { RegularButtonAtomProps } from '../RegularButtonAtomProps';

const DeleteButtonAtom: React.FC<RegularButtonAtomProps> = ({
  className,
  sx,
  onClick,
  tooltip,
  text,
}) => (
  <Tooltip title={tooltip} arrow>
    <Button
      className={className}
      startIcon={<DeleteIcon />}
      color='error'
      variant='outlined'
      sx={sx}
      onClick={onClick}>
      {text}
    </Button>
  </Tooltip>
);

export default DeleteButtonAtom;
