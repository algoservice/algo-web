import React from 'react';

import { Button, Stack, Tooltip } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';

import { ButtonAtomProps } from '../../ButtonAtomProps';

import { en } from '../../../../../../constants/l10n/en';

const CreateMenuButtonAtom: React.FC<ButtonAtomProps> = ({
  className,
  sx,
  onClick,
}) => (
  <Tooltip title={en.menu.create.tooltip} arrow>
    <Button className={className} sx={sx} onClick={onClick}>
      <Stack className={`${className}-content`} direction='row'>
        <AddIcon color='action' />
        <ArrowDropDownIcon color='action' />
      </Stack>
    </Button>
  </Tooltip>
);

export default CreateMenuButtonAtom;
