import React from 'react';

import { ComponentProps } from '../../ComponentProps';

export type ButtonAtomProps = ComponentProps & {
  tooltip?: string;
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
  disabled?: boolean;
};
