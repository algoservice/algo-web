import SelectAtomProps from '../SelectAtomProps';

type SingleSelectAtomProps<T> = SelectAtomProps<T> & {
  value?: T;
  onChange?: (newValue: T) => void;
};

export default SingleSelectAtomProps;
