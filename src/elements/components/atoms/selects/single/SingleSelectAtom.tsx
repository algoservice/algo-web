import React from 'react';

import {
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material';

import SingleSelectAtomProps from './SingleSelectAtomProps';

const SingleSelectAtom: React.FC<SingleSelectAtomProps<string>> = ({
  className,
  sx,
  label,
  required,
  error,
  disabled,
  hint,
  size,
  autoWidth,
  value,
  valueOptions,
  onChange,
}) => {
  const handleChange = (event: SelectChangeEvent) => {
    onChange && onChange(event.target.value);
  };

  const renderLabelText = () => (required ? `${label} *` : label);

  const renderLabel = () =>
    !!label && (
      <InputLabel id={`${className}-select-form__select-label`}>
        {renderLabelText()}
      </InputLabel>
    );

  const renderValues = () => {
    if (valueOptions) {
      return valueOptions.map((valueOption) => (
        <MenuItem
          key={valueOption.value}
          className={`select-form__value-${valueOption.value}`}
          value={valueOption.value}>
          {valueOption.label}
        </MenuItem>
      ));
    }
  };

  const renderHint = () =>
    hint && (
      <FormHelperText className='select-form__hint'>{hint}</FormHelperText>
    );

  return (
    <FormControl
      className={`${className} select-form`}
      sx={sx}
      size={size}
      disabled={disabled}
      error={error}>
      {renderLabel()}
      <Select
        className='select-form__select'
        labelId={`${className}-select-form__select-label`}
        value={value}
        label={renderLabelText()}
        required={required}
        autoWidth={autoWidth}
        onChange={handleChange}>
        {renderValues()}
      </Select>
      {renderHint()}
    </FormControl>
  );
};

export default SingleSelectAtom;
