import { ComponentProps } from '../../ComponentProps';

type SelectAtomProps<T> = ComponentProps & {
  label?: string;
  required?: boolean;
  error?: boolean;
  disabled?: boolean;
  hint?: string;
  size?: SelectAtomSize;
  autoWidth?: boolean;
  valueOptions: SelectAtomValueOption<T>[];
};

type SelectAtomSize = 'small' | 'medium';

export type SelectAtomValueOption<T> = {
  label: string;
  value: T;
};

export default SelectAtomProps;
