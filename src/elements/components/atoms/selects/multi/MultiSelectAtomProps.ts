import SelectAtomProps from '../SelectAtomProps';

export type MultiSelectValueOnChange<T> = {
  values: T[];
  newValue: T | null;
  removedValue: T | null;
};

type MultiSelectAtomProps<T> = SelectAtomProps<T> & {
  value: T[];
  onChange?: (newValue: MultiSelectValueOnChange<T>) => void;
};

export default MultiSelectAtomProps;
