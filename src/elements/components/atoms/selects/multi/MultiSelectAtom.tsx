import React from 'react';

import {
  Box,
  Chip,
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material';

import MultiSelectAtomProps from './MultiSelectAtomProps';

const MultiSelectAtom: React.FC<MultiSelectAtomProps<string>> = ({
  className,
  sx,
  label,
  required,
  error,
  disabled,
  hint,
  size,
  autoWidth,
  value,
  valueOptions,
  onChange,
}) => {
  const handleChange = (event: SelectChangeEvent<typeof value>) => {
    const newValues =
      typeof event.target.value === 'string'
        ? event.target.value.split(',')
        : event.target.value;

    if (value.length > newValues.length) {
      let removedValue = value[value.length - 1];
      for (let i = 0; i < newValues.length; i++) {
        if (value[i] !== newValues[i]) {
          removedValue = value[i];
          break;
        }
      }
      onChange &&
        onChange({
          values: newValues,
          newValue: null,
          removedValue,
        });
    }
    if (value.length < newValues.length) {
      onChange &&
        onChange({
          values: newValues,
          newValue: newValues[newValues.length - 1],
          removedValue: null,
        });
    }
  };

  const renderLabelText = () => (required ? `${label} *` : label);

  const renderLabel = () =>
    label && (
      <InputLabel id={`${className}-multiselect-form__select-label`}>
        {renderLabelText()}
      </InputLabel>
    );

  const renderValues = () => {
    if (valueOptions) {
      return valueOptions.map((valueOption) => (
        <MenuItem
          key={valueOption.value}
          className={`multiselect-form__value-${valueOption.value}`}
          value={valueOption.value}>
          {valueOption.label}
        </MenuItem>
      ));
    }
  };

  const renderHint = () =>
    hint && (
      <FormHelperText className='multiselect-form__hint'>{hint}</FormHelperText>
    );

  const renderValue = (selected: string[]) => (
    <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
      {selected.map((selectedValue) => {
        const selectedOption = valueOptions.find(
          (valueOption) => valueOption.value === selectedValue,
        );
        return (
          <Chip
            key={selectedOption ? selectedOption.value : selectedValue}
            label={selectedOption ? selectedOption.label : selectedValue}
          />
        );
      })}
    </Box>
  );

  return (
    <FormControl
      className={`${className} multiselect-form`}
      sx={sx}
      size={size}
      disabled={disabled}
      error={error}>
      {renderLabel()}
      <Select
        className='multiselect-form__select'
        labelId={`${className}-multiselect-form__select-label`}
        multiple
        value={value}
        label={renderLabelText()}
        required={required}
        autoWidth={autoWidth}
        onChange={handleChange}
        renderValue={renderValue}>
        {renderValues()}
      </Select>
      {renderHint()}
    </FormControl>
  );
};

export default MultiSelectAtom;
