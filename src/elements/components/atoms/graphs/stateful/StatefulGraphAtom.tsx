import React, { useCallback, useEffect, useState } from 'react';

import { Box } from '@mui/material';

import {
  applyEdgeChanges,
  applyNodeChanges,
  Background,
  Connection,
  Controls,
  EdgeChange,
  NodeChange,
  NodePositionChange,
  ReactFlow,
} from 'reactflow';
import 'reactflow/dist/style.css';

import { GraphHooks } from '../../../../../graph/hooks/GraphHooks';
import useInternalGraphScheme from '../../../../../hooks/internal_graph/useInternalGraphScheme';

import { ComponentProps } from '../../../ComponentProps';
import GraphTheme from '../../../../themes/graph/GraphTheme';
import GraphController from '../../../../../graph/controller/GraphController';
import convertNodes from 'src/graph/utils/convertNodes';
import convertEdges from '../../../../../graph/utils/convertEdges';
import isGraphNode from '../../../../../graph/utils/guards/isGraphNode';
import isGraphEdge from '../../../../../graph/utils/guards/isGraphEdge';
import extractElement, {
  ExtractedValue,
} from '../../../../../graph/utils/extractElement';
import { GraphNode } from '../../../../../graph/types/node/GraphNode';
import { GraphEdge } from '../../../../../graph/types/edge/GraphEdge';

type GraphAtomProps = ComponentProps & {
  interactive?: boolean;
};

const StatefulGraphAtom: React.FC<GraphAtomProps> = ({
  className,
  sx,
  interactive,
}) => {
  const { onNodeDragStart, onEdgeClick, onPaneClick } =
    GraphHooks.useGraphCallbacks();
  const {
    graphScheme,
    selectedGraphElement,
    updateGraphScheme,
    updateSelectedGraphElement,
    addEdgeToGraph,
  } = useInternalGraphScheme();

  const [nodes, setNodes] = useState(graphScheme.nodes);
  const [edges, setEdges] = useState(graphScheme.edges);

  useEffect(() => {
    setNodes(
      isGraphNode(selectedGraphElement)
        ? [...graphScheme.nodes, selectedGraphElement]
        : graphScheme.nodes,
    );
    setEdges(
      isGraphEdge(selectedGraphElement)
        ? [...graphScheme.edges, selectedGraphElement]
        : graphScheme.edges,
    );
  }, [graphScheme, selectedGraphElement, setNodes, setEdges]);

  const updateExternalOnInternalChanges = useCallback(
    (newNodes: GraphNode[], newEdges: GraphEdge[]) => {
      const extractedNodeElement: ExtractedValue<GraphNode> = extractElement(
        isGraphNode(selectedGraphElement) ? selectedGraphElement.id : null,
        newNodes,
      );
      if (extractedNodeElement.extractedElement) {
        updateSelectedGraphElement(extractedNodeElement.extractedElement);
      }
      const extractedEdgeElement: ExtractedValue<GraphEdge> = extractElement(
        isGraphEdge(selectedGraphElement) ? selectedGraphElement.id : null,
        newEdges,
      );
      if (extractedEdgeElement.extractedElement) {
        updateSelectedGraphElement(extractedEdgeElement.extractedElement);
      }
      updateGraphScheme({
        nodes: extractedNodeElement.reducedElements,
        edges: extractedEdgeElement.reducedElements,
      });
    },
    [selectedGraphElement, updateGraphScheme, updateSelectedGraphElement],
  );

  // TODO (Michael):
  //  For further optimization, it is required to implement a storage
  //  as referenced stateful object for the graph schema in the editor.
  //  This is because there are too many updates for the graph scheme in the Redux.
  //  If we have storage for the current state of the graph during editing
  //  and only load and unload the schema into the Redux if necessary, then performance will increase significantly.
  //  We will also be able to optimize rendering even more, for example,
  //  by placing the coordinates of the nodes in a separate array.
  const onNodesChange = useCallback(
    (changes: NodeChange[]) => {
      const newNodes = convertNodes(applyNodeChanges(changes, nodes));
      setNodes(newNodes);
      if (
        changes.at(0)?.type !== 'position' ||
        (changes.at(0)?.type === 'position' &&
          !(changes.at(0) as NodePositionChange)?.dragging)
      ) {
        updateExternalOnInternalChanges(newNodes, edges);
      }
    },
    [nodes, edges, setNodes, updateExternalOnInternalChanges],
  );

  const onEdgesChange = useCallback(
    (changes: EdgeChange[]) => {
      const newEdges = convertEdges(applyEdgeChanges(changes, edges));
      setEdges(newEdges);
      updateExternalOnInternalChanges(nodes, newEdges);
    },
    [nodes, edges, setEdges, updateExternalOnInternalChanges],
  );

  const onConnect = useCallback(
    (params: Connection) => {
      if (params.source && params.target) {
        addEdgeToGraph(params.source, params.target);
      }
    },
    [addEdgeToGraph],
  );

  const isInteractive = () => interactive ?? false;

  return (
    <Box className={className} sx={sx}>
      <GraphTheme>
        <ReactFlow
          nodes={nodes}
          edges={edges}
          onNodesChange={onNodesChange}
          onEdgesChange={onEdgesChange}
          onConnect={onConnect}
          onNodeDragStart={onNodeDragStart}
          onEdgeClick={onEdgeClick}
          onPaneClick={onPaneClick}
          elementsSelectable={isInteractive()}
          nodesConnectable={isInteractive()}
          nodesFocusable={isInteractive()}>
          <Background />
          <Controls showInteractive={isInteractive()} />
          <GraphController />
        </ReactFlow>
      </GraphTheme>
    </Box>
  );
};

export default StatefulGraphAtom;
