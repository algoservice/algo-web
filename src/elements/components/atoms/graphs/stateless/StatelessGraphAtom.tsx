import React from 'react';

import { Box } from '@mui/material';

import { Background, Controls, ReactFlow } from 'reactflow';
import 'reactflow/dist/style.css';

import { ComponentProps } from '../../../ComponentProps';
import GraphTheme from '../../../../themes/graph/GraphTheme';
import GraphController from '../../../../../graph/controller/GraphController';
import { GraphScheme } from '../../../../../graph/types/scheme/GraphScheme';

type GraphAtomProps = ComponentProps & {
  graphScheme: GraphScheme;
  interactive?: boolean;
};

const StatelessGraphAtom: React.FC<GraphAtomProps> = ({
  className,
  sx,
  graphScheme,
  interactive,
}) => {
  const isInteractive = () => interactive ?? false;

  return (
    <Box className={className} sx={sx}>
      <GraphTheme>
        <ReactFlow
          nodes={graphScheme.nodes}
          edges={graphScheme.edges}
          elementsSelectable={isInteractive()}
          nodesConnectable={isInteractive()}
          nodesFocusable={isInteractive()}>
          <Background />
          <Controls showInteractive={isInteractive()} />
          <GraphController />
        </ReactFlow>
      </GraphTheme>
    </Box>
  );
};

export default StatelessGraphAtom;
