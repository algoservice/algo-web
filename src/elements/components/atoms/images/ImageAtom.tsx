import React from 'react';

import { Box } from '@mui/material';

import ImageAtomProps from './ImageAtomProps';

const ImageAtom: React.FC<ImageAtomProps> = ({ className, src, alt, sx }) => (
  <Box className={`${className}-wrapper`} sx={sx}>
    <img className={className} width='100%' height='100%' src={src} alt={alt} />
  </Box>
);

export default ImageAtom;
