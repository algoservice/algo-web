import { ComponentProps } from '../../ComponentProps';

type ImageAtomProps = ComponentProps & {
  src: string;
  alt: string;
};

export default ImageAtomProps;
