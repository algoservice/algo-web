import React, { ReactNode, useMemo } from 'react';

import { Box } from '@mui/material';

import { ComponentProps } from '../../../ComponentProps';
import { AppTheme } from '../../../../../hooks/theme/types/AppTheme';
import { useAppTheme } from '../../../../../hooks/theme/useAppTheme';

type MarginBoxAtomProps = ComponentProps & {
  children?: ReactNode;
  width?: string;
  height?: string;
  top?: string;
  right?: string;
  bottom?: string;
  left?: string;
  shrinkTop?: boolean;
  shrinkRight?: boolean;
  shrinkBottom?: boolean;
  shrinkLeft?: boolean;
};

const MarginBoxAtom: React.FC<MarginBoxAtomProps> = ({
  className,
  sx,
  children,
  width,
  height,
  top,
  right,
  bottom,
  left,
  shrinkTop,
  shrinkRight,
  shrinkBottom,
  shrinkLeft,
}) => {
  const theme: AppTheme = useAppTheme();

  const defaultSize = '100%';
  const defaultShrink = '0%';

  const calculateWidth = useMemo(() => {
    let calculatedWidth = width ?? defaultSize;
    if (shrinkRight) {
      calculatedWidth = `${calculatedWidth} - ${right ?? defaultShrink}`;
    }
    if (shrinkLeft) {
      calculatedWidth = `${calculatedWidth} - ${left ?? defaultShrink}`;
    }
    return `calc(${calculatedWidth})`;
  }, [width, right, shrinkRight, left, shrinkLeft]);

  const calculateHeight = useMemo(() => {
    let calculatedHeight = height ?? defaultSize;
    if (shrinkTop) {
      calculatedHeight = `${calculatedHeight} - ${top ?? defaultShrink}`;
    }
    if (shrinkBottom) {
      calculatedHeight = `${calculatedHeight} - ${bottom ?? defaultShrink}`;
    }
    return `calc(${calculatedHeight})`;
  }, [height, top, shrinkTop, bottom, shrinkBottom]);

  return (
    <Box
      className={className}
      sx={sx}
      style={{
        transition: theme.transitions.create(['margin', 'width', 'height'], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        width: calculateWidth,
        height: calculateHeight,
        ...((shrinkTop || shrinkRight || shrinkBottom || shrinkLeft) && {
          transition: theme.transitions.create(['margin', 'width', 'height'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
          }),
          ...(shrinkTop && { marginTop: top ?? defaultShrink }),
          ...(shrinkRight && { marginRight: right ?? defaultShrink }),
          ...(shrinkBottom && { marginBottom: bottom ?? defaultShrink }),
          ...(shrinkLeft && { marginLeft: left ?? defaultShrink }),
        }),
      }}>
      {children}
    </Box>
  );
};

export default MarginBoxAtom;
