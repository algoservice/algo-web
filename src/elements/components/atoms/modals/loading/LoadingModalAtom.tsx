import React, { ReactNode } from 'react';

import { Box, Modal, Stack } from '@mui/material';

import { ModalAtomProps } from '../ModalAtomProps';

export enum LoadingModalAtomState {
  LOADING = 'LOADING',
  SUCCESS = 'SUCCESS',
  FAILURE = 'FAILURE',
}

type LoadingModalAtomProps = ModalAtomProps & {
  state: LoadingModalAtomState;
  renderOnLoading?: () => ReactNode;
  renderOnSuccess?: () => ReactNode;
  renderOnFailure?: () => ReactNode;
};
const LoadingModalAtom: React.FC<LoadingModalAtomProps> = ({
  className,
  open,
  state,
  renderOnLoading,
  renderOnSuccess,
  renderOnFailure,
}) => {
  const renderNoContent = () => <Box />;
  const renderContent = () => {
    switch (state) {
      case LoadingModalAtomState.LOADING:
        return renderOnLoading ? renderOnLoading() : renderNoContent();
      case LoadingModalAtomState.SUCCESS:
        return renderOnSuccess ? renderOnSuccess() : renderNoContent();
      case LoadingModalAtomState.FAILURE:
        return renderOnFailure ? renderOnFailure() : renderNoContent();
      default:
        return renderNoContent();
    }
  };

  return (
    <Modal className={className} open={open}>
      <Stack
        className={`${className}-container`}
        width='100%'
        height='100%'
        direction='column'
        alignItems='center'
        justifyContent='center'
        spacing={2}>
        {renderContent()}
      </Stack>
    </Modal>
  );
};

export default LoadingModalAtom;
