import { ComponentProps } from '../../ComponentProps';

export type ModalAtomProps = ComponentProps & {
  open: boolean;
};
