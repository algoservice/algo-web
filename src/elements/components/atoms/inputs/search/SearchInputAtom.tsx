import React from 'react';

import { Autocomplete, TextField } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';

import { ComponentProps } from '../../../ComponentProps';

import './style.css';

type SearchInputAtomProps = ComponentProps & {
  placeholder?: string;
  suggestions?: string[];
  onChange?: (newValue: string) => void;
  onSearch?: () => void;
};

const SearchInputAtom: React.FC<SearchInputAtomProps> = ({
  className,
  sx,
  placeholder,
  suggestions,
  onChange,
  onSearch,
}) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    onChange && onChange(event.target.value);
  };

  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      onSearch && onSearch();
    }
  };

  return (
    <Autocomplete
      className={className}
      sx={sx}
      freeSolo
      options={suggestions ?? []}
      renderInput={(params) => (
        <TextField
          {...params}
          placeholder={placeholder}
          InputProps={{
            ...params.InputProps,
            startAdornment: <SearchIcon />,
            type: 'search',
          }}
          onChange={handleChange}
          onKeyDown={handleKeyDown}
        />
      )}
    />
  );
};

export default SearchInputAtom;
