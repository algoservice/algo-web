import { ComponentProps } from '../../ComponentProps';

export type InputAtomProps = ComponentProps & {
  label?: string;
  required?: boolean;
  highlightOnError?: boolean;
  error?: boolean;
};
