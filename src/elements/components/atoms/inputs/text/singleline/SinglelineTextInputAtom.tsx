import React from 'react';

import { TextField } from '@mui/material';

import TextInputAtomProps from '../TextInputAtomProps';

import { en } from '../../../../../../constants/l10n/en';

const SinglelineTextInputAtom: React.FC<TextInputAtomProps> = ({
  className,
  sx,
  label,
  type = 'text',
  error,
  highlightOnError,
  value,
  restrictedValues,
  hintOnRestrictedMatch,
  required,
  hintOnEmpty,
  maxLength,
  hint,
  onChange,
}) => {
  const isInputEmpty = (currentValue?: string) =>
    !!required && currentValue ? currentValue.length === 0 : true;

  const isInputMatchedRestrictedValue = (currentValue?: string) =>
    !!restrictedValues && currentValue
      ? restrictedValues.includes(currentValue)
      : false;

  const isError = (currentValue?: string) =>
    isInputEmpty(currentValue) || isInputMatchedRestrictedValue(currentValue);

  const shouldHighlightError = (currentValue?: string) =>
    !!highlightOnError && isError(currentValue);

  const renderHelperText = (currentValue?: string) => {
    if (isInputEmpty(currentValue)) {
      return hintOnEmpty ?? en.components.inputs.singleline.defaultHintOnEmpty;
    }
    if (isInputMatchedRestrictedValue(currentValue)) {
      return (
        hintOnRestrictedMatch ??
        en.components.inputs.singleline.defaultHintOnRestricted
      );
    }
    return hint ?? '';
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const currentInputValue = event.target.value;
    onChange &&
      onChange({
        value: currentInputValue,
        isError: isError(currentInputValue),
      });
  };

  return (
    <TextField
      className={className}
      type={type}
      error={!!error || shouldHighlightError(value)}
      label={label}
      value={value}
      required={required}
      helperText={renderHelperText(value)}
      onChange={handleChange}
      sx={sx}
      inputProps={{
        maxLength: maxLength && maxLength > 0 ? maxLength : undefined,
      }}
    />
  );
};

export default SinglelineTextInputAtom;
