import { InputAtomProps } from '../InputAtomProps';

type TextInputAtomProps = InputAtomProps & {
  type?: 'text' | 'password';
  value?: string;
  restrictedValues?: string[];
  hintOnRestrictedMatch?: string;
  hintOnEmpty?: string;
  maxLength?: number;
  hint?: string;
  multiline?: boolean;
  rows?: number;
  onChange?: (newValue: TextInputAtomOnChange) => void;
};

export type TextInputAtomOnChange = {
  value: string;
  isError: boolean;
};

export default TextInputAtomProps;
