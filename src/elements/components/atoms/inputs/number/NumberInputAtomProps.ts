import { InputAtomProps } from '../InputAtomProps';

type NumberInputAtomProps = InputAtomProps & {
  value?: number;
  integer?: boolean;
  hint?: string;
  min?: number;
  max?: number;
  onChange?: (newValue: NumberInputAtomOnChange) => void;
};

export type NumberInputAtomOnChange = {
  value: number;
  isError: boolean;
};

export default NumberInputAtomProps;
