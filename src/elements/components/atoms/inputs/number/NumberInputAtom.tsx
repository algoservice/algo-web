import React from 'react';

import { TextField } from '@mui/material';

import NumberInputAtomProps from './NumberInputAtomProps';

const NumberInputAtom: React.FC<NumberInputAtomProps> = ({
  className,
  sx,
  label,
  required,
  highlightOnError,
  error,
  value,
  integer = false,
  hint,
  min,
  max,
  onChange,
}) => {
  const shouldHighlightError = () => !!highlightOnError && false;

  const renderHelperText = () => hint ?? '';

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const currentInputValue = Number(event.target.value);
    onChange &&
      onChange({
        value: currentInputValue,
        isError: false,
      });
  };

  const setValue = (newValue: number) => {
    let number = newValue;
    if (integer) {
      number = Math.round(newValue);
    }
    if (min && number < min) {
      number = min;
    }
    if (max && number > max) {
      number = max;
    }
    return number;
  };

  return (
    <TextField
      className={className}
      type='number'
      error={!!error || shouldHighlightError()}
      label={label}
      value={setValue(value ?? 0)}
      required={required}
      helperText={renderHelperText()}
      onChange={handleChange}
      sx={sx}
    />
  );
};

export default NumberInputAtom;
