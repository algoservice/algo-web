import React from 'react';

import { ListItemIcon, ListItemText, Menu, MenuItem } from '@mui/material';
import UploadFileIcon from '@mui/icons-material/UploadFile';
import NoteAddIcon from '@mui/icons-material/NoteAdd';

import { MenuAtomProps } from '../MenuAtomProps';

import { en } from '../../../../../constants/l10n/en';

type CreateGraphMenuAtomProps = MenuAtomProps & {
  anchorEl: HTMLElement | null;
  onClickNew: () => void;
  onClickImport: () => void;
};
const CreateGraphMenuAtom: React.FC<CreateGraphMenuAtomProps> = ({
  open,
  anchorEl,
  onClose,
  onClickNew,
  onClickImport,
}) => (
  <Menu open={open} onClose={onClose} anchorEl={anchorEl}>
    <MenuItem onClick={onClickNew}>
      <ListItemIcon>
        <NoteAddIcon />
      </ListItemIcon>
      <ListItemText>{en.menu.create.options.new}</ListItemText>
    </MenuItem>
    <MenuItem disabled onClick={onClickImport}>
      <ListItemIcon>
        <UploadFileIcon />
      </ListItemIcon>
      <ListItemText>{en.menu.create.options.import}</ListItemText>
    </MenuItem>
  </Menu>
);

export default CreateGraphMenuAtom;
