import { ComponentProps } from '../../ComponentProps';

export type MenuAtomProps = ComponentProps & {
  open: boolean;
  onClose: () => void;
};
