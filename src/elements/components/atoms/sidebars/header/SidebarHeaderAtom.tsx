import React from 'react';

import { Box } from '@mui/material';

import { ComponentProps } from '../../../ComponentProps';
import CloseIconButtonAtom from '../../buttons/icon/close/CloseIconButtonAtom';
import TypographyAtom from '../../typography/default/TypographyAtom';

type SideBarHeaderAtomProps = ComponentProps & {
  heading?: string;
  onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
};

const SidebarHeaderAtom: React.FC<SideBarHeaderAtomProps> = ({
  className,
  heading,
  onClick,
}) => (
  <Box
    className={className}
    width='100%'
    display='flex'
    flexDirection='row'
    alignItems='start'>
    <TypographyAtom
      className={`${className}__title`}
      component='h5'
      variant='h5'
      flex={1}
      padding='3px'>
      {heading}
    </TypographyAtom>
    <CloseIconButtonAtom
      className={`${className}__close-button`}
      onClick={onClick}
      sx={{ marginLeft: '5%' }}
    />
  </Box>
);

export default SidebarHeaderAtom;
