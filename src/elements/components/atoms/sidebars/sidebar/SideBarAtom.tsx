import React, { ReactNode } from 'react';

import { Box, Drawer } from '@mui/material';

import { ComponentProps } from '../../../ComponentProps';

export enum SideBarPosition {
  LEFT = 'left',
  RIGHT = 'right',
  BOTTOM = 'bottom',
}

type SideBarAtomProps = ComponentProps & {
  side: SideBarPosition;
  open: boolean;
  size: string;
  parentRef: React.RefObject<HTMLDivElement>;
  header?: ReactNode;
  children?: ReactNode;
};

const SideBarAtom: React.FC<SideBarAtomProps> = ({
  className,
  side,
  open,
  size,
  parentRef,
  header,
  children,
}) => {
  const calculateHeight = () =>
    side === SideBarPosition.BOTTOM ? size : '100%';

  const calculateWidth = () =>
    side === SideBarPosition.BOTTOM ? '100%' : size;

  const renderNoContent = () => <Box className={`${className}-no-content`} />;

  return (
    <Drawer
      className={className}
      anchor={side}
      open={open}
      variant='persistent'
      sx={{
        width: calculateWidth(),
        height: calculateHeight(),
      }}
      PaperProps={{
        style: {
          position: 'absolute',
          width: calculateWidth(),
          height: calculateHeight(),
        },
      }}
      ModalProps={{
        container: parentRef.current,
        style: {
          position: 'absolute',
        },
        slotProps: {
          backdrop: {
            style: {
              position: 'absolute',
            },
          },
        },
      }}>
      <Box
        className={`${className}-wrapper`}
        minHeight='100%'
        display='flex'
        flexDirection='column'>
        <Box className={`${className}-header`}>
          {header ?? renderNoContent()}
        </Box>
        <Box className={`${className}-content`} height='100%' overflow='auto'>
          {children ?? renderNoContent()}
        </Box>
      </Box>
    </Drawer>
  );
};

export default SideBarAtom;
