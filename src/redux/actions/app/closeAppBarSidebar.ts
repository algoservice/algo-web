import { Dispatch } from 'redux';

import { CLOSE_APP_BAR_SIDEBAR } from './appActionTypes';

export type CloseAppBarSidebarAction = {
  type: typeof CLOSE_APP_BAR_SIDEBAR;
};

export const closeAppBarSidebar =
  () => (dispatch: Dispatch<CloseAppBarSidebarAction>) => {
    dispatch({ type: CLOSE_APP_BAR_SIDEBAR });
  };
