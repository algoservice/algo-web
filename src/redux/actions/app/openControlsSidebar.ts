import { Dispatch } from 'redux';

import { OPEN_CONTROLS_SIDEBAR } from './appActionTypes';

export type OpenControlsSidebarAction = {
  type: typeof OPEN_CONTROLS_SIDEBAR;
};

export const openControlsSidebar =
  () => (dispatch: Dispatch<OpenControlsSidebarAction>) => {
    dispatch({ type: OPEN_CONTROLS_SIDEBAR });
  };
