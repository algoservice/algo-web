import { Dispatch } from 'redux';

import { OPEN_APP_BAR_SIDEBAR } from './appActionTypes';

export type OpenAppBarSidebarAction = {
  type: typeof OPEN_APP_BAR_SIDEBAR;
};

export const openAppBarSidebar =
  () => (dispatch: Dispatch<OpenAppBarSidebarAction>) => {
    dispatch({ type: OPEN_APP_BAR_SIDEBAR });
  };
