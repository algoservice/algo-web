/* App bar sidebar */
export const OPEN_APP_BAR_SIDEBAR = 'App/OPEN_APP_BAR_SIDEBAR';
export const CLOSE_APP_BAR_SIDEBAR = 'App/CLOSE_APP_BAR_SIDEBAR';

/* Controls sidebar */
export const OPEN_CONTROLS_SIDEBAR = 'App/OPEN_CONTROLS_SIDEBAR';
export const CLOSE_CONTROLS_SIDEBAR = 'App/CLOSE_CONTROLS_SIDEBAR';

/* Element properties sidebar */
export const OPEN_PARAMETERS_SIDEBAR = 'App/OPEN_ELEMENT_PROPERTIES_SIDEBAR';
export const CLOSE_PARAMETERS_SIDEBAR = 'App/CLOSE_ELEMENT_PROPERTIES_SIDEBAR';
