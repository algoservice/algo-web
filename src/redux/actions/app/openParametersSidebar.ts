import { Dispatch } from 'redux';

import { OPEN_PARAMETERS_SIDEBAR } from './appActionTypes';

export type OpenParametersSidebarAction = {
  type: typeof OPEN_PARAMETERS_SIDEBAR;
};

export const openParametersSidebar =
  () => (dispatch: Dispatch<OpenParametersSidebarAction>) => {
    dispatch({ type: OPEN_PARAMETERS_SIDEBAR });
  };
