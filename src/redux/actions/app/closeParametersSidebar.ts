import { Dispatch } from 'redux';

import { CLOSE_PARAMETERS_SIDEBAR } from './appActionTypes';

export type CloseParametersSidebarAction = {
  type: typeof CLOSE_PARAMETERS_SIDEBAR;
};

export const closeParametersSidebar =
  () => (dispatch: Dispatch<CloseParametersSidebarAction>) => {
    dispatch({ type: CLOSE_PARAMETERS_SIDEBAR });
  };
