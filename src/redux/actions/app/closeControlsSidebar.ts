import { Dispatch } from 'redux';

import { CLOSE_CONTROLS_SIDEBAR } from './appActionTypes';

export type CloseControlsSidebarAction = {
  type: typeof CLOSE_CONTROLS_SIDEBAR;
};

export const closeControlsSidebar =
  () => (dispatch: Dispatch<CloseControlsSidebarAction>) => {
    dispatch({ type: CLOSE_CONTROLS_SIDEBAR });
  };
