export enum GraphInternalActionState {
  /**
   * Action required by some component
   */
  REQUIRED,
  /**
   * Action allowed by some middleware component
   */
  ALLOWED,
  /**
   * Action finished of canceled and not required anymore
   */
  NOT_REQUIRED,
}

export const SET_GRAPH = 'Graph/Internal/SET_GRAPH';
export const CLEAR_GRAPH = 'Graph/Internal/CLEAR_GRAPH';
export const UPDATE_SELECTED_GRAPH_ELEMENT =
  'Graph/Internal/UPDATE_SELECTED_GRAPH_ELEMENT';
export const DELETE_SELECTED_GRAPH_ELEMENT =
  'Graph/Internal/DELETE_SELECTED_GRAPH_ELEMENT';
export const UPDATE_GRAPH_INFO = 'Graph/Internal/UPDATE_GRAPH_INFO';
export const UPDATE_GRAPH_SCHEME = 'Graph/Internal/UPDATE_GRAPH_SCHEME';
export const ADD_NODE = 'Graph/Internal/ADD_NODE';
export const ADD_NODE_COMPLETED = 'Graph/Internal/ADD_NODE_COMPLETED';
export const ADD_EDGE = 'Graph/Internal/ADD_EDGE';
export const FIT_GRAPH = 'Graph/Internal/FIT_GRAPH';
export const FIT_GRAPH_COMPLETED = 'Graph/Internal/FIT_GRAPH_COMPLETED';
export const CENTER_GRAPH = 'Graph/Internal/CENTER_GRAPH';
export const CENTER_GRAPH_COMPLETED = 'Graph/Internal/CENTER_GRAPH_COMPLETED';
