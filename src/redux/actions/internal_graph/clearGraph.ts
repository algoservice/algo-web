import { Dispatch } from 'redux';

import { CLEAR_GRAPH } from './graphInternalActionTypes';

export type ClearGraphInternalAction = {
  type: typeof CLEAR_GRAPH;
};

export const clearGraph =
  () => (dispatch: Dispatch<ClearGraphInternalAction>) => {
    dispatch({ type: CLEAR_GRAPH });
  };
