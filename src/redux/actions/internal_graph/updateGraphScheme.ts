import { Dispatch } from 'redux';

import { UPDATE_GRAPH_SCHEME } from './graphInternalActionTypes';

import { GraphScheme } from '../../../graph/types/scheme/GraphScheme';

export type UpdateGraphSchemeInternalAction = {
  type: typeof UPDATE_GRAPH_SCHEME;
  payload: GraphScheme;
};

export const updateGraphScheme =
  (newGraphScheme: GraphScheme) =>
  (dispatch: Dispatch<UpdateGraphSchemeInternalAction>) => {
    dispatch({ type: UPDATE_GRAPH_SCHEME, payload: newGraphScheme });
  };
