import { Dispatch } from 'redux';

import { ADD_EDGE } from './graphInternalActionTypes';

import { GraphScheme } from '../../../graph/types/scheme/GraphScheme';

export type AddEdgeInternalAction = {
  type: typeof ADD_EDGE;
  payload: GraphScheme;
};

export const addEdge =
  (newScheme: GraphScheme) => (dispatch: Dispatch<AddEdgeInternalAction>) => {
    dispatch({ type: ADD_EDGE, payload: newScheme });
  };
