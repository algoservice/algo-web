import { Dispatch } from 'redux';

import { FIT_GRAPH_COMPLETED } from './graphInternalActionTypes';

export type FitGraphCompletedInternalAction = {
  type: typeof FIT_GRAPH_COMPLETED;
};

export const fitGraphCompleted =
  () => (dispatch: Dispatch<FitGraphCompletedInternalAction>) => {
    dispatch({ type: FIT_GRAPH_COMPLETED });
  };
