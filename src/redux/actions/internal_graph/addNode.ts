import { Dispatch } from 'redux';

import { ADD_NODE } from './graphInternalActionTypes';

import { GraphNode } from '../../../graph/types/node/GraphNode';

export type AddNodeInternalAction = {
  type: typeof ADD_NODE;
  payload: GraphNode;
};

export const addNode =
  (newNode: GraphNode) => (dispatch: Dispatch<AddNodeInternalAction>) => {
    dispatch({ type: ADD_NODE, payload: newNode });
  };
