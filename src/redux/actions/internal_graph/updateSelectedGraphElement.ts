import { Dispatch } from 'redux';

import { UPDATE_SELECTED_GRAPH_ELEMENT } from './graphInternalActionTypes';

import { GraphNode } from '../../../graph/types/node/GraphNode';
import { GraphEdge } from '../../../graph/types/edge/GraphEdge';

export type UpdateSelectedGraphElementInternalAction = {
  type: typeof UPDATE_SELECTED_GRAPH_ELEMENT;
  payload: GraphNode | GraphEdge | null;
};

export const updateSelectedGraphElement =
  (selectedElement: GraphNode | GraphEdge | null) =>
  (dispatch: Dispatch<UpdateSelectedGraphElementInternalAction>) => {
    dispatch({ type: UPDATE_SELECTED_GRAPH_ELEMENT, payload: selectedElement });
  };
