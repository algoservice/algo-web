import { Dispatch } from 'redux';

import { ADD_NODE_COMPLETED } from './graphInternalActionTypes';

import { GraphScheme } from '../../../graph/types/scheme/GraphScheme';

export type AddNodeCompletedInternalAction = {
  type: typeof ADD_NODE_COMPLETED;
  payload: GraphScheme;
};

export const addNodeCompleted =
  (newScheme: GraphScheme) =>
  (dispatch: Dispatch<AddNodeCompletedInternalAction>) => {
    dispatch({ type: ADD_NODE_COMPLETED, payload: newScheme });
  };
