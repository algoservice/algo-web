import { Dispatch } from 'redux';

import { DELETE_SELECTED_GRAPH_ELEMENT } from './graphInternalActionTypes';

export type DeleteSelectedGraphElementInternalAction = {
  type: typeof DELETE_SELECTED_GRAPH_ELEMENT;
};

export const deleteSelectedGraphElement =
  () => (dispatch: Dispatch<DeleteSelectedGraphElementInternalAction>) => {
    dispatch({ type: DELETE_SELECTED_GRAPH_ELEMENT });
  };
