import { Dispatch } from 'redux';

import { FIT_GRAPH } from './graphInternalActionTypes';

export type FitGraphInternalAction = {
  type: typeof FIT_GRAPH;
};

export const fitGraph = () => (dispatch: Dispatch<FitGraphInternalAction>) => {
  dispatch({ type: FIT_GRAPH });
};
