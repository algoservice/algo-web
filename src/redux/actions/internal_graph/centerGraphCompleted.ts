import { Dispatch } from 'redux';

import { CENTER_GRAPH_COMPLETED } from './graphInternalActionTypes';

export type CenterGraphCompletedInternalAction = {
  type: typeof CENTER_GRAPH_COMPLETED;
};

export const centerGraphCompleted =
  () => (dispatch: Dispatch<CenterGraphCompletedInternalAction>) => {
    dispatch({ type: CENTER_GRAPH_COMPLETED });
  };
