import { Dispatch } from 'redux';

import { UPDATE_GRAPH_INFO } from './graphInternalActionTypes';

import { GraphData } from '../../../graph/types/data/GraphData';

export type UpdateGraphInfoInternalAction = {
  type: typeof UPDATE_GRAPH_INFO;
  payload: GraphData;
};

export const updateGraphInfo =
  (newGraphInfo: GraphData) =>
  (dispatch: Dispatch<UpdateGraphInfoInternalAction>) => {
    dispatch({ type: UPDATE_GRAPH_INFO, payload: newGraphInfo });
  };
