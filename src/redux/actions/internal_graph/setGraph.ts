import { Dispatch } from 'redux';

import { SET_GRAPH } from './graphInternalActionTypes';
import { MetaDataSchemeGraph } from '../../../graph/types/MetaDataSchemeGraph';

export type SetGraphInternalAction = {
  type: typeof SET_GRAPH;
  payload: MetaDataSchemeGraph;
};

export const setGraph =
  (graph: MetaDataSchemeGraph) =>
  (dispatch: Dispatch<SetGraphInternalAction>) => {
    dispatch({ type: SET_GRAPH, payload: graph });
  };
