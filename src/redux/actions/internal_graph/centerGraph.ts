import { Dispatch } from 'redux';

import { CENTER_GRAPH } from './graphInternalActionTypes';

export type CenterGraphInternalAction = {
  type: typeof CENTER_GRAPH;
};

export const centerGraph =
  () => (dispatch: Dispatch<CenterGraphInternalAction>) => {
    dispatch({ type: CENTER_GRAPH });
  };
