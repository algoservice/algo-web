import { Dispatch } from 'redux';

import { SWITCH_THEME_MODE } from './themeActionTypes';

export type SwitchThemeModeAction = {
  type: typeof SWITCH_THEME_MODE;
};

export const switchThemeMode =
  () => (dispatch: Dispatch<SwitchThemeModeAction>) => {
    dispatch({ type: SWITCH_THEME_MODE });
  };
