// Graph regular actions
export const GET_GRAPH_REQUEST = 'Graph/Request/GET_GRAPH_REQUEST';
export const GET_GRAPH_RECEIVE = 'Graph/Receive/GET_GRAPH_REQUEST';
export const GET_GRAPH_ERROR = 'Graph/Error/GET_GRAPH_REQUEST';

export const GET_GRAPHS_REQUEST = 'Graph/Request/GET_GRAPHS_REQUEST';
export const GET_GRAPHS_RECEIVE = 'Graph/Receive/GET_GRAPHS_REQUEST';
export const GET_GRAPHS_ERROR = 'Graph/Error/GET_GRAPHS_REQUEST';

export const CREATE_GRAPH_REQUEST = 'Graph/Request/CREATE_GRAPH_REQUEST';
export const CREATE_GRAPH_RECEIVE = 'Graph/Receive/CREATE_GRAPH_REQUEST';
export const CREATE_GRAPH_ERROR = 'Graph/Error/CREATE_GRAPH_REQUEST';

export const UPDATE_GRAPH_REQUEST = 'Graph/Request/UPDATE_GRAPH_REQUEST';
export const UPDATE_GRAPH_RECEIVE = 'Graph/Receive/UPDATE_GRAPH_REQUEST';
export const UPDATE_GRAPH_ERROR = 'Graph/Error/UPDATE_GRAPH_REQUEST';
