import { Dispatch } from 'redux';

import {
  UPDATE_GRAPH_ERROR,
  UPDATE_GRAPH_RECEIVE,
  UPDATE_GRAPH_REQUEST,
} from './graphActionTypes';
import { GraphMetaId } from '../../../graph/types/meta/GraphMetaId';
import { DataSchemeGraph } from '../../../graph/types/DataSchemeGraph';

export type UpdateGraphRequestAction = {
  type: typeof UPDATE_GRAPH_REQUEST;
};
export type UpdateGraphReceiveAction = {
  type: typeof UPDATE_GRAPH_RECEIVE;
};
export type UpdateGraphErrorAction = {
  type: typeof UPDATE_GRAPH_ERROR;
  errorMessage?: string;
};

export type UpdateGraphAction =
  | UpdateGraphRequestAction
  | UpdateGraphReceiveAction
  | UpdateGraphErrorAction;

export const updateGraph =
  (graphId: GraphMetaId, graphData: DataSchemeGraph) =>
  async (dispatch: Dispatch<UpdateGraphAction>) => {
    dispatch({ type: UPDATE_GRAPH_REQUEST });
    try {
      // TODO (Michael): Remove mock graphs request
      await new Promise((resolve) => setTimeout(resolve, 500));
      dispatch({ type: UPDATE_GRAPH_RECEIVE });
    } catch (e) {
      if (e instanceof Error) {
        dispatch({ type: UPDATE_GRAPH_ERROR, errorMessage: e.message });
      } else {
        dispatch({ type: UPDATE_GRAPH_ERROR });
      }
    }
  };
