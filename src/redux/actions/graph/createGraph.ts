import { Dispatch } from 'redux';

import {
  CREATE_GRAPH_ERROR,
  CREATE_GRAPH_RECEIVE,
  CREATE_GRAPH_REQUEST,
} from './graphActionTypes';
import { GraphMetaId } from '../../../graph/types/meta/GraphMetaId';
import { DataSchemeGraph } from '../../../graph/types/DataSchemeGraph';

export type CreateGraphRequestAction = {
  type: typeof CREATE_GRAPH_REQUEST;
};
export type CreateGraphReceiveAction = {
  type: typeof CREATE_GRAPH_RECEIVE;
  payload: GraphMetaId;
};
export type CreateGraphErrorAction = {
  type: typeof CREATE_GRAPH_ERROR;
  errorMessage?: string;
};

export type CreateGraphAction =
  | CreateGraphRequestAction
  | CreateGraphReceiveAction
  | CreateGraphErrorAction;

export const createGraph =
  (graphData: DataSchemeGraph) =>
  async (dispatch: Dispatch<CreateGraphAction>) => {
    dispatch({ type: CREATE_GRAPH_REQUEST });
    try {
      // TODO (Michael): Remove mock graphs request
      await new Promise((resolve) => setTimeout(resolve, 500));
      dispatch({ type: CREATE_GRAPH_RECEIVE, payload: '1' });
    } catch (e) {
      if (e instanceof Error) {
        dispatch({ type: CREATE_GRAPH_ERROR, errorMessage: e.message });
      } else {
        dispatch({ type: CREATE_GRAPH_ERROR });
      }
    }
  };
