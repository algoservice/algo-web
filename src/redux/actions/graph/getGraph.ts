import { Dispatch } from 'redux';

import {
  GET_GRAPH_ERROR,
  GET_GRAPH_RECEIVE,
  GET_GRAPH_REQUEST,
} from './graphActionTypes';
import { MetaDataSchemeGraph } from '../../../graph/types/MetaDataSchemeGraph';
import { GraphMetaId } from '../../../graph/types/meta/GraphMetaId';

import { publicFiles } from '../../../constants/public/files';

export type GetGraphRequestAction = {
  type: typeof GET_GRAPH_REQUEST;
};
export type GetGraphReceiveAction = {
  type: typeof GET_GRAPH_RECEIVE;
  payload: MetaDataSchemeGraph;
};
export type GetGraphErrorAction = {
  type: typeof GET_GRAPH_ERROR;
  errorMessage?: string;
};

export type GetGraphAction =
  | GetGraphRequestAction
  | GetGraphReceiveAction
  | GetGraphErrorAction;

export const getGraph =
  (graphId: GraphMetaId) => async (dispatch: Dispatch<GetGraphAction>) => {
    dispatch({ type: GET_GRAPH_REQUEST });
    try {
      // TODO (Michael): Remove mock graphs request
      await new Promise((resolve) => setTimeout(resolve, 500));
      const stub: MetaDataSchemeGraph = await fetch(
        publicFiles.mockapi.simpleGraph,
      ).then((res) => res.json());
      dispatch({ type: GET_GRAPH_RECEIVE, payload: stub });
    } catch (e) {
      if (e instanceof Error) {
        dispatch({ type: GET_GRAPH_ERROR, errorMessage: e.message });
      } else {
        dispatch({ type: GET_GRAPH_ERROR });
      }
    }
  };
