import { Dispatch } from 'redux';

import {
  GET_GRAPHS_ERROR,
  GET_GRAPHS_RECEIVE,
  GET_GRAPHS_REQUEST,
} from './graphActionTypes';
import { MetaDataGraph } from '../../../graph/types/MetaDataGraph';
import { GraphSearch } from '../../../search/types/GraphSearch';

import { publicFiles } from '../../../constants/public/files';

export type GetGraphsRequestAction = {
  type: typeof GET_GRAPHS_REQUEST;
};
export type GetGraphsReceiveAction = {
  type: typeof GET_GRAPHS_RECEIVE;
  payload: MetaDataGraph[];
};
export type GetGraphsErrorAction = {
  type: typeof GET_GRAPHS_ERROR;
  errorMessage?: string;
};

export type GetGraphsAction =
  | GetGraphsRequestAction
  | GetGraphsReceiveAction
  | GetGraphsErrorAction;

export const getGraphs =
  (graphSearch: GraphSearch) => async (dispatch: Dispatch<GetGraphsAction>) => {
    dispatch({ type: GET_GRAPHS_REQUEST });
    try {
      // TODO (Michael): Remove mock graphs request
      await new Promise((resolve) => setTimeout(resolve, 1000));
      const graphs: MetaDataGraph[] = await fetch(
        publicFiles.mockapi.graphInfos,
      ).then((res) => res.json());
      const stubGraphs = graphs.slice(
        graphSearch.searchPagination.page * graphSearch.searchPagination.size,
        (graphSearch.searchPagination.page + 1) *
          graphSearch.searchPagination.size,
      );

      dispatch({ type: GET_GRAPHS_RECEIVE, payload: stubGraphs });
    } catch (e) {
      if (e instanceof Error) {
        dispatch({ type: GET_GRAPHS_ERROR, errorMessage: e.message });
      } else {
        dispatch({ type: GET_GRAPHS_ERROR });
      }
    }
  };
