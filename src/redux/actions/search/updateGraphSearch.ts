import { Dispatch } from 'redux';

import { GraphSearch } from '../../../search/types/GraphSearch';
import { UPDATE_GRAPH_SEARCH } from './searchActionTypes';

export type UpdateGraphSearchAction = {
  type: typeof UPDATE_GRAPH_SEARCH;
  payload: GraphSearch;
};

export const updateGraphSearch =
  (graphSearch: GraphSearch) =>
  (dispatch: Dispatch<UpdateGraphSearchAction>) => {
    dispatch({ type: UPDATE_GRAPH_SEARCH, payload: graphSearch });
  };
