import { Dispatch } from 'redux';

import { UPDATE_GRAPH_SEARCH_STATUS } from './searchActionTypes';

import { SearchStatus } from '../../../search/types/common/SearchStatus';

export type UpdateGraphSearchStatusAction = {
  type: typeof UPDATE_GRAPH_SEARCH_STATUS;
  payload: SearchStatus;
};

export const updateGraphSearchStatus =
  (graphSearchStatus: SearchStatus) =>
  (dispatch: Dispatch<UpdateGraphSearchStatusAction>) => {
    dispatch({ type: UPDATE_GRAPH_SEARCH_STATUS, payload: graphSearchStatus });
  };
