import { GraphSearch } from '../../search/types/GraphSearch';
import makeGraphSearch from '../../search/makers/makeGraphSearch';
import { SearchStatus } from '../../search/types/common/SearchStatus';
import {
  UPDATE_GRAPH_SEARCH,
  UPDATE_GRAPH_SEARCH_STATUS,
} from '../actions/search/searchActionTypes';
import { UpdateGraphSearchAction } from '../actions/search/updateGraphSearch';
import { UpdateGraphSearchStatusAction } from '../actions/search/updateGraphSearchStatus';

interface SearchStore {
  graphSearch: GraphSearch;
  graphSearchStatus: SearchStatus;
}

const initialState: SearchStore = {
  graphSearch: makeGraphSearch(),
  graphSearchStatus: SearchStatus.AVAILABLE,
};

export type SearchReducerTypes =
  | UpdateGraphSearchAction
  | UpdateGraphSearchStatusAction;

export const searchReducer = (
  state: SearchStore = initialState,
  action: SearchReducerTypes,
): SearchStore => {
  switch (action.type) {
    case UPDATE_GRAPH_SEARCH: {
      return {
        ...state,
        graphSearch: makeGraphSearch(action.payload),
      };
    }
    case UPDATE_GRAPH_SEARCH_STATUS: {
      return {
        ...state,
        graphSearchStatus: action.payload,
      };
    }
    default:
      return state;
  }
};
