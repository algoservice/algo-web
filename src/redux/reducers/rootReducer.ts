import { combineReducers } from '@reduxjs/toolkit';

import { themeReducer } from './themeReducer';
import { appReducer } from './appReducer';
import { internalGraphReducer } from './internalGraphReducer';
import { graphReducer } from './graphReducer';
import { searchReducer } from './searchReducer';

export const rootReducer = combineReducers({
  themeReducer,
  appReducer,
  internalGraphReducer,
  graphReducer,
  searchReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
