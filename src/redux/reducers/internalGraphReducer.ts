import { MetaDataSchemeGraph } from '../../graph/types/MetaDataSchemeGraph';
import { emptyGraph } from '../../graph/instances/emptyGraph';
import { emptyNode } from '../../graph/instances/emptyNode';
import { GraphNode } from '../../graph/types/node/GraphNode';
import { GraphEdge } from '../../graph/types/edge/GraphEdge';
import {
  ADD_EDGE,
  ADD_NODE,
  ADD_NODE_COMPLETED,
  CENTER_GRAPH,
  CENTER_GRAPH_COMPLETED,
  CLEAR_GRAPH,
  DELETE_SELECTED_GRAPH_ELEMENT,
  FIT_GRAPH,
  FIT_GRAPH_COMPLETED,
  GraphInternalActionState,
  SET_GRAPH,
  UPDATE_GRAPH_INFO,
  UPDATE_GRAPH_SCHEME,
  UPDATE_SELECTED_GRAPH_ELEMENT,
} from '../actions/internal_graph/graphInternalActionTypes';
import { FitGraphInternalAction } from '../actions/internal_graph/fitGraph';
import { FitGraphCompletedInternalAction } from '../actions/internal_graph/fitGraphCompleted';
import { ClearGraphInternalAction } from '../actions/internal_graph/clearGraph';
import { CenterGraphInternalAction } from '../actions/internal_graph/centerGraph';
import { CenterGraphCompletedInternalAction } from '../actions/internal_graph/centerGraphCompleted';
import { UpdateGraphInfoInternalAction } from '../actions/internal_graph/updateGraphInfo';
import { AddNodeInternalAction } from '../actions/internal_graph/addNode';
import { AddNodeCompletedInternalAction } from '../actions/internal_graph/addNodeCompleted';
import { UpdateGraphSchemeInternalAction } from '../actions/internal_graph/updateGraphScheme';
import { UpdateSelectedGraphElementInternalAction } from '../actions/internal_graph/updateSelectedGraphElement';
import { AddEdgeInternalAction } from '../actions/internal_graph/addEdge';
import { DeleteSelectedGraphElementInternalAction } from '../actions/internal_graph/deleteSelectedGraphElement';
import { SetGraphInternalAction } from '../actions/internal_graph/setGraph';

interface InternalGraphStore {
  graph: MetaDataSchemeGraph;
  newNode: GraphNode;
  selectedElement: GraphNode | GraphEdge | null;
  addNode: GraphInternalActionState;
  addEdge: GraphInternalActionState;
  fitGraph: GraphInternalActionState;
  centerGraph: GraphInternalActionState;
}

const initialState: InternalGraphStore = {
  graph: emptyGraph,
  newNode: emptyNode,
  selectedElement: null,
  addNode: GraphInternalActionState.NOT_REQUIRED,
  addEdge: GraphInternalActionState.NOT_REQUIRED,
  fitGraph: GraphInternalActionState.NOT_REQUIRED,
  centerGraph: GraphInternalActionState.NOT_REQUIRED,
};

export type InternalGraphReducerTypes =
  | SetGraphInternalAction
  | ClearGraphInternalAction
  | UpdateSelectedGraphElementInternalAction
  | DeleteSelectedGraphElementInternalAction
  | AddNodeInternalAction
  | AddNodeCompletedInternalAction
  | AddEdgeInternalAction
  | UpdateGraphInfoInternalAction
  | UpdateGraphSchemeInternalAction
  | FitGraphInternalAction
  | FitGraphCompletedInternalAction
  | CenterGraphInternalAction
  | CenterGraphCompletedInternalAction;

export const internalGraphReducer = (
  state = initialState,
  action: InternalGraphReducerTypes,
): InternalGraphStore => {
  switch (action.type) {
    case SET_GRAPH: {
      return {
        ...state,
        graph: action.payload,
        newNode: emptyNode,
        selectedElement: null,
      };
    }
    case CLEAR_GRAPH: {
      return {
        ...state,
        graph: emptyGraph,
        newNode: emptyNode,
        selectedElement: null,
      };
    }
    case UPDATE_SELECTED_GRAPH_ELEMENT: {
      return {
        ...state,
        selectedElement: action.payload,
      };
    }
    case DELETE_SELECTED_GRAPH_ELEMENT: {
      return {
        ...state,
        selectedElement: null,
      };
    }
    case ADD_NODE: {
      return {
        ...state,
        newNode: action.payload,
        addNode: GraphInternalActionState.ALLOWED,
      };
    }
    case ADD_NODE_COMPLETED: {
      return {
        ...state,
        graph: {
          ...state.graph,
          scheme: action.payload,
        },
        newNode: emptyNode,
        addNode: GraphInternalActionState.NOT_REQUIRED,
      };
    }
    case ADD_EDGE: {
      return {
        ...state,
        graph: {
          ...state.graph,
          scheme: action.payload,
        },
        addEdge: GraphInternalActionState.NOT_REQUIRED,
      };
    }
    case UPDATE_GRAPH_INFO: {
      return {
        ...state,
        graph: {
          ...state.graph,
          data: action.payload,
        },
      };
    }
    case UPDATE_GRAPH_SCHEME: {
      return {
        ...state,
        graph: {
          ...state.graph,
          scheme: action.payload,
        },
      };
    }
    case FIT_GRAPH: {
      return {
        ...state,
        fitGraph: GraphInternalActionState.ALLOWED,
      };
    }
    case FIT_GRAPH_COMPLETED: {
      return {
        ...state,
        fitGraph: GraphInternalActionState.NOT_REQUIRED,
      };
    }
    case CENTER_GRAPH: {
      return {
        ...state,
        centerGraph: GraphInternalActionState.ALLOWED,
      };
    }
    case CENTER_GRAPH_COMPLETED: {
      return {
        ...state,
        centerGraph: GraphInternalActionState.NOT_REQUIRED,
      };
    }
    default:
      return state;
  }
};
