import { GetGraphAction } from '../actions/graph/getGraph';
import { CreateGraphAction } from '../actions/graph/createGraph';
import { UpdateGraphAction } from '../actions/graph/updateGraph';
import { GetGraphsAction } from '../actions/graph/getGraphs';
import {
  CREATE_GRAPH_ERROR,
  CREATE_GRAPH_RECEIVE,
  CREATE_GRAPH_REQUEST,
  GET_GRAPH_ERROR,
  GET_GRAPH_RECEIVE,
  GET_GRAPH_REQUEST,
  GET_GRAPHS_ERROR,
  GET_GRAPHS_RECEIVE,
  GET_GRAPHS_REQUEST,
  UPDATE_GRAPH_ERROR,
  UPDATE_GRAPH_RECEIVE,
  UPDATE_GRAPH_REQUEST,
} from '../actions/graph/graphActionTypes';
import { Graph } from '../../graph/types/Graph';
import { emptyGraphMeta } from '../../graph/instances/emptyGraphMeta';

interface GraphStore {
  graph: Graph | null;
  graphs: Graph[];
  loading: boolean;
  success: boolean;
  errorMessage: string | null;
}

const initialState: GraphStore = {
  graph: null,
  graphs: [],
  loading: false,
  success: false,
  errorMessage: null,
};

export type GraphReducerTypes =
  | GetGraphAction
  | GetGraphsAction
  | CreateGraphAction
  | UpdateGraphAction;

export const graphReducer = (
  state = initialState,
  action: GraphReducerTypes,
): GraphStore => {
  switch (action.type) {
    case GET_GRAPH_REQUEST:
    case GET_GRAPHS_REQUEST:
    case CREATE_GRAPH_REQUEST:
    case UPDATE_GRAPH_REQUEST: {
      return {
        ...state,
        loading: true,
        success: false,
        errorMessage: null,
      };
    }
    case GET_GRAPH_RECEIVE: {
      return {
        ...state,
        loading: false,
        graph: action.payload,
        graphs: [],
        success: true,
        errorMessage: null,
      };
    }
    case GET_GRAPHS_RECEIVE: {
      return {
        ...state,
        loading: false,
        graph: null,
        graphs: action.payload,
        success: true,
        errorMessage: null,
      };
    }
    case CREATE_GRAPH_RECEIVE: {
      return {
        ...state,
        loading: false,
        graph: {
          ...state.graph,
          meta: {
            ...(state.graph ? state.graph.meta : emptyGraphMeta),
            id: action.payload,
          },
        },
        success: true,
        errorMessage: null,
      };
    }
    case UPDATE_GRAPH_RECEIVE: {
      return {
        ...state,
        loading: false,
        success: true,
        errorMessage: null,
      };
    }
    case GET_GRAPH_ERROR:
    case GET_GRAPHS_ERROR:
    case CREATE_GRAPH_ERROR:
    case UPDATE_GRAPH_ERROR: {
      return {
        ...state,
        loading: false,
        success: false,
        errorMessage: action.errorMessage ? action.errorMessage : null,
      };
    }
    default:
      return state;
  }
};
