import {
  CLOSE_APP_BAR_SIDEBAR,
  CLOSE_CONTROLS_SIDEBAR,
  CLOSE_PARAMETERS_SIDEBAR,
  OPEN_APP_BAR_SIDEBAR,
  OPEN_CONTROLS_SIDEBAR,
  OPEN_PARAMETERS_SIDEBAR,
} from '../actions/app/appActionTypes';

import { CloseAppBarSidebarAction } from '../actions/app/closeAppBarSidebar';
import { OpenAppBarSidebarAction } from '../actions/app/openAppBarSidebar';
import { OpenControlsSidebarAction } from '../actions/app/openControlsSidebar';
import { CloseControlsSidebarAction } from '../actions/app/closeControlsSidebar';
import { OpenParametersSidebarAction } from '../actions/app/openParametersSidebar';
import { CloseParametersSidebarAction } from '../actions/app/closeParametersSidebar';

interface AppStore {
  isAppBarOpened: boolean;
  isControlsOpened: boolean;
  isParametersOpened: boolean;
}

const initialState: AppStore = {
  isAppBarOpened: false,
  isControlsOpened: false,
  isParametersOpened: false,
};

export type AppBarReducerTypes =
  | OpenAppBarSidebarAction
  | CloseAppBarSidebarAction
  | OpenControlsSidebarAction
  | CloseControlsSidebarAction
  | OpenParametersSidebarAction
  | CloseParametersSidebarAction;

export const appReducer = (
  state = initialState,
  action: AppBarReducerTypes,
): AppStore => {
  switch (action.type) {
    case OPEN_APP_BAR_SIDEBAR: {
      return {
        ...state,
        isAppBarOpened: true,
      };
    }
    case CLOSE_APP_BAR_SIDEBAR: {
      return {
        ...state,
        isAppBarOpened: false,
      };
    }
    case OPEN_CONTROLS_SIDEBAR: {
      return {
        ...state,
        isControlsOpened: true,
      };
    }
    case CLOSE_CONTROLS_SIDEBAR: {
      return {
        ...state,
        isControlsOpened: false,
      };
    }
    case OPEN_PARAMETERS_SIDEBAR: {
      return {
        ...state,
        isParametersOpened: true,
      };
    }
    case CLOSE_PARAMETERS_SIDEBAR: {
      return {
        ...state,
        isParametersOpened: false,
      };
    }
    default:
      return state;
  }
};
