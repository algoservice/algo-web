import { SWITCH_THEME_MODE } from '../actions/theme/themeActionTypes';
import {
  DarkMode,
  LightMode,
  AppPaletteMode,
} from '../../constants/palettes/types/AppPaletteMode';
import { SwitchThemeModeAction } from '../actions/theme/switchThemeMode';

interface ThemeStore {
  mode: AppPaletteMode;
}

const initialState: ThemeStore = {
  mode: DarkMode,
};

export type ThemeReducerTypes = SwitchThemeModeAction;

export const themeReducer = (
  state = initialState,
  action: ThemeReducerTypes,
): ThemeStore => {
  switch (action.type) {
    case SWITCH_THEME_MODE: {
      return {
        ...state,
        mode: state.mode === LightMode ? DarkMode : LightMode,
      };
    }
    default:
      return state;
  }
};
