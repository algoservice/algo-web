import { useDispatch } from 'react-redux';

import store from '../store/store';

export type RootDispatch = typeof store.dispatch;

export const useRootDispatch: () => RootDispatch = useDispatch;
