import React, { StrictMode } from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import { BrowserRouter, Outlet, Route, Routes } from 'react-router-dom';

import store from './redux/store/store';

import AppTheme from './elements/themes/app/AppTheme';
import MainBarLayout from './elements/layouts/main_bar/MainBarLayout';
import MainLayout from './elements/layouts/main/MainLayout';
import BlankLayout from './elements/layouts/blank/BlankLayout';
import FooterLayout from './elements/layouts/footer/FooterLayout';
import EditorPage from './elements/pages/editor/EditorPage';
import NotFoundPage from './elements/pages/notfound/NotFoundPage';
import ViewPage from './elements/pages/view/ViewPage';
import HomePage from './elements/pages/home/HomePage';
import GraphsPage from './elements/pages/graphs/GraphsPage';

import { navigationScheme } from './constants/navigation/navigationScheme';
import './index.css';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement,
);

// @ts-ignore
root.render(
  <StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <AppTheme>
          <Routes>
            <Route path={navigationScheme.home.url} element={<Outlet />}>
              <Route element={<BlankLayout elevated />}>
                <Route element={<MainBarLayout elevated />}>
                  <Route
                    path={navigationScheme.editor.url}
                    element={<EditorPage />}
                  />
                  <Route
                    path={navigationScheme.view.url}
                    element={<ViewPage />}
                  />
                </Route>
              </Route>
              <Route element={<MainLayout />}>
                <Route
                  path={navigationScheme.home.url}
                  element={<HomePage />}
                />
                <Route element={<FooterLayout />}>
                  <Route element={<MainBarLayout elevated />}>
                    <Route
                      path={navigationScheme.any.url}
                      element={<NotFoundPage />}
                    />
                    <Route
                      path={navigationScheme.graphs.url}
                      element={<GraphsPage />}
                    />
                  </Route>
                </Route>
              </Route>
            </Route>
          </Routes>
        </AppTheme>
      </BrowserRouter>
    </Provider>
  </StrictMode>,
);
