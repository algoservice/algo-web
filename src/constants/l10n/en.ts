export const en = {
  serviceName: 'Algo',
  footer: {
    copyright: {
      title: '© 2023 Algo',
    },
    links: {
      feedback: 'Feedback',
      license: 'License',
    },
    dev: {
      createdBy: 'Created by:',
      creator: 'Michael Linker',
    },
  },
  pages: {
    notFound: {
      header: 'The page are you looking for does not exist.',
      fzf: '404',
      back: 'GO BACK HOME',
    },
    home: {
      header: 'Algo',
      search: {
        placeholder: 'Search Algo',
      },
    },
    graphs: {
      notFound: 'There`s no graphs yet',
    },
  },
  components: {
    inputs: {
      singleline: {
        defaultHintOnEmpty: 'This field is required',
        defaultHintOnRestricted: 'This value is not allowed',
      },
    },
  },
  graph: {
    loading: 'Loading algo-graph...',
    notFound: 'Failed to load algo-graph.',
    back: 'GO BACK HOME',
    editor: {
      sidebar: {
        controls: {
          heading: 'Graph settings',
          sections: {
            general: {
              heading: 'General settings',
              nameInput: {
                label: 'Graph name',
                hintOnEmpty: 'Graph name is required',
                hintOnRestricted: 'This graph name is already in use',
              },
            },
            elements: {
              heading: 'Elements',
              inputNode: {
                heading: 'Input Node',
              },
              outputNode: {
                heading: 'Output Node',
              },
              transputNode: {
                heading: 'Transput Node',
              },
            },
          },
          buttons: {
            save: {
              text: 'Save',
              tooltip: 'Save graph',
            },
          },
        },
        parameters: {
          heading: 'Element parameters',
          type: {
            empty: {
              hint: 'Please select any node or edge',
            },
            node: {
              sections: {
                data: {
                  heading: 'Data',
                  labelInput: {
                    label: 'Node label',
                    hintOnEmpty: 'Node label is required',
                  },
                },
                position: {
                  heading: 'Position',
                  xInput: {
                    label: 'X',
                  },
                  yInput: {
                    label: 'Y',
                  },
                },
                connection: {
                  heading: 'Connections',
                  typeSelector: {
                    label: 'Node type',
                  },
                  input: 'Input connections',
                  output: 'Output connections',
                },
              },
              delete: 'Delete node',
            },
            edge: {
              sections: {
                data: {
                  heading: 'Data',
                  labelInput: {
                    label: 'Edge label',
                  },
                },
                connection: {
                  heading: 'Connections',
                  source: {
                    label: 'Source',
                  },
                  target: {
                    label: 'Target',
                  },
                },
              },
              delete: 'Delete edge',
            },
          },
        },
      },
    },
    scheme: {
      placeholder: {
        node: 'Node',
      },
    },
  },
  menu: {
    theme: {
      tooltip: 'Switch theme',
    },
    create: {
      tooltip: 'Create new...',
      options: {
        new: 'New graph',
        import: 'Import graph',
      },
    },
  },
};
