export interface GraphPalette {
  background: string;
  primary: string;
  node: {
    background: string;
    primary: string;
    border: {
      normal: string;
      selected: string;
    };
  };
  edge: {
    text: {
      fill: string;
      background: {
        fill: string;
      };
    };
  };
  handle: {
    background: string;
  };
  minimap: {
    background: string;
    mask: {
      background: string;
    };
    node: {
      fill: string;
    };
  };
  controls: {
    button: {
      background: {
        normal: string;
        hover: string;
      };
      primary: string;
      border: string;
    };
  };
  attribution: {
    background: string;
    textColor: string;
  };
}
