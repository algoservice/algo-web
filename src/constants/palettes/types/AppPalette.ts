import { Palette } from '@mui/material/styles/createPalette';

export type AppPalette = Partial<Palette>;
