import { PaletteMode } from '@mui/material';

export const LightMode: PaletteMode = 'light';
export const DarkMode: PaletteMode = 'dark';

export type AppPaletteMode = typeof LightMode | typeof DarkMode;
