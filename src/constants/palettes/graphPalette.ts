import { AppPaletteMode, LightMode } from './types/AppPaletteMode';
import { GraphPalette } from './types/GraphPalette';
import { AppTheme } from '../../hooks/theme/types/AppTheme';

export const createGraphPalette = (
  appTheme: AppTheme,
  mode: AppPaletteMode,
): GraphPalette => ({
  ...(mode === LightMode
    ? {
        background: appTheme.palette.background.paper,
        primary: appTheme.palette.primary.main,
        node: {
          background: appTheme.palette.background.default,
          primary: appTheme.palette.text.primary,
          border: {
            normal: appTheme.palette.grey['900'],
            selected: appTheme.palette.primary.main,
          },
        },
        edge: {
          text: {
            fill: appTheme.palette.text.primary,
            background: {
              fill: appTheme.palette.background.paper,
            },
          },
        },
        handle: {
          background: appTheme.palette.primary.main,
        },
        minimap: {
          background: appTheme.palette.background.paper,
          mask: {
            background: appTheme.palette.background.default,
          },
          node: {
            fill: appTheme.palette.background.default,
          },
        },
        controls: {
          button: {
            background: {
              normal: appTheme.palette.background.default,
              hover: appTheme.palette.grey.A200,
            },
            primary: appTheme.palette.text.primary,
            border: appTheme.palette.grey.A400,
          },
        },
        attribution: {
          background: appTheme.palette.background.default,
          textColor: appTheme.palette.text.primary,
        },
      }
    : {
        background: appTheme.palette.background.paper,
        primary: appTheme.palette.primary.main,
        node: {
          background: appTheme.palette.background.default,
          primary: appTheme.palette.text.primary,
          border: {
            normal: appTheme.palette.grey['900'],
            selected: appTheme.palette.primary.main,
          },
        },
        edge: {
          text: {
            fill: appTheme.palette.text.primary,
            background: {
              fill: appTheme.palette.background.paper,
            },
          },
        },
        handle: {
          background: appTheme.palette.primary.main,
        },
        minimap: {
          background: appTheme.palette.background.paper,
          mask: {
            background: appTheme.palette.background.default,
          },
          node: {
            fill: appTheme.palette.background.default,
          },
        },
        controls: {
          button: {
            background: {
              normal: appTheme.palette.background.default,
              hover: appTheme.palette.grey['700'],
            },
            primary: appTheme.palette.text.primary,
            border: appTheme.palette.grey['700'],
          },
        },
        attribution: {
          background: appTheme.palette.background.default,
          textColor: appTheme.palette.text.primary,
        },
      }),
});
