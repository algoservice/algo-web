import { AppPaletteMode, LightMode } from './types/AppPaletteMode';
import { AppPalette } from './types/AppPalette';

export const createAppPalette = (
  mode: AppPaletteMode,
): { palette: AppPalette } => ({
  palette: {
    mode,
    ...(mode === LightMode
      ? {
          // Light theme
          primary: {
            main: '#1976d2',
          },
          secondary: {
            main: '#ff9c0a',
          },
          background: {
            paper: '#fff',
            default: '#dcdcdc',
          },
        }
      : {
          // Dark theme
          primary: {
            main: '#1976d2',
          },
          secondary: {
            main: '#ff9c0a',
          },
          background: {
            paper: '#121212',
            default: '#242424',
          },
        }),
  } as AppPalette,
});
