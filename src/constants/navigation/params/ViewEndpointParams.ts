import { GraphMetaId } from '../../../graph/types/meta/GraphMetaId';

export type ViewEndpointParams = {
  graphId: GraphMetaId;
};
