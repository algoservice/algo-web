import { Endpoint } from './Endpoint';

export type ParamsEndpoint<T> = Endpoint & {
  params: T;
};
