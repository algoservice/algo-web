import { Endpoint } from './endpoints/Endpoint';
import { ViewEndpointParams } from './params/ViewEndpointParams';
import { ParamsEndpoint } from './endpoints/ParamsEndpoint';

type NavigationScheme = {
  home: Endpoint;
  graphs: Endpoint;
  editor: Endpoint;
  view: ParamsEndpoint<ViewEndpointParams>;
  any: Endpoint;
};

export const navigationScheme: NavigationScheme = {
  home: {
    url: '/',
  },
  graphs: {
    url: 'graphs',
  },
  editor: {
    url: 'editor',
  },
  view: {
    url: ':algoGraphId',
    params: {
      graphId: '',
    },
  },
  any: {
    url: '*',
  },
};
