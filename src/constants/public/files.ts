import { publicPaths } from './paths';

export const publicFiles = {
  mockapi: {
    simpleGraph: `${publicPaths.mockapi.path}/simple-graph.json`,
    graphInfos: `${publicPaths.mockapi.path}/graph-infos.json`,
  },
  static: {
    images: {
      DarkGraphInputNode: `${publicPaths.static.images.path}/DarkGraphInputNode.png`,
      DarkGraphOutputNode: `${publicPaths.static.images.path}/DarkGraphOutputNode.png`,
      DarkGraphTransputNode: `${publicPaths.static.images.path}/DarkGraphTransputNode.png`,
      DarkLogo: `${publicPaths.static.images.path}/DarkLogo.svg`,
      LightGraphInputNode: `${publicPaths.static.images.path}/LightGraphInputNode.png`,
      LightGraphOutputNode: `${publicPaths.static.images.path}/LightGraphOutputNode.png`,
      LightGraphTransputNode: `${publicPaths.static.images.path}/LightGraphTransputNode.png`,
      LightLogo: `${publicPaths.static.images.path}/LightLogo.svg`,
      UniversalHome: `${publicPaths.static.images.path}/UniversalHome.png`,
    },
  },
};
