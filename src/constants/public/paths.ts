export const publicPaths = {
  path: '/',
  mockapi: {
    path: '/mockapi',
  },
  static: {
    path: '/static',
    images: {
      path: '/static/images',
    },
  },
};
