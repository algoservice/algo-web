import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';

import { Box } from '@mui/material';

import { useReactFlow } from 'reactflow';

import { GraphHooks } from '../hooks/GraphHooks';
import useInternalGraphScheme from '../../hooks/internal_graph/useInternalGraphScheme';

import { RootState } from '../../redux/reducers/rootReducer';
import { useRootDispatch } from '../../redux/dispatch/rootDispatch';
import { fitGraphCompleted } from '../../redux/actions/internal_graph/fitGraphCompleted';
import { centerGraphCompleted } from '../../redux/actions/internal_graph/centerGraphCompleted';
import { GraphInternalActionState } from '../../redux/actions/internal_graph/graphInternalActionTypes';
import { addNodeCompleted } from '../../redux/actions/internal_graph/addNodeCompleted';

const GraphController: React.FC<unknown> = () => {
  const dispatch = useRootDispatch();
  const { fitView, setCenter, viewportInitialized } = useReactFlow();

  const { getNodeCenterCoordinatesWithOffset } =
    GraphHooks.useGraphCoordinates();
  const { graphScheme } = useInternalGraphScheme();
  const { fitGraph, centerGraph, addNode, newNode } = useSelector(
    (state: RootState) => state.internalGraphReducer,
  );

  useEffect(() => {
    if (fitGraph === GraphInternalActionState.ALLOWED && viewportInitialized) {
      fitView({ duration: 1000 });
      dispatch(fitGraphCompleted());
    }
  }, [viewportInitialized, fitGraph, dispatch, fitView]);

  useEffect(() => {
    if (
      centerGraph === GraphInternalActionState.ALLOWED &&
      viewportInitialized
    ) {
      setCenter(0, 0);
      dispatch(centerGraphCompleted());
    }
  }, [viewportInitialized, centerGraph, dispatch, setCenter]);

  useEffect(() => {
    if (addNode === GraphInternalActionState.ALLOWED && viewportInitialized) {
      const newScheme = {
        ...graphScheme,
        nodes: [
          ...graphScheme.nodes,
          {
            ...newNode,
            position: getNodeCenterCoordinatesWithOffset(),
          },
        ],
      };
      dispatch(addNodeCompleted(newScheme));
    }
  }, [
    viewportInitialized,
    addNode,
    dispatch,
    graphScheme,
    getNodeCenterCoordinatesWithOffset,
    newNode,
  ]);

  return <Box display='none' />;
};

export default GraphController;
