import { GraphMetaId } from './GraphMetaId';

export type GraphMeta = {
  id?: GraphMetaId;
};
