import { GraphDataName } from './GraphDataName';

export type GraphData = {
  name: GraphDataName;
};
