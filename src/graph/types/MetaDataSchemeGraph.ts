import { Graph } from './Graph';
import { MetaGraph } from './MetaGraph';
import { DataGraph } from './DataGraph';
import { SchemeGraph } from './SchemeGraph';

export type MetaDataSchemeGraph = Graph & MetaGraph & DataGraph & SchemeGraph;
