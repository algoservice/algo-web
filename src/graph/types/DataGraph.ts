import { GraphData } from './data/GraphData';

export type DataGraph = {
  data: GraphData;
};
