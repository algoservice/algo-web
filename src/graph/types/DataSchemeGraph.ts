import { Graph } from './Graph';
import { DataGraph } from './DataGraph';
import { SchemeGraph } from './SchemeGraph';

export type DataSchemeGraph = Graph & DataGraph & SchemeGraph;
