import { GraphEdgeId } from './GraphEdgeId';
import { GraphNodeId } from '../node/GraphNodeId';
import { GraphEdgeLabel } from './GraphEdgeLabel';
import { GraphEdgeType } from './GraphEdgeType';

export type GraphEdge = {
  id: GraphEdgeId;
  source: GraphNodeId;
  target: GraphNodeId;
  type?: GraphEdgeType;
  label?: GraphEdgeLabel;
};
