export const DefaultEdgeType = 'default';
export const StraightEdgeType = 'straight';
export const StepEdgeType = 'step';
export const SmoothStepEdgeType = 'smoothstep';
export const SimpleBezierEdgeType = 'simplebezier';

export type GraphEdgeType =
  | typeof DefaultEdgeType
  | typeof StraightEdgeType
  | typeof StepEdgeType
  | typeof SmoothStepEdgeType
  | typeof SimpleBezierEdgeType;
