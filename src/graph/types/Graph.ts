import { MetaGraph } from './MetaGraph';
import { DataGraph } from './DataGraph';
import { SchemeGraph } from './SchemeGraph';

export type Graph = Partial<MetaGraph> &
  Partial<DataGraph> &
  Partial<SchemeGraph>;
