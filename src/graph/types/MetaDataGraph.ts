import { Graph } from './Graph';
import { MetaGraph } from './MetaGraph';
import { DataGraph } from './DataGraph';

export type MetaDataGraph = Graph & MetaGraph & DataGraph;
