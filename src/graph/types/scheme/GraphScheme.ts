import { GraphEdge } from '../edge/GraphEdge';
import { GraphNode } from '../node/GraphNode';

export type GraphScheme = {
  nodes: GraphNode[];
  edges: GraphEdge[];
};
