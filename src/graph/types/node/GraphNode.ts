import { GraphNodePosition } from './GraphNodePosition';
import { GraphNodeData } from './GraphNodeData';
import { GraphNodeType } from './GraphNodeType';
import { GraphNodeId } from './GraphNodeId';

export type GraphNode = {
  id: GraphNodeId;
  position: GraphNodePosition;
  data: GraphNodeData;
  type?: GraphNodeType;
};
