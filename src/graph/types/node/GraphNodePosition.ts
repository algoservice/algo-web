export type GraphNodePosition = {
  x: number;
  y: number;
};
