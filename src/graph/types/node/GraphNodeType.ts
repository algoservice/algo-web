export const DefaultNodeType = 'default';
export const InputNodeType = 'input';
export const OutputNodeType = 'output';
export const GroupNodeType = 'group';

export type GraphNodeType =
  | typeof DefaultNodeType
  | typeof InputNodeType
  | typeof OutputNodeType
  | typeof GroupNodeType;
