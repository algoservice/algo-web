import { GraphScheme } from './scheme/GraphScheme';

export type SchemeGraph = {
  scheme: GraphScheme;
};
