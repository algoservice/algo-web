import { GraphMeta } from './meta/GraphMeta';

export type MetaGraph = {
  meta: GraphMeta;
};
