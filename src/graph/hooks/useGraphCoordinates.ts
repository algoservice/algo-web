import { useCallback, useState } from 'react';

import { useStoreApi } from 'reactflow';

import { GraphHookType } from './GraphHooks';
import { GraphNodePosition } from '../types/node/GraphNodePosition';
import { NODE_DIMENSIONS } from '../constants/NodeDimensions';

type GraphCoordinates = {
  x: number;
  y: number;
};

interface UseGraphCoordinates {
  getViewportCenterCoordinates: () => GraphCoordinates;
  getNodeCenterCoordinates: () => GraphNodePosition;
  getNodeCenterCoordinatesWithOffset: () => GraphNodePosition;
}

/**
 * Use only in the GraphController.
 */
export const useGraphCoordinates = (): UseGraphCoordinates => {
  const { getState } = useStoreApi();

  const [previousNodeOffset, setPreviousNodeOffset] = useState<
    GraphNodePosition & { counter: number }
  >({
    x: Infinity,
    y: Infinity,
    counter: 0,
  });

  const getViewportCenterCoordinates = useCallback(() => {
    const {
      width,
      height,
      transform: [transformX, transformY, zoomLevel],
    } = getState();

    const zoomMultiplier = 1 / zoomLevel;

    return {
      x: (width * zoomMultiplier) / 2 - transformX * zoomMultiplier,
      y: (height * zoomMultiplier) / 2 - transformY * zoomMultiplier,
    };
  }, [getState]);

  const getNodeCenterCoordinates = useCallback((): GraphNodePosition => {
    const DEFAULT_ZOOM_LEVEL = 1;
    const { transform } = getState();
    const zoomLevel = transform.at(2) ?? DEFAULT_ZOOM_LEVEL;
    const zoomMultiplier = 1 / zoomLevel;

    const { x: viewportCenterX, y: viewportCenterY } =
      getViewportCenterCoordinates();

    const nodeWidthOffset = (NODE_DIMENSIONS.defaultWidth * zoomMultiplier) / 2;
    const nodeHeightOffset =
      (NODE_DIMENSIONS.defaultHeight * zoomMultiplier) / 2;

    return {
      x: viewportCenterX - nodeWidthOffset,
      y: viewportCenterY - nodeHeightOffset,
    };
  }, [getState, getViewportCenterCoordinates]);

  const getNodeCenterCoordinatesWithOffset =
    useCallback((): GraphNodePosition => {
      const { x: nodeCenterX, y: nodeCenterY } = getNodeCenterCoordinates();

      let currentCounter = previousNodeOffset.counter;
      if (
        nodeCenterX === previousNodeOffset.x &&
        nodeCenterY === previousNodeOffset.y
      ) {
        currentCounter += 1;
      } else {
        currentCounter = 0;
      }

      setPreviousNodeOffset({
        x: nodeCenterX,
        y: nodeCenterY,
        counter: currentCounter,
      });

      return {
        x: nodeCenterX + NODE_DIMENSIONS.xOffset * currentCounter,
        y: nodeCenterY + NODE_DIMENSIONS.yOffset * currentCounter,
      };
    }, [getNodeCenterCoordinates, previousNodeOffset]);

  return {
    getViewportCenterCoordinates,
    getNodeCenterCoordinates,
    getNodeCenterCoordinatesWithOffset,
  };
};

const UseGraphCoordinatesHook: GraphHookType<UseGraphCoordinates> = {
  hook: useGraphCoordinates,
};

export default UseGraphCoordinatesHook;
