import UseGraphCoordinatesHook from './useGraphCoordinates';
import UseGraphCallbacksHook from './useGraphCallbacks';

export type GraphHookType<T> = {
  hook: () => T;
};

export const GraphHooks = {
  useGraphCoordinates: UseGraphCoordinatesHook.hook,
  useGraphCallbacks: UseGraphCallbacksHook.hook,
};
