import { MouseEvent as ReactMouseEvent, useCallback } from 'react';

import { Node } from '@reactflow/core/dist/esm/types/nodes';
import { Edge } from '@reactflow/core/dist/esm/types/edges';

import { GraphHookType } from './GraphHooks';
import useInternalGraphScheme from '../../hooks/internal_graph/useInternalGraphScheme';

import { GraphNodeId } from '../types/node/GraphNodeId';
import { GraphEdgeId } from '../types/edge/GraphEdgeId';

interface UseGraphCallbacks {
  onNodeDragStart: (event: ReactMouseEvent, node: Node) => void;
  onEdgeClick: (event: ReactMouseEvent, edge: Edge) => void;
  onPaneClick: (event: ReactMouseEvent) => void;
}

export const useGraphCallbacks = (): UseGraphCallbacks => {
  const { selectedGraphElement, selectGraphElement } = useInternalGraphScheme();

  const onNodeDragStart = useCallback(
    (_: ReactMouseEvent, node: Node) => {
      const nodeId: GraphNodeId = node.id;
      if (nodeId !== selectedGraphElement?.id) {
        selectGraphElement(nodeId);
      }
    },
    [selectGraphElement, selectedGraphElement],
  );

  const onEdgeClick = useCallback(
    (_: ReactMouseEvent, edge: Edge) => {
      const edgeId: GraphEdgeId = edge.id;
      if (edgeId !== selectedGraphElement?.id) {
        selectGraphElement(edgeId);
      }
    },
    [selectGraphElement, selectedGraphElement],
  );

  const onPaneClick = useCallback(
    (_: ReactMouseEvent) => {
      if (selectedGraphElement !== null) {
        selectGraphElement(null);
      }
    },
    [selectGraphElement, selectedGraphElement],
  );

  return {
    onNodeDragStart,
    onEdgeClick,
    onPaneClick,
  };
};

const UseGraphCallbacksHook: GraphHookType<UseGraphCallbacks> = {
  hook: useGraphCallbacks,
};

export default UseGraphCallbacksHook;
