export const NODE_DIMENSIONS = {
  defaultWidth: 150,
  defaultHeight: 35,
  xOffset: 20,
  yOffset: 20,
};
