import {
  DefaultNodeType,
  GraphNodeType,
  InputNodeType,
  OutputNodeType,
} from '../types/node/GraphNodeType';

import { en } from '../../constants/l10n/en';

export const NODES: Record<string, { name: string; type: GraphNodeType }> = {
  output: {
    name: en.graph.editor.sidebar.controls.sections.elements.outputNode.heading,
    type: InputNodeType,
  },
  default: {
    name: en.graph.editor.sidebar.controls.sections.elements.transputNode
      .heading,
    type: DefaultNodeType,
  },
  input: {
    name: en.graph.editor.sidebar.controls.sections.elements.inputNode.heading,
    type: OutputNodeType,
  },
};
