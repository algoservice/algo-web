import { GraphEdge } from '../types/edge/GraphEdge';
import { GraphNode } from '../types/node/GraphNode';
import { GraphScheme } from '../types/scheme/GraphScheme';
import extractElement from './extractElement';
import { GraphNodeId } from '../types/node/GraphNodeId';
import { GraphEdgeId } from '../types/edge/GraphEdgeId';
import isGraphNode from './guards/isGraphNode';
import isGraphEdge from './guards/isGraphEdge';

export type ElementSelection = {
  selectedElement: GraphNode | GraphEdge | null;
  newScheme: GraphScheme;
};

/**
 * No element selected, no element currently in selection.
 * @param oldScheme Graph scheme.
 */
export const noElementSelection = (
  oldScheme: GraphScheme,
): ElementSelection => ({
  selectedElement: null,
  newScheme: oldScheme,
});

/**
 * Element selected, no element currently in selection.
 * @param selectedElementId New selected element ID.
 * @param oldScheme Graph scheme.
 */
export const newElementSelection = (
  selectedElementId: GraphNodeId | GraphEdgeId,
  oldScheme: GraphScheme,
): ElementSelection => {
  let elementSelection: ElementSelection;

  const extractedNode = extractElement<GraphNode, GraphNodeId>(
    selectedElementId,
    oldScheme.nodes,
  );
  elementSelection = {
    selectedElement: extractedNode.extractedElement,
    newScheme: {
      ...oldScheme,
      nodes: extractedNode.reducedElements,
    },
  };
  if (!extractedNode.extractedElement) {
    const extractedEdge = extractElement<GraphEdge, GraphEdgeId>(
      selectedElementId,
      oldScheme.edges,
    );
    elementSelection = {
      selectedElement: extractedEdge.extractedElement,
      newScheme: {
        ...oldScheme,
        edges: extractedEdge.reducedElements,
      },
    };
  }

  return elementSelection;
};

/**
 * No element selected, some element currently in selection.
 *
 * @param unselectedElement Element that should not be selected.
 * @param oldScheme Graph scheme.
 */
export const currentElementSelection = (
  unselectedElement: GraphNode | GraphEdge,
  oldScheme: GraphScheme,
): ElementSelection => ({
  selectedElement: null,
  newScheme: {
    ...oldScheme,
    nodes: isGraphNode(unselectedElement)
      ? [...oldScheme.nodes, unselectedElement]
      : oldScheme.nodes,
    edges: isGraphEdge(unselectedElement)
      ? [...oldScheme.edges, unselectedElement]
      : oldScheme.edges,
  },
});

/**
 * New element selected, some element currently in selection.
 *
 * @param selectedElementId New selected element ID.
 * @param unselectedElement Element that currently in selection and should not be selected.
 * @param oldScheme Graph scheme.
 */
export const replacementElementSelection = (
  selectedElementId: GraphNodeId | GraphEdgeId,
  unselectedElement: GraphNode | GraphEdge,
  oldScheme: GraphScheme,
): ElementSelection => {
  if (selectedElementId === unselectedElement.id) {
    return {
      selectedElement: unselectedElement,
      newScheme: oldScheme,
    };
  } else {
    const unselectedAction = currentElementSelection(
      unselectedElement,
      oldScheme,
    );
    return newElementSelection(selectedElementId, unselectedAction.newScheme);
  }
};
