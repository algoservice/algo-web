import { GraphEdge } from '../types/edge/GraphEdge';
import { GraphNode } from '../types/node/GraphNode';
import getNode from './getNode';
import {
  DefaultNodeType,
  InputNodeType,
  OutputNodeType,
} from '../types/node/GraphNodeType';
import { GraphNodeId } from '../types/node/GraphNodeId';

export type EdgeAvailableConnections = {
  source: GraphNode[];
  target: GraphNode[];
};

const getAvailableNodesWithSource = (
  targetEdge: GraphEdge,
  nodes: GraphNode[],
  edges: GraphEdge[],
): GraphNode[] => {
  const allNodesWithOutput = nodes.filter(
    (node) => node.type === InputNodeType || node.type === DefaultNodeType,
  );
  const restrictedNodeWithOutputIds: GraphNodeId[] = edges
    .filter((edge) => targetEdge.target === edge.target)
    .map((restrictedEdge) => restrictedEdge.source);
  const availableSourceNodes = allNodesWithOutput.filter(
    (node) =>
      node.id !== targetEdge.source &&
      node.id !== targetEdge.target &&
      !restrictedNodeWithOutputIds.includes(node.id),
  );
  const sourceNode = getNode(targetEdge.source, nodes);
  return sourceNode
    ? [sourceNode, ...availableSourceNodes]
    : availableSourceNodes;
};
const getAvailableNodesWithTarget = (
  targetEdge: GraphEdge,
  nodes: GraphNode[],
  edges: GraphEdge[],
): GraphNode[] => {
  const allNodesWithInput = nodes.filter(
    (node) => node.type === OutputNodeType || node.type === DefaultNodeType,
  );
  const restrictedNodeWithInputIds: GraphNodeId[] = edges
    .filter((edge) => targetEdge.source === edge.source)
    .map((restrictedEdge) => restrictedEdge.target);
  const availableInputNodes = allNodesWithInput.filter(
    (node) =>
      node.id !== targetEdge.source &&
      node.id !== targetEdge.target &&
      !restrictedNodeWithInputIds.includes(node.id),
  );
  const targetNode = getNode(targetEdge.target, nodes);
  return targetNode
    ? [targetNode, ...availableInputNodes]
    : availableInputNodes;
};

const getEdgeAvailableConnections = (
  targetEdge: GraphEdge,
  nodes: GraphNode[],
  edges: GraphEdge[],
): EdgeAvailableConnections => ({
  source: getAvailableNodesWithSource(targetEdge, nodes, edges),
  target: getAvailableNodesWithTarget(targetEdge, nodes, edges),
});

export default getEdgeAvailableConnections;
