import { GraphNode } from '../types/node/GraphNode';

const replaceNode = (node: GraphNode, nodes: GraphNode[]): GraphNode[] => {
  const nodeIndex = nodes.findIndex((n) => n.id === node.id);
  const newNodes = nodes.slice();
  newNodes.splice(nodeIndex, 1, node);
  return newNodes;
};

export default replaceNode;
