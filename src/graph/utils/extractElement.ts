import { GraphNode } from '../types/node/GraphNode';
import { GraphEdge } from '../types/edge/GraphEdge';
import { GraphEdgeId } from '../types/edge/GraphEdgeId';
import { GraphNodeId } from '../types/node/GraphNodeId';

export type ExtractedValue<T extends GraphNode | GraphEdge> = {
  extractedElement: T | null;
  reducedElements: T[];
};

const extractElement = <
  T extends GraphNode | GraphEdge,
  V extends GraphNodeId | GraphEdgeId | null,
>(
  elementId: V,
  elements: T[],
): ExtractedValue<T> => {
  if (elementId === null) {
    return {
      extractedElement: null,
      reducedElements: elements,
    };
  }

  let extractedElement: T | null = null;
  const reducedElements = elements.filter((element) => {
    if (element.id === elementId) {
      extractedElement = element;
      return false;
    } else {
      return true;
    }
  });

  return {
    extractedElement,
    reducedElements,
  };
};

export default extractElement;
