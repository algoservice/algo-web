import { GraphNode } from '../../types/node/GraphNode';
import { GraphEdge } from '../../types/edge/GraphEdge';

const isGraphNode = (
  graphElement: GraphNode | GraphEdge | null | undefined,
): graphElement is GraphNode => {
  if (!graphElement) {
    return false;
  }
  return (graphElement as GraphNode).position !== undefined;
};

export default isGraphNode;
