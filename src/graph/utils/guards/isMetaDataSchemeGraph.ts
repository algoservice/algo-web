import { Graph } from '../../types/Graph';
import { MetaDataSchemeGraph } from '../../types/MetaDataSchemeGraph';

const isMetaDataSchemeGraph = (
  graph: Graph | null | undefined,
): graph is MetaDataSchemeGraph => {
  if (!graph) {
    return false;
  }
  return (
    (graph as MetaDataSchemeGraph).meta !== undefined &&
    (graph as MetaDataSchemeGraph).data !== undefined &&
    (graph as MetaDataSchemeGraph).scheme !== undefined
  );
};

export default isMetaDataSchemeGraph;
