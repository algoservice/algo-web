import { Graph } from '../../types/Graph';
import { DataSchemeGraph } from '../../types/DataSchemeGraph';

const isDataSchemeGraph = (
  graph: Graph | null | undefined,
): graph is DataSchemeGraph => {
  if (!graph) {
    return false;
  }
  return (
    (graph as DataSchemeGraph).data !== undefined &&
    (graph as DataSchemeGraph).scheme !== undefined
  );
};

export default isDataSchemeGraph;
