import { Graph } from '../../types/Graph';
import { MetaDataGraph } from '../../types/MetaDataGraph';

const isMetaDataGraph = (
  graph: Graph | null | undefined,
): graph is MetaDataGraph => {
  if (!graph) {
    return false;
  }
  return (
    (graph as MetaDataGraph).meta !== undefined &&
    (graph as MetaDataGraph).data !== undefined
  );
};

export default isMetaDataGraph;
