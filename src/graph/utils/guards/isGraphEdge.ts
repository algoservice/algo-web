import { GraphNode } from '../../types/node/GraphNode';
import { GraphEdge } from '../../types/edge/GraphEdge';

const isGraphEdge = (
  graphElement: GraphNode | GraphEdge | null | undefined,
): graphElement is GraphEdge => {
  if (!graphElement) {
    return false;
  }
  return (
    (graphElement as GraphEdge).source !== undefined &&
    (graphElement as GraphEdge).target !== undefined
  );
};

export default isGraphEdge;
