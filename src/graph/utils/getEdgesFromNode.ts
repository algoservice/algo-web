import { GraphNodeId } from '../types/node/GraphNodeId';
import { GraphEdge } from '../types/edge/GraphEdge';

const getEdgesFromNode = (
  nodeId: GraphNodeId,
  edges: GraphEdge[],
): GraphEdge[] => edges.filter((edge) => edge.source === nodeId);

export default getEdgesFromNode;
