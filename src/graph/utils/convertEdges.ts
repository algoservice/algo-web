import { Edge } from 'reactflow';

import { GraphEdge } from '../types/edge/GraphEdge';

const convertEdges = (reactFlowEdges: Edge[]): GraphEdge[] =>
  reactFlowEdges.map((edge) => ({
    id: edge.id,
    source: edge.source,
    target: edge.target,
  }));

export default convertEdges;
