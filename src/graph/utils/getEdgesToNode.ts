import { GraphNodeId } from '../types/node/GraphNodeId';
import { GraphEdge } from '../types/edge/GraphEdge';

const getEdgesToNode = (nodeId: GraphNodeId, edges: GraphEdge[]): GraphEdge[] =>
  edges.filter((edge) => edge.target === nodeId);

export default getEdgesToNode;
