import { Node } from 'reactflow';

import { GraphNodeData } from '../types/node/GraphNodeData';
import { GraphNode } from '../types/node/GraphNode';

const convertNodes = (reactFlowNodes: Node<GraphNodeData>[]): GraphNode[] =>
  reactFlowNodes.map((node) => ({
    id: node.id,
    position: {
      x: node.position.x,
      y: node.position.y,
    },
    data: node.data,
    type: node.type as GraphNode['type'],
  }));

export default convertNodes;
