import { GraphNode } from '../types/node/GraphNode';
import { GraphEdge } from '../types/edge/GraphEdge';
import getNode from './getNode';

export type NodeConnections = {
  input: GraphNode[];
  output: GraphNode[];
};

const getConnectedNodesToInput = (
  node: GraphNode,
  nodes: GraphNode[],
  edges: GraphEdge[],
): GraphNode[] =>
  edges
    .filter((edge) => edge.target === node.id)
    .map((edge) => getNode(edge.source, nodes) as GraphNode);

const getConnectedNodesFromOutput = (
  node: GraphNode,
  nodes: GraphNode[],
  edges: GraphEdge[],
): GraphNode[] =>
  edges
    .filter((edge) => edge.source === node.id)
    .map((edge) => getNode(edge.target, nodes) as GraphNode);

const getNodeConnections = (
  node: GraphNode,
  nodes: GraphNode[],
  edges: GraphEdge[],
): NodeConnections =>
  // It's not a bug, it's a feature cause:
  // reactflow output node = is our input node
  // reactflow input node = is our output node
  // For more information @see NODES in the /src/graph/constants/Nodes.ts
  ({
    input: getConnectedNodesToInput(node, nodes, edges),
    output: getConnectedNodesFromOutput(node, nodes, edges),
  });

export default getNodeConnections;
