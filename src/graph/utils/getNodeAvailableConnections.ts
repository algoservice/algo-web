import { GraphNode } from '../types/node/GraphNode';
import {
  DefaultNodeType,
  InputNodeType,
  OutputNodeType,
} from '../types/node/GraphNodeType';

export type NodeAvailableConnections = {
  input: GraphNode[];
  output: GraphNode[];
};

const getAvailableInputNodes = (
  node: GraphNode,
  nodes: GraphNode[],
): GraphNode[] => {
  let availableNodesWithInput: GraphNode[] = [];

  if (node.type === OutputNodeType || node.type === DefaultNodeType) {
    availableNodesWithInput = nodes.filter(
      (inputNode) =>
        inputNode.type === InputNodeType || inputNode.type === DefaultNodeType,
    );
  }

  return availableNodesWithInput;
};

const getAvailableOutputNodes = (
  node: GraphNode,
  nodes: GraphNode[],
): GraphNode[] => {
  let availableNodesWithOutput: GraphNode[] = [];

  if (node.type === InputNodeType || node.type === DefaultNodeType) {
    availableNodesWithOutput = nodes.filter(
      (outputNode) =>
        outputNode.type === OutputNodeType ||
        outputNode.type === DefaultNodeType,
    );
  }

  return availableNodesWithOutput;
};

const getNodeAvailableConnections = (
  node: GraphNode,
  nodes: GraphNode[],
): NodeAvailableConnections => ({
  // It's not a bug, it's a feature cause:
  // reactflow output node = is our input node
  // reactflow input node = is our output node
  // For more information @see NODES in the /src/graph/constants/Nodes.ts
  input: getAvailableInputNodes(node, nodes),
  output: getAvailableOutputNodes(node, nodes),
});

export default getNodeAvailableConnections;
