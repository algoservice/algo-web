import { GraphNode } from '../types/node/GraphNode';
import { GraphNodeId } from '../types/node/GraphNodeId';

const getNode = (nodeId: GraphNodeId, nodes: GraphNode[]): GraphNode | null =>
  nodes.find((node) => node.id === nodeId) || null;

export default getNode;
