import { GraphNode } from '../types/node/GraphNode';

export const emptyNode: GraphNode = {
  id: '',
  position: {
    x: 0,
    y: 0,
  },
  data: {
    label: '',
  },
};
