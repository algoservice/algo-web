import { GraphScheme } from '../types/scheme/GraphScheme';
import { GraphNode } from '../types/node/GraphNode';
import { GraphEdge } from '../types/edge/GraphEdge';

export const emptyGraphNodes: GraphNode[] = [];
export const emptyGraphEdges: GraphEdge[] = [];

export const emptyGraphScheme: GraphScheme = {
  nodes: emptyGraphNodes,
  edges: emptyGraphEdges,
};
