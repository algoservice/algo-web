import { GraphData } from '../types/data/GraphData';
import { GraphDataName } from '../types/data/GraphDataName';

export const emptyGraphDataName: GraphDataName = '';

export const emptyGraphInfo: GraphData = {
  name: emptyGraphDataName,
};
