import { MetaDataSchemeGraph } from '../types/MetaDataSchemeGraph';
import { emptyGraphInfo } from './emptyGraphInfo';
import { emptyGraphScheme } from './emptyGraphScheme';
import { emptyGraphMeta } from './emptyGraphMeta';

export const emptyGraph: MetaDataSchemeGraph = {
  meta: emptyGraphMeta,
  data: emptyGraphInfo,
  scheme: emptyGraphScheme,
};
