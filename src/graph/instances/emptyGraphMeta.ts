import { GraphMeta } from '../types/meta/GraphMeta';

export const emptyGraphMeta: GraphMeta = {
  id: undefined,
};
