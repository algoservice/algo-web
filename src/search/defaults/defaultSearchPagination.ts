import { SearchPagination } from '../types/common/SearchPagination';

export const DEFAULT_SEARCH_PAGINATION: SearchPagination = {
  page: 0,
  size: 10,
};
