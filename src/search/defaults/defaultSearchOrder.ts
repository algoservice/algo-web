import { SearchOrder } from '../types/common/SearchOrder';

export const DEFAULT_SEARCH_ORDER: SearchOrder = SearchOrder.RAND;
