import { GraphSearch } from '../types/GraphSearch';
import { DEFAULT_SEARCH_TEXT } from '../defaults/defaultSearchText';
import { DEFAULT_SEARCH_ORDER } from '../defaults/defaultSearchOrder';
import { DEFAULT_SEARCH_PAGINATION } from '../defaults/defaultSearchPagination';

const makeGraphSearch = (search: Partial<GraphSearch> = {}): GraphSearch => ({
  searchText: search.searchText ?? DEFAULT_SEARCH_TEXT,
  searchOrder: search.searchOrder ?? DEFAULT_SEARCH_ORDER,
  searchPagination: search.searchPagination ?? DEFAULT_SEARCH_PAGINATION,
});

export default makeGraphSearch;
