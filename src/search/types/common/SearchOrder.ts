export enum SearchOrder {
  ASC = 'ASC',
  DESC = 'DESC',
  RAND = 'RAND',
}
