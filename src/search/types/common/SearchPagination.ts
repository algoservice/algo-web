export type SearchPagination = {
  page: number;
  size: number;
};
