import { SearchText } from './common/SearchText';
import { SearchPagination } from './common/SearchPagination';
import { SearchOrder } from './common/SearchOrder';

export type GraphSearch = {
  searchText: SearchText;
  searchOrder: SearchOrder;
  searchPagination: SearchPagination;
};
